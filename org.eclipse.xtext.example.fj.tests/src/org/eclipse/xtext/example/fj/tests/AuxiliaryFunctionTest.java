/**
 * 
 */
package org.eclipse.xtext.example.fj.tests;

import java.io.IOException;
import java.util.List;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Field;
import org.eclipse.xtext.example.fj.Method;
import org.eclipse.xtext.example.fj.New;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.linker.FJLinkingResource;
import org.eclipse.xtext.example.lookup.AuxiliaryFunctions;
import org.eclipse.xtext.example.util.ClassFactory;
import org.eclipse.xtext.example.util.ClassSet;
import org.eclipse.xtext.example.util.TypeUtils;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * @author bettini
 *
 * Tests for auxiliary functions
 */
public class AuxiliaryFunctionTest extends TestWithLoader {
	AuxiliaryFunctions fixture;
	
	public AuxiliaryFunctionTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		fixture = new AuxiliaryFunctions();
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test the fields lookup function on object
	 */
	public void testObjectFields() {
		List<Field> fields = fixture.getFields(new FJLinkingResource().getObjectClass());
		assertEquals(0, fields.size());
	}
	
	/**
	 * Test for file one_class.fj
	 */
	public void testOneClass() {
		Program program = getProgramFromResource("one_class.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(0);
		List<Field> fields = fixture.getFields(class1);
		assertEquals(1, fields.size());
		assertEquals("a", fields.get(0).getName());
	}
	
	/**
	 * Test for file two_classes.fj
	 */
	public void testTwoClasses() {
		Program program = getProgramFromResource("two_classes.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(1);
		assertEquals("B", class1.getName());
		List<Field> fields = fixture.getFields(class1);
		assertEquals(1, fields.size());
		assertEquals("a", fields.get(0).getName());
	}

	/**
	 * Test for file classes_fields.fj
	 */
	public void testFieldsAdditions() {
		Program program = getProgramFromResource("classes_fields.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(2);
		assertEquals("C", class1.getName());
		List<Field> fields = fixture.getFields(class1);
		System.out.println("fields: " + fields);
		assertEquals(3, fields.size());
		assertEquals("c", fields.get(0).getName());
		assertEquals("b", fields.get(1).getName());
		assertEquals("a", fields.get(2).getName());
	}

	/**
	 * Test for file classes_fields2.fj.
	 * Fields are collected with inherited fields first.
	 */
	public void testOrderedFieldsAdditions() {
		Program program = getProgramFromResource("classes_fields2.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(2);
		assertEquals("C", class1.getName());
		List<Field> fields = fixture.getOrderedFields(class1);
		System.out.println("fields: " + fields);
		assertEquals(6, fields.size());
		assertEquals("b", fields.get(0).getName());
		assertEquals("a", fields.get(1).getName());
		assertEquals("ba", fields.get(2).getName());
		assertEquals("bb", fields.get(3).getName());
		assertEquals("cb", fields.get(4).getName());
		assertEquals("ca", fields.get(5).getName());
	}

	public void testGetField() {
		Program program = getProgramFromResource("classes_fields.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(2);
		assertEquals("C", class1.getName());
		Field field = fixture.getField(class1, "c");
		assertTrue(field != null);
		assertEquals("Object", TypeUtils.getClassref(field.getType()).getName());
		field = fixture.getField(class1, "a");
		assertTrue(field != null);
		assertEquals("A", TypeUtils.getClassref(field.getType()).getName());
		field = fixture.getField(class1, "foobar");
		assertTrue(field == null);
	}
	
	/**
	 * Test for file two_classes.fj and getMethods
	 */
	public void testGetMethods() {
		Program program = getProgramFromResource("two_classes.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(1);
		assertEquals("B", class1.getName());
		List<Method> methods = fixture.getMethods(class1);
		assertEquals(1, methods.size());
		assertEquals("m", methods.get(0).getName());
	}

	/**
	 * Test for file classes_methods.fj
	 */
	public void testMethodsAdditions() {
		Program program = getProgramFromResource("classes_methods.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(2);
		assertEquals("C", class1.getName());
		List<Method> methods = fixture.getMethods(class1);
		System.out.println("methods: " + methods);
		assertEquals(3, methods.size());
		assertEquals("n", methods.get(0).getName());
		assertEquals("o", methods.get(1).getName());
		assertEquals("m", methods.get(2).getName());
	}

	public void testGetMethod() {
		Program program = getProgramFromResource("classes_methods.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(2);
		assertEquals("C", class1.getName());
		Method method = fixture.getMethod(class1, "o");
		assertTrue(method != null);
		assertEquals("C", TypeUtils.getClassref(method.getReturntype()).getName());
		method = fixture.getMethod(class1, "m");
		assertTrue(method != null);
		assertEquals("A", TypeUtils.getClassref(method.getReturntype()).getName());
		method = fixture.getMethod(class1, "foobar");
		assertTrue(method == null);
	}

	public void testGetMethodOverridden() {
		Program program = getProgramFromResource("classes_methods_overridden.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(2);
		assertEquals("C", class1.getName());
		Method method = fixture.getMethod(class1, "m");
		assertTrue(method != null);
		assertEquals("B", ((New)(method.getBody().getExpression())).getType().getClassref().getName());
		method = fixture.getMethod(class1, "n");
		assertTrue(method != null);
		assertEquals("C", ((New)(method.getBody().getExpression())).getType().getClassref().getName());
		method = fixture.getMethod(class1, "foobar");
		assertTrue(method == null);
	}

	public void testSuperclasses() {
		// make sure to put everything in a resource
		Resource resource = createResource();
		
		Class c1 = ClassFactory.createClass("c1");
		resource.getContents().add(c1);
		Class c2 = ClassFactory.createClass("c2", c1);
		resource.getContents().add(c2);
		Class c3 = ClassFactory.createClass("c3", c2);
		resource.getContents().add(c3);
		
		ClassSet superClasses = fixture.getSuperclasses(c1);
		System.out.println("superclasses of c1: " + superClasses);
		// Object is implicit
		assertEquals(1, superClasses.size());
		assertEquals("Object", superClasses.first().getName());
		
		superClasses = fixture.getSuperclasses(c3);
		System.out.println("superclasses of c3: " + superClasses);
		assertEquals(3, superClasses.size());
		assertTrue(superClasses.contains(c1));
		assertTrue(superClasses.contains(c2));
	}
	
	public void testOrderedSuperclasses() {
		// make sure to put everything in a resource
		Resource resource = createResource();
		
		Class c1 = ClassFactory.createClass("c1");
		resource.getContents().add(c1);
		Class c2 = ClassFactory.createClass("c2", c1);
		resource.getContents().add(c2);
		Class c3 = ClassFactory.createClass("c3", c2);
		resource.getContents().add(c3);
		
		List<Class> superClasses = fixture.getOrderedSuperclasses(c1);
		System.out.println("superclasses of c1: " + superClasses);
		// Object is implicit
		assertEquals(1, superClasses.size());
		assertEquals("Object", superClasses.get(0).getName());
		
		superClasses = fixture.getOrderedSuperclasses(c3);
		System.out.println("superclasses of c3: " + superClasses);
		assertEquals(3, superClasses.size());
		assertEquals("c2", superClasses.get(0).getName());
		assertEquals("c1", superClasses.get(1).getName());
		assertEquals("Object", superClasses.get(2).getName());
	}
	
	public void testCollectClasses() throws IOException {
		Program program = loadProgramFromString("class B {} class A {}");
		List<Class> classes = fixture.collectClasses(program.getClasses().get(0));
		System.out.println("classes: " + classes);
		assertEquals(3, classes.size());
		assertEquals("Object", classes.get(0).getName());
		assertEquals("B", classes.get(1).getName());
		assertEquals("A", classes.get(2).getName());
	}
	
	public void testCollectClassesProgram() throws IOException {
		Program program = loadProgramFromString("class B {} class A {}");
		List<Class> classes = fixture.collectClasses(program);
		System.out.println("classes: " + classes);
		assertEquals(3, classes.size());
		assertEquals("Object", classes.get(0).getName());
		assertEquals("B", classes.get(1).getName());
		assertEquals("A", classes.get(2).getName());
	}
}
