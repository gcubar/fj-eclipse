/**
 * <copyright>
 * </copyright>
 *
 * $Id: CastTest.java,v 1.1 2009-11-02 11:25:12 bettini Exp $
 */
package org.eclipse.xtext.example.fj.tests;

import junit.textui.TestRunner;

import org.eclipse.xtext.example.FJStandaloneSetup;
import org.eclipse.xtext.example.fj.Expression;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.typing.FJTypeChecker;
import org.eclipse.xtext.junit.AbstractXtextTests;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Cast</b></em>'. <!-- end-user-doc -->
 */
public class CastTest extends AbstractXtextTests {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CastTest.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		with(new FJStandaloneSetup());
	}
	
	/**
	 * test for incorrect cast
	 * @throws Exception 
	 */
	public void testCastFail() throws Exception {
		Program program = (Program) getModel("class B { } class A { } (B) new A()");
		Expression main = program.getMain();
		String errors = new FJTypeChecker().typeCheck(main);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("expression type A and B are unrelated", errors);
	}
} // CastTest
