/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassTest.java,v 1.1 2009-11-02 11:25:13 bettini Exp $
 */
package org.eclipse.xtext.example.fj.tests;

import java.io.IOException;

import junit.textui.TestRunner;
import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Field;
import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.typing.FJTypeChecker;
import org.eclipse.xtext.example.util.TypeUtils;
import org.eclipse.xtext.example.validation.FJJavaValidator;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Class</b></em>'. <!-- end-user-doc -->
 * 
 * @generated NOT
 */
public class ClassTest extends TestWithLoader {
	/**
	 * The fixture for this Class test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 */
	protected Class fixture = null;

	/**
	 * Sets the fixture for this Class test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 */
	public void setFixture(Class fixture) {
		this.fixture = fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 */
	public static void main(String[] args) {
		TestRunner.run(ClassTest.class);
	}

	/**
	 * Constructs a new Class test case with the given name. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 */
	public ClassTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Class test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	protected org.eclipse.xtext.example.fj.Class getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		setFixture(FjFactory.eINSTANCE.createClass());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	protected void tearDown() throws Exception {
		setFixture(null);
		super.tearDown();
	}

	/**
	 * Test for file one_class.fj
	 */
	public void testOneClass() {
		Program program = getProgramFromResource("one_class.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(0);
		assertEquals("A", class1.getName());
		assertTrue(class1.getExtends() == null);
		Field field = class1.getFields().get(0);
		assertTrue(field != null);
		assertEquals("A", TypeUtils.getClassref(field.getType()).getName());
		assertEquals("a", field.getName());
	}

	/**
	 * Test for file two_classes.fj
	 */
	public void testTwoClasses() {
		Program program = getProgramFromResource("two_classes.fj");
		assertTrue(program != null);
		Class class1 = program.getClasses().get(0);
		assertEquals("A", class1.getName());
		assertTrue(class1.getExtends() == null);
		Field field = class1.getFields().get(0);
		assertTrue(field != null);
		assertEquals("A", TypeUtils.getClassref(field.getType()).getName());
		assertEquals("a", field.getName());
	}

	/**
	 * Test for file class_sytanx_error.fj which contains a syntax error
	 */
	public void testClassSyntaxError() {
		Resource resource = loadResource("class_syntax_error.fj");
		EList<Diagnostic> errors = resource.getErrors();
		System.out.println(errors);
		assertEquals(1, errors.size());
		assertEquals("mismatched input '}' expecting '('", errors.get(0)
				.getMessage());
	}

	/**
	 * Detects simple cycle in the hierarchy
	 * 
	 * <pre>
	 * class A extends A {
	 * }
	 * </pre>
	 * 
	 * @throws IOException
	 */
	public void testSimpleCycle() throws IOException {
		Program program = loadProgramFromString("class A extends A {}");
		String errors = new FJTypeChecker().typeCheck(program.getClasses().get(
				0));
		System.out.println("errors: " + errors);
		assertEquals("cycle in the hierarchy of A", errors);
	}

	/**
	 * Detects cycles in the class hierarchy
	 * 
	 * A - B - C - A
	 */
	public void testAcyclic() {
		Class A = FjFactory.eINSTANCE.createClass();
		A.setName("A");
		Class B = FjFactory.eINSTANCE.createClass();
		B.setName("B");
		B.setExtends(A);
		Class C = FjFactory.eINSTANCE.createClass();
		C.setName("C");
		C.setExtends(B);
		Class A1 = FjFactory.eINSTANCE.createClass();
		A1.setName("A");
		A1.setExtends(C);

		// we need the resource for computing superclasses
		Resource resource = createResource();
		resource.getContents().add(A);
		resource.getContents().add(B);
		resource.getContents().add(C);
		resource.getContents().add(A1);

		FJJavaValidator validator = getValidator();
		BasicDiagnostic basicDiagnostic = new BasicDiagnostic();
		validator.validate(A1, basicDiagnostic, null);
		System.out.println("diagnostic: " + basicDiagnostic.toString());
		System.out.println("diagnostic: "
				+ basicDiagnostic.getChildren().get(0).getMessage());
		String diagnostic = basicDiagnostic.getChildren().get(0).toString();
		System.out.println("diagnostic: " + diagnostic);
		assertEquals(
				"Diagnostic ERROR \"Cycle in the class hierarchy\" at Class'A'.name==\"A\"",
				diagnostic);
	}

	/**
	 * Test for file classes with the same name in the same program
	 */
	public void testDuplicateClassesValidation() {
		Resource resource = loadResource("duplicate_classes.fj");
		Program program = (Program) resource.getContents().get(0);
		
		Class A1 = program.getClasses().get(0);
		Class B = program.getClasses().get(1);
		Class C = program.getClasses().get(2);
		Class A2 = program.getClasses().get(3);
		
		FJJavaValidator validator = getValidator();
		BasicDiagnostic basicDiagnostic = new BasicDiagnostic();
		
		validator.validate(A1, basicDiagnostic, null);
		System.out.println("diagnostic: "
				+ basicDiagnostic.getChildren().get(0).getMessage());
		assertEquals("class A is already declared in the program",
				basicDiagnostic.getChildren().get(0).getMessage());
		// error for class B
		basicDiagnostic = new BasicDiagnostic();
		validator.validate(B, basicDiagnostic, null);
		System.out.println("diagnostic: "
				+ basicDiagnostic.toString());
		assertEquals(0, basicDiagnostic.getChildren().size());
		// error for class C
		basicDiagnostic = new BasicDiagnostic();
		validator.validate(C, basicDiagnostic, null);
		System.out.println("diagnostic: "
				+ basicDiagnostic.toString());
		assertEquals(0, basicDiagnostic.getChildren().size());
		// error also for the second occurrence of A
		basicDiagnostic = new BasicDiagnostic();
		validator.validate(A2, basicDiagnostic, null);
		System.out.println("diagnostic: "
				+ basicDiagnostic.getChildren().get(0).getMessage());
		assertEquals("class A is already declared in the program",
				basicDiagnostic.getChildren().get(0).getMessage());
	}

} // ClassTest
