/**
 * <copyright>
 * </copyright>
 *
 * $Id: MethodBodyTest.java,v 1.1 2009-11-02 11:25:12 bettini Exp $
 */
package org.eclipse.xtext.example.fj.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.MethodBody;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Method Body</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MethodBodyTest extends TestCase {

	/**
	 * The fixture for this Method Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodBody fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MethodBodyTest.class);
	}

	/**
	 * Constructs a new Method Body test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodBodyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Method Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MethodBody fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Method Body test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodBody getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FjFactory.eINSTANCE.createMethodBody());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MethodBodyTest
