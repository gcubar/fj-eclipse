/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProgramTest.java,v 1.1 2009-11-02 11:25:13 bettini Exp $
 */
package org.eclipse.xtext.example.fj.tests;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import junit.textui.TestRunner;
import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Field;
import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.New;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.util.ClassFactory;
import org.eclipse.xtext.example.util.FragmentPrinter;
import org.eclipse.xtext.example.util.TypeUtils;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.parsetree.reconstr.XtextSerializationException;

/**
 * <!-- begin-user-doc --> A test case for the model object '
 * <em><b>Program</b></em>'. <!-- end-user-doc -->
 * 
 * @generated NOT
 */
public class ProgramTest extends TestWithLoader {

	/**
	 * The fixture for this Program test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected Program fixture = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ProgramTest.class);
	}

	/**
	 * Constructs a new Program test case with the given name. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ProgramTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Program test case. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	protected void setFixture(Program fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Program test case. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected Program getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setFixture(FjFactory.eINSTANCE.createProgram());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * An empty program must not have Object either
	 */
	public void testEmptyProgram() {
		assertEquals(0, fixture.getClasses().size());
	}

	/**
	 * An empty resource must not have Object either
	 */
	public void testEmptyResource() {
		Resource resource = createResource();

		assertEquals(0, resource.getContents().size());
	}

	/**
	 * Test that object is not automatically added to the resource when using a
	 * model directly
	 */
	public void testResourceWithOneClass() {
		Resource resource = createResource();

		resource.getContents().add(fixture);
		assertEquals(1, sizeOfIterable(resource.getAllContents()));

		Class cl = ClassFactory.createClass("A");
		fixture.getClasses().add(cl);
		assertEquals(1, fixture.getClasses().size());
	}

	/**
	 * In a program, class Object is always implicitly added
	 * 
	 * @throws IOException
	 */
	public void testSimpleProgram() throws IOException {
		Resource resource = loadFromString("new Object()");
		fixture = (Program) resource.getContents().get(0);
		New n = (New) fixture.getMain();
		System.out.println("new type: " + n.getType().getClassref().getName());
		assertEquals("Object", n.getType().getClassref().getName());
	}

	/**
	 * Saves the resource corresponding to a program and makes sure that the
	 * result is just the same as the original program source.
	 * 
	 * @throws IOException
	 */
	public void testSaveResource() throws IOException {
		String program = "new Object()";
		Resource resource = loadFromString(program);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		resource.save(outputStream, null);
		System.out.println("saved: " + outputStream.toString());
		// the saved program must not contain the implicit class object
		assertEquals(program, outputStream.toString());
	}

	/**
	 * Saves the resource corresponding to a program and makes sure that the
	 * result is just the same as the original program source.
	 * 
	 * @throws IOException
	 */
	public void testSaveResource2() throws IOException {
		String program = "class A extends Object {}";
		Resource resource = loadFromString(program);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			resource.save(outputStream, null);
		} catch (XtextSerializationException e) {
			e.printStackTrace();
			throw e;
		}
		System.out.println("saved: " + outputStream.toString());
		// the saved program must not contain the implicit class object
		assertEquals(program, outputStream.toString());
	}

	/**
	 * Saves the resource corresponding to a program and makes sure that the
	 * result is just the same as the original program source.
	 * 
	 * @throws IOException
	 */
	public void testSaveResourceWithInheritance() throws IOException {
		String program = "class A {} class B extends A {} class C extends B{}";
		Resource resource = loadFromString(program);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		resource.save(outputStream, null);
		System.out.println("saved: " + outputStream.toString());
		fixture = (Program) resource.getContents().get(0);
		Class C = fixture.getClasses().get(2);
		assertEquals("C", C.getName());
		assertEquals("B", C.getExtends().getName());
		// the saved program must not contain the implicit class object
		assertEquals(program, outputStream.toString());
	}

	/**
	 * Print the fragments of all elements of a program, and checks
	 * that reference to types have exactly the same fragment of the types
	 * themselves (thus we can uniquely refer to a class in a resource
	 * using its fragment).
	 * 
	 * @throws IOException
	 */
	public void testFragments() throws IOException {
		Program program = getProgramFromString("class A { B b; A c; C m(Object a) { return new C(); } } class B {} class C {}");
		FragmentPrinter fragmentPrinter = new FragmentPrinter();
		fragmentPrinter.printFragment(program.eResource());
		Class class0 = program.getClasses().get(0);
		String fragmentToStringA = fragmentPrinter.fragmentToString(class0);
		System.out
				.println("class A: "
						+ fragmentToStringA);
		String fragmentToStringB = fragmentPrinter.fragmentToString(program.getClasses()
				.get(1));
		System.out
				.println("class B: "
						+ fragmentToStringB);
		String fragmentToStringC = fragmentPrinter.fragmentToString(program.getClasses()
				.get(2));
		System.out
				.println("class C: "
						+ fragmentToStringC);
		EList<Field> fieldsClass0 = class0
				.getFields();
		String fragmentToString_btype = fragmentPrinter.fragmentToString(TypeUtils.getClassref(fieldsClass0.get(0).getType()));
		System.out.println("field b's type: "
				+ fragmentToString_btype);
		String fragmentToString_ctype = fragmentPrinter.fragmentToString(TypeUtils.getClassref(fieldsClass0.get(1).getType()));
		System.out.println("field c's type: "
				+ fragmentToString_ctype);
		String fragmentToString_mrettype = fragmentPrinter.fragmentToString(TypeUtils.getClassref(class0
				.getMethods().get(0).getReturntype()));
		System.out.println("method m's return type: "
				+ fragmentToString_mrettype);
		
		assertEquals(fragmentToStringB, fragmentToString_btype);
		assertEquals(fragmentToStringA, fragmentToString_ctype);
		assertEquals(fragmentToStringC, fragmentToString_mrettype);
	}
} // ProgramTest
