/**
 * 
 */
package org.eclipse.xtext.example.fj.tests;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Type;
import org.eclipse.xtext.example.linker.FJLinkingResource;
import org.eclipse.xtext.example.typing.FJSubtyping;
import org.eclipse.xtext.example.util.ClassFactory;
import org.eclipse.xtext.example.util.TypeUtils;

/**
 * @author bettini
 * 
 *         Tests for subtyping
 */
public class SubtypingTest extends TestWithLoader {
	FJSubtyping fixture;

	public SubtypingTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		fixture = new FJSubtyping();
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testBasicTypes() {
		Type intType = TypeUtils.createBasicType("int");
		Type intType2 = TypeUtils.createBasicType("int");

		assertTrue(fixture.isSubtype(intType, intType2));

		Type stringType = TypeUtils.createBasicType("String");

		assertFalse(fixture.isSubtype(intType, stringType));
		assertTrue(fixture.isSubtype(stringType, stringType));
	}

	public void testBasicAndClassFail() {
		Type intType = TypeUtils.createBasicType("int");
		Type classType = TypeUtils.createClassType(ClassFactory.createClass("C"));

		assertFalse(fixture.isSubtype(intType, classType));
	}

	public void testClasses() {
		// make sure to put everything in a resource
		Resource resource = createResource();

		Class c1 = ClassFactory.createClass("c1");
		resource.getContents().add(c1);
		Class c2 = ClassFactory.createClass("c2", c1);
		resource.getContents().add(c2);
		Class c3 = ClassFactory.createClass("c3", c2);
		resource.getContents().add(c3);

		resource.getContents().add(c1);
		resource.getContents().add(c2);
		resource.getContents().add(c3);

		assertTrue(fixture.isSubtype(c3, c2));
		assertTrue(fixture.isSubtype(c3, c1));
		assertTrue(fixture.isSubtype(c3, c3));

		assertTrue(fixture.isSubtype(c1, ((FJLinkingResource) resource)
				.getObjectClass()));
		
		// wrap it in types
		Type t1 = TypeUtils.createClassType(c1);
		Type t2 = TypeUtils.createClassType(c2);
		Type t3 = TypeUtils.createClassType(c3);
		assertTrue(fixture.isSubtype(t3, t1));
		assertTrue(fixture.isSubtype(t3, t2));
		assertTrue(fixture.isSubtype(t2, t1));
	}
}
