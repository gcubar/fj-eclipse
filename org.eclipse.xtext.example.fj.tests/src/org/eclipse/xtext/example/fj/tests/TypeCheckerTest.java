/**
 * 
 */
package org.eclipse.xtext.example.fj.tests;

import java.io.IOException;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Expression;
import org.eclipse.xtext.example.fj.Field;
import org.eclipse.xtext.example.fj.Method;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.typing.FJTypeChecker;

/**
 * @author bettini
 * 
 *         Tests for validation (type checking)
 */
public class TypeCheckerTest extends TestWithLoader {
	FJTypeChecker fixture;

	public TypeCheckerTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		fixture = new FJTypeChecker();
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Checks method body
	 * 
	 * @throws IOException
	 */
	public void testMethod() throws IOException {
		Program program = getProgramFromString("class A { Object f; Object m() {return this.f;} }");
		Method method = program.getClasses().get(0).getMethods().get(0);
		String errors = fixture.typeCheck(method);
		assertTrue(errors.length() == 0);
	}

	/**
	 * Checks method body where body is a subtype of return type
	 * 
	 * @throws IOException
	 */
	public void testMethodWithSubtyping() throws IOException {
		Program program = getProgramFromString("class B {} class A extends B { A a; B m() {return this.a;} }");
		Method method = program.getClasses().get(1).getMethods().get(0);
		String errors = fixture.typeCheck(method);
		assertTrue(errors.length() == 0);
	}

	/**
	 * body returns a basic type which is not subtype of class type
	 * 
	 * @throws IOException
	 */
	public void testMethodFail() throws IOException {
		Program program = getProgramFromString("class A { int f; Object m() {return this.f;} }");
		Method method = program.getClasses().get(0).getMethods().get(0);
		String errors = fixture.typeCheck(method);
		System.out.println("error: " + errors);
		assertTrue(errors.length() != 0);
		assertEquals("the type of body, int, is not a subtype of Object",
				errors);
	}

	/**
	 * incorrect method override (different return types)
	 * 
	 * @throws IOException
	 */
	public void testOverriddenFail() throws IOException {
		Program program = getProgramFromString("class B { B m() {return new B();} } class A extends B { Object f; Object m() {return this.f;} }");
		Method method = program.getClasses().get(1).getMethods().get(0);
		String errors = fixture.typeCheck(method);
		System.out.println("error: " + errors);
		assertTrue(errors.length() != 0);
		assertEquals("return types are different (method overriding)",
				errors);
	}

	/**
	 * incorrect method override (different parameters)
	 * 
	 * @throws IOException
	 */
	public void testOverriddenFail2() throws IOException {
		Program program = getProgramFromString("class B { B m(Object a, int i) {return new B();} } class A extends B { Object f; B m(Object a, String s) {return this.f;} }");
		Method method = program.getClasses().get(1).getMethods().get(0);
		String errors = fixture.typeCheck(method);
		System.out.println("error: " + errors);
		assertTrue(errors.length() != 0);
		assertEquals("parameter type String is not equal to parameter type int",
				errors);
	}

	/**
	 * correct method override
	 * 
	 * @throws IOException
	 */
	public void testOverridden() throws IOException {
		Program program = getProgramFromString("class B { B m(Object a, int i) {return new B();} } class A extends B { Object f; B m(Object c, int s) {return new B();} }");
		Method method = program.getClasses().get(1).getMethods().get(0);
		String errors = fixture.typeCheck(method);
		System.out.println("error: " + errors);
		assertTrue(errors.length() == 0);
	}

	/**
	 * passing the wrong number of arguments to a method
	 * 
	 * @throws IOException
	 */
	public void testWrongNumberOfArgs() throws IOException {
		Program program = getProgramFromString("class A { int f; Object m() {return this.m(new A());} }");
		Expression body = program.getClasses().get(0).getMethods().get(0)
				.getBody().getExpression();
		String errors = fixture.typeCheck(body);
		System.out.println("error: " + errors);
		assertTrue(errors.length() != 0);
		assertEquals("expected 0 arguments, while 1 are provided", errors);
	}

	/**
	 * passing the argument with wrong type
	 * 
	 * @throws IOException
	 */
	public void testWrongArguments() throws IOException {
		Program program = getProgramFromString("class A { int f; Object m(Object a) {return this.m(this.f);} }");
		Expression body = program.getClasses().get(0).getMethods().get(0)
				.getBody().getExpression();
		String errors = fixture.typeCheck(body);
		System.out.println("error: " + errors);
		assertTrue(errors.length() != 0);
		assertEquals("argument type int is not a subtype of param type Object",
				errors);
	}

	/**
	 * passing the arguments with right types
	 * 
	 * @throws IOException
	 */
	public void testCorrectArguments() throws IOException {
		Program program = getProgramFromString("class A { int f; Object m(Object a, int i) {return this.m(new A(), this.f);} }");
		Expression body = program.getClasses().get(0).getMethods().get(0)
				.getBody().getExpression();
		String errors = fixture.typeCheck(body);
		System.out.println("error: " + errors);
		assertTrue(errors.length() == 0);
	}

	/**
	 * Test for duplicate fields in a class and in a hierarchy
	 * 
	 * @throws IOException
	 */
	public void testDuplicateFields() throws IOException {
		Program program = getProgramFromString("class B { int h; } class A extends B { int f; int g; int f; Object h; }");
		Class classA = program.getClasses().get(1);
		Field f1 = classA.getFields().get(0);
		String errors = fixture.typeCheck(f1);
		System.out.println("error: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("duplicate field: f", errors);
		f1 = classA.getFields().get(2);
		errors = fixture.typeCheck(f1);
		System.out.println("error: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("duplicate field: f", errors);
		f1 = classA.getFields().get(1);
		errors = fixture.typeCheck(f1);
		System.out.println("error: " + errors);
		assertTrue(errors.length() == 0);
		f1 = classA.getFields().get(3);
		errors = fixture.typeCheck(f1);
		System.out.println("error: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("field h already defined in superclasses", errors);
	}

	public void testDuplicateMethods() throws IOException {
		Program program = getProgramFromString("class A { A m() { return this; } Object m() { return this; } Object n() { return this; } }");
		Class classA = program.getClasses().get(0);
		Method m = classA.getMethods().get(0);
		String errors = fixture.typeCheck(m);
		System.out.println("error: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("duplicate method: m", errors);
		m = classA.getMethods().get(1);
		errors = fixture.typeCheck(m);
		System.out.println("error: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("duplicate method: m", errors);
		m = classA.getMethods().get(2);
		errors = fixture.typeCheck(m);
		System.out.println("error: " + errors);
		assertTrue(errors.length() == 0);
	}
	
	public void testNew() throws IOException {
		Program program = getProgramFromString("class A { int f; Object a; A m() { return new A(this.f, this.a); } }");
		Class classA = program.getClasses().get(0);
		Method m = classA.getMethods().get(0);
		Expression n = m.getBody().getExpression();
		String errors = fixture.typeCheck(n);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() == 0);
	}

	public void testNewInheritance() throws IOException {
		Program program = getProgramFromString("class B { Object c; } class A extends B { int f; Object a; A m() { return new A(this.a, this.f, this.a); } }");
		Class classA = program.getClasses().get(1);
		Method m = classA.getMethods().get(0);
		Expression n = m.getBody().getExpression();
		String errors = fixture.typeCheck(n);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() == 0);
	}

	/**
	 * wrong new invocation missing one argument
	 * 
	 * @throws IOException
	 */
	public void testNewFail() throws IOException {
		Program program = getProgramFromString("class A { int f; Object a; A m() { return new A(this.f); } }");
		Class classA = program.getClasses().get(0);
		Method m = classA.getMethods().get(0);
		Expression n = m.getBody().getExpression();
		String errors = fixture.typeCheck(n);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("expected 2 field initializers, while 1 are provided", errors);
	}

	/**
	 * wrong new invocation, type mismatch, (with inherited fields)
	 * 
	 * @throws IOException
	 */
	public void testNewInheritanceFail() throws IOException {
		Program program = getProgramFromString("class B { Object g; } class A extends B { int f; Object a; A m() { return new A(this.a, this.f, this.f); } }");
		Class classA = program.getClasses().get(1);
		Method m = classA.getMethods().get(0);
		Expression n = m.getBody().getExpression();
		String errors = fixture.typeCheck(n);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("argument type int is not a subtype of field type Object", errors);
	}

	/**
	 * test for correct cast
	 * 
	 * @throws IOException
	 */
	public void testCast() throws IOException {
		Program program = getProgramFromString("class B { } class A extends B { } (A) new B()");
		Expression main = program.getMain();
		String errors = fixture.typeCheck(main);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() == 0);
	}

	/**
	 * test for correct cast
	 * 
	 * @throws IOException
	 */
	public void testCast2() throws IOException {
		Program program = getProgramFromString("class B { } class A extends B { } (B) new A()");
		Expression main = program.getMain();
		String errors = fixture.typeCheck(main);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() == 0);
	}

	/**
	 * test for incorrect cast
	 * 
	 * @throws IOException
	 */
	public void testCastFail() throws IOException {
		Program program = getProgramFromString("class B { } class A { } (B) new A()");
		Expression main = program.getMain();
		String errors = fixture.typeCheck(main);
		System.out.println("errors: " + errors);
		assertTrue(errors.length() > 0);
		assertEquals("expression type A and B are unrelated", errors);
	}

	/**
	 * Detects simple cycle in the hierarchy
	 * <pre>class A extends A {} new A()</pre>
	 * @throws IOException 
	 */
	public void testSimpleCycleAndNew() throws IOException {
		Program program = loadProgramFromString("class A extends A {} new A()");
		String errors = new FJTypeChecker().typeCheck(program.getClasses().get(0));
		System.out.println("errors: " + errors);
		assertEquals("cycle in the hierarchy of A", errors);
	}
	
	/**
	 * Detects duplicate classes in a program
	 * @throws IOException
	 */
	public void testDuplicateClasses() throws IOException {
		Program program = loadProgramFromString("class A {} class B {} class C {} class A{}");
		String errors = new FJTypeChecker().typeCheck(program);
		System.out.println("errors: " + errors);
	}
	
	/**
	 * body returns a basic type which is not subtype of the method return type
	 * 
	 * @throws IOException
	 */
	public void testWrongConstant() throws IOException {
		Program program = getProgramFromString("class A { int m() {return true;} }");
		Method method = program.getClasses().get(0).getMethods().get(0);
		String errors = fixture.typeCheck(method);
		System.out.println("error: " + errors);
		assertTrue(errors.length() != 0);
		assertEquals("the type of body, boolean, is not a subtype of int",
				errors);
	}

}
