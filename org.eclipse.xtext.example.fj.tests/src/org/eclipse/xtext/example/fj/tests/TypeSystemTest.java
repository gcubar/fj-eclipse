/**
 * 
 */
package org.eclipse.xtext.example.fj.tests;

import java.io.IOException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;

import org.eclipse.xtext.example.fj.Argument;
import org.eclipse.xtext.example.fj.BoolConstant;
import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Expression;
import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.Field;
import org.eclipse.xtext.example.fj.FieldSelection;
import org.eclipse.xtext.example.fj.IntConstant;
import org.eclipse.xtext.example.fj.Method;
import org.eclipse.xtext.example.fj.New;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.fj.Selection;
import org.eclipse.xtext.example.fj.StringConstant;
import org.eclipse.xtext.example.fj.TypedElement;
import org.eclipse.xtext.example.typing.FJTypeSystem;
import org.eclipse.xtext.example.typing.TypeResult;
import org.eclipse.xtext.example.util.ContainingClassFinder;
import org.eclipse.xtext.example.util.TypeUtils;

/**
 * @author bettini
 * 
 *         Tests for type system
 */
public class TypeSystemTest extends TestWithLoader {
	FJTypeSystem fixture;

	public TypeSystemTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		fixture = new FJTypeSystem();
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test the type for new instance
	 */
	public void testNew() {
		Class c = FjFactory.eINSTANCE.createClass();
		c.setName("A");
		New n = FjFactory.eINSTANCE.createNew();
		n.setType(TypeUtils.createClassType(c));

		TypeResult typeResult = fixture.getType(n);
		assertEquals("A", TypeUtils.typeToString(typeResult.getType()));
	}

	/**
	 * Test the typability of the expression
	 * 
	 * <pre>
	 * new A().f
	 * </pre>
	 * 
	 * where
	 * 
	 * <pre>
	 * class A {
	 * 	F f;
	 * }
	 * </pre>
	 */
	public void testFieldSelection() {
		Resource resource = createResource();
		
		Class c = FjFactory.eINSTANCE.createClass();
		c.setName("A");
		Class c1 = FjFactory.eINSTANCE.createClass();
		c1.setName("F"); // the type of the field
		New n = FjFactory.eINSTANCE.createNew();
		n.setType(TypeUtils.createClassType(c));
		Field field = FjFactory.eINSTANCE.createField();
		field.setName("f");
		TypeUtils.setTypeClassReference(field, c1);
		c.getFields().add(field);
		Selection selection = FjFactory.eINSTANCE.createSelection();
		selection.setReceiver(n);
		FieldSelection fieldSelection = FjFactory.eINSTANCE
				.createFieldSelection();
		fieldSelection.setName(field);
		selection.setMessage(fieldSelection);
		
		resource.getContents().add(c);
		resource.getContents().add(c1);

		TypeResult typeResult = fixture.getType(selection);
		System.out.println("type: " + typeResult);
		assertTrue(typeResult.getType() != null);
		assertEquals("F", TypeUtils.typeToString(typeResult.getType()));
	}

	/**
	 * Test the typability of the expression
	 * 
	 * <pre>
	 * this.f
	 * </pre>
	 * 
	 * where
	 * 
	 * <pre>
	 * class A {
	 * 	F f;
	 * }
	 * </pre>
	 * 
	 * @throws IOException
	 */
	public void testFieldSelectionOnThis() throws IOException {
		Program program = getProgramFromString("class F {} class A { F f; F m() { return this.f; }}");

		Expression exp = program.getClasses().get(1).getMethods().get(0)
				.getBody().getExpression();

		TypeResult typeResult = fixture.getType(exp);
		System.out.println("type: " + typeResult);
		assertTrue(typeResult.getType() != null);
		assertEquals("F", TypeUtils.typeToString(typeResult.getType()));
	}

	/**
	 * Test the typability of the expression
	 * 
	 * <pre>
	 * this.f.g
	 * </pre>
	 * 
	 * where
	 * 
	 * <pre>
	 * class A {
	 * 	F f;
	 * }
	 * 
	 * class G {}
	 * 
	 * class F {
	 *  G g
	 * }
	 * </pre>
	 * 
	 * @throws IOException
	 */
	public void testFieldSelectionOnField() throws IOException {
		Program program = getProgramFromString("class F { G g; } class A { F f; G m() { return this.f.g; }} class G {}");

		Expression exp = program.getClasses().get(1).getMethods().get(0)
				.getBody().getExpression();

		TypeResult typeResult = fixture.getType(exp);
		System.out.println("type: " + typeResult);
		assertTrue(typeResult.getType() != null);
		assertEquals("G", TypeUtils.typeToString(typeResult.getType()));
	}

	/**
	 * Tests finding the class of this
	 * 
	 * @throws IOException
	 */
	public void testFindContainingClass() throws IOException {
		Program program = getProgramFromString("class A { A f; A m() { return this; }}");
		Expression thisV = program.getClasses().get(0).getMethods().get(0)
				.getBody().getExpression();
		Class containingClass = new ContainingClassFinder()
				.getContainingClass(thisV);
		System.out.println("containing class: " + containingClass.getName());
		assertEquals("A", containingClass.getName());
	}

	/**
	 * Tests finding the class of this
	 * 
	 * @throws IOException
	 */
	public void testFindContainingClass2() throws IOException {
		Program program = getProgramFromString("class A { A f; A m() { return new A(this); }}");
		New newA = (New) program.getClasses().get(0).getMethods().get(0)
				.getBody().getExpression();
		Argument thisV = newA.getArgs().get(0);
		Class containingClass = new ContainingClassFinder()
				.getContainingClass(thisV);
		System.out.println("containing class: " + containingClass.getName());
		assertEquals("A", containingClass.getName());
	}

	/**
	 * Cannot find the class of this in the main
	 * 
	 * @throws IOException
	 */
	public void testFindContainingClassFail() throws IOException {
		Program program = getProgramFromString("this");
		Expression thisV = program.getMain();
		Class containingClass = new ContainingClassFinder()
				.getContainingClass(thisV);
		assertTrue(containingClass == null);
	}

	/**
	 * Test the type for new instance
	 */
	public void testCannotType() {
		New n = FjFactory.eINSTANCE.createNew();

		TypeResult typeResult = fixture.getType(n);
		assertEquals("cannot type", typeResult.getDiagnostic());
	}

	/**
	 * Test the typability of the expression
	 * 
	 * <pre>
	 * this.n()
	 * </pre>
	 * 
	 * where
	 * 
	 * <pre>
	 * class A {
	 * 	F m() {
	 * 		return this.n();
	 * 	}
	 * 
	 * 	F m() {
	 * 		return new F();
	 * 	}
	 * }
	 * </pre>
	 * 
	 * @throws IOException
	 */
	public void testMethodSelectionOnThis() throws IOException {
		Program program = getProgramFromString("class F {} class A { F f; F m() { return this.n(); } F n() { return new F(); }}");

		Expression exp = program.getClasses().get(1).getMethods().get(0)
				.getBody().getExpression();

		TypeResult typeResult = fixture.getType(exp);
		System.out.println("type: " + typeResult);
		assertTrue(typeResult.getType() != null);
		assertEquals("F", TypeUtils.typeToString(typeResult.getType()));
	}

	/**
	 * Test the untypability of field selection on a basic type
	 * 
	 * @throws IOException
	 */
	public void testFieldSelectionOnBasicTypes() throws IOException {
		EList<Diagnostic> errors = getErrorProgramFromString("class A { int f; int g; int m() { return this.f.g; }}");

		System.out.println("errors: " + errors);
		assertEquals(1, errors.size());
		System.out.println("error: " + errors.get(0).getMessage());
		assertEquals("Couldn't resolve reference to Field 'g'.", errors.get(0)
				.getMessage());
	}

	/**
	 * Test the untypability of method selection on a basic type
	 * 
	 * @throws IOException
	 */
	public void testMethodSelectionOnBasicTypes() throws IOException {
		EList<Diagnostic> errors = getErrorProgramFromString("class A { int f; int m() { return this.f.m(); }}");

		System.out.println("errors: " + errors);
		assertEquals(1, errors.size());
		System.out.println("error: " + errors.get(0).getMessage());
		assertEquals("Couldn't resolve reference to Method 'm'.", errors.get(0)
				.getMessage());
	}

	public void testField() throws IOException {
		Program program = getProgramFromString("class A { Object f;}");
		TypedElement field = program.getClasses().get(0).getFields().get(0);
		TypeResult typeResult = fixture.getType(field);
		assertTrue(typeResult.getClassref() != null);
		assertEquals("Object", typeResult.getClassref().getName());
	}

	public void testParameter() throws IOException {
		Program program = getProgramFromString("class A { A m(A a, Object o) {return a;} }");
		TypedElement field = program.getClasses().get(0).getMethods().get(0)
				.getParams().get(0);
		TypeResult typeResult = fixture.getType(field);
		assertTrue(typeResult.getClassref() != null);
		assertEquals("A", typeResult.getClassref().getName());
		field = program.getClasses().get(0).getMethods().get(0).getParams()
				.get(1);
		typeResult = fixture.getType(field);
		assertTrue(typeResult.getClassref() != null);
		assertEquals("Object", typeResult.getClassref().getName());
	}

	public void testVariable() throws IOException {
		Program program = getProgramFromString("class A { A m(A a, Object o) {return a;} }");
		Expression variable = program.getClasses().get(0).getMethods().get(0)
				.getBody().getExpression();
		TypeResult typeResult = fixture.getType(variable);
		assertTrue(typeResult.getClassref() != null);
		assertEquals("A", typeResult.getClassref().getName());
	}
	
	public void testConstant() throws IOException {
		StringConstant stringConstant = FjFactory.eINSTANCE.createStringConstant();
		stringConstant.setConstant("foobar");
		TypeResult typeResult = fixture.getType(stringConstant);
		assertTrue(typeResult.getBasicType() != null);
		assertEquals("String", typeResult.getBasicType());
		
		IntConstant intConstant = FjFactory.eINSTANCE.createIntConstant();
		intConstant.setConstant(10);
		typeResult = fixture.getType(intConstant);
		assertTrue(typeResult.getBasicType() != null);
		assertEquals("int", typeResult.getBasicType());
		
		BoolConstant boolConstant = FjFactory.eINSTANCE.createBoolConstant();
		boolConstant.setConstant("true");
		typeResult = fixture.getType(boolConstant);
		assertTrue(typeResult.getBasicType() != null);
		assertEquals("boolean", typeResult.getBasicType());
		
		Resource resource = loadFromString
			("class A { int m() { return 10; } boolean n() {return false;} String p() { return \"foo\";}}");
		EList<Diagnostic> errors = resource.getErrors();
		assertTrue(errors.size() == 0);
	}
	
	public void testMethod() throws IOException {
		Program program = getProgramFromString("class A { Object m() {return this.m();} }");
		Method method = program.getClasses().get(0).getMethods().get(0);
		TypeResult typeResult = fixture.getType(method);
		assertTrue(typeResult.getClassref() != null);
		assertEquals("Object", typeResult.getClassref().getName());
	}
}
