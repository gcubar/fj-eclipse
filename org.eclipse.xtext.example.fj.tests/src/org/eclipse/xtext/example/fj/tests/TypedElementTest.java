/**
 * <copyright>
 * </copyright>
 *
 * $Id: TypedElementTest.java,v 1.1 2009-11-02 11:25:13 bettini Exp $
 */
package org.eclipse.xtext.example.fj.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Typed Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypedElementTest extends TestCase {

	/**
	 * The fixture for this Typed Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypedElement fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TypedElementTest.class);
	}

	/**
	 * Constructs a new Typed Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypedElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Typed Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(TypedElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Typed Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypedElement getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FjFactory.eINSTANCE.createTypedElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TypedElementTest
