/**
 * 
 */
package org.eclipse.xtext.example.fj.tests;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.linker.FJLinkingResource;
import org.eclipse.xtext.example.typing.FJTypeSystem;
import org.eclipse.xtext.example.util.ClassFactory;
import org.eclipse.xtext.example.util.ClassNameComparator;
import org.eclipse.xtext.example.util.ClassSet;

import org.eclipse.emf.ecore.resource.Resource;

/**
 * @author bettini
 * 
 *         Tests for utility functions
 */
public class UtilTest extends TestWithLoader {
	FJTypeSystem fixture;

	public UtilTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		fixture = new FJTypeSystem();
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testClassComparator() {
		ClassNameComparator comparator = new ClassNameComparator();

		Class c1 = FjFactory.eINSTANCE.createClass();
		c1.setName("c1");
		Class c1bis = FjFactory.eINSTANCE.createClass();
		c1bis.setName("c1");

		assertEquals(0, comparator.compare(c1, c1bis));

		Class c2 = FjFactory.eINSTANCE.createClass();
		c2.setName("c2");

		assertTrue(comparator.compare(c1, c2) < 0);
	}

	public void testClassTreeSet() {
		ClassSet classSet = new ClassSet();

		Class c1 = FjFactory.eINSTANCE.createClass();
		c1.setName("c1");
		Class c1bis = FjFactory.eINSTANCE.createClass();
		c1bis.setName("c1");
		Class c2 = FjFactory.eINSTANCE.createClass();
		c2.setName("c2");

		classSet.add(c1);

		assertTrue(classSet.contains(c1bis));
		assertFalse(classSet.contains(c2));

		// if we add c1bis there should still be one class
		classSet.add(c1bis);

		assertEquals(1, classSet.size());
	}

	public void testCreateClass() {
		Class c1 = ClassFactory.createClass("c1");

		assertEquals("c1", c1.getName());
		assertTrue(c1.getExtends() == null);
		
		// make sure to put everything in a resource
		Resource resource = createResource();
		
		Program program = FjFactory.eINSTANCE.createProgram();
		program.getClasses().add(c1);
		
		resource.getContents().add(program);

		assertEquals(FJLinkingResource.class.getName(), c1.eResource()
				.getClass().getName());
		
		Class c2 = ClassFactory.createClass("c2", c1);
		assertEquals("c2", c2.getName());
		assertEquals("c1", c2.getExtends().getName());
	}
}
