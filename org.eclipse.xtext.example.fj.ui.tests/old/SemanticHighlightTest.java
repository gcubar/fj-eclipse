package org.eclipse.xtext.example.fj.ui.tests;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.eclipse.xtext.example.fj.tests.TestWithLoader;
import org.eclipse.xtext.example.syntaxcoloring.SemanticHighlightingCalculator;
import org.eclipse.xtext.example.syntaxcoloring.SemanticHighlightingConfiguration;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.syntaxcoloring.LightweightPosition;
import org.eclipse.xtext.ui.editor.syntaxcoloring.MergingHighlightedPositionAcceptor;
import org.eclipse.xtext.util.Strings;

public class SemanticHighlightTest extends TestWithLoader {

	private MergingHighlightedPositionAcceptor acceptor;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		acceptor = new MergingHighlightedPositionAcceptor(null);
	}
	
	@Override
	protected void tearDown() throws Exception {
		acceptor = null;
		super.tearDown();
	}

	public SemanticHighlightTest(String name) {
		super(name);
	}

	private void checkPosition(LightweightPosition position, int offset, int length, int timestamp, String... ids) {
		assertNotNull(position);
		if (timestamp >= 0)
			assertEquals("timestamp", timestamp, position.getTimestamp());
		if (offset >= 0)
			assertEquals("offset", offset, position.getOffset());
		if (length >= 0)
			assertEquals("length", length, position.getLength());
		checkIds(ids, position.getIds());
	}

	private void checkIds(String[] expected, String[] actual) {
		assertEquals(Strings.concat(", ", Arrays.asList(actual)) + " vs. " + Strings.concat(", ", Arrays.asList(expected)),
				actual.length, expected.length);
		for(int i = 0; i < actual.length; i++) {
			assertEquals("at index: " + i, expected[i], actual[i]);
		}
	}

	public void testFieldSelectionHighlight() throws IOException {
		Resource resource = loadFromString("class A { A field; A m() { return this.field; }}");
		SemanticHighlightingCalculator semanticHighlightingCalculator = new SemanticHighlightingCalculator();
		semanticHighlightingCalculator.provideHighlightingFor((XtextResource) resource, acceptor);
		List<LightweightPosition> positions = acceptor.getPositions();
		LightweightPosition position = positions.get(0);
		System.out.println("positions: " + position);
		// 39 is the offset of "field" after "this."
		checkPosition(position, 39, 5, 0, SemanticHighlightingConfiguration.FIELD_REF);
	}
	
	public void testFieldSelectionHighlight2() throws IOException {
		Resource resource = loadFromString("class A { Object field; Object m() { return this.field; }} new A(new Object()).field");
		SemanticHighlightingCalculator semanticHighlightingCalculator = new SemanticHighlightingCalculator();
		semanticHighlightingCalculator.provideHighlightingFor((XtextResource) resource, acceptor);
		List<LightweightPosition> positions = acceptor.getPositions();
		LightweightPosition position = positions.get(0);
		LightweightPosition position1 = positions.get(1);
		System.out.println("positions: " + position);
		System.out.println("positions: " + position1);
		checkPosition(position, 49, 5, 0, SemanticHighlightingConfiguration.FIELD_REF);
		checkPosition(position1, 79, 5, 1, SemanticHighlightingConfiguration.FIELD_REF);
	}
}
