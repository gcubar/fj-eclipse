package org.eclipse.xtext.example.fj.ui.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for org.eclipse.xtext.example.fj.ui.tests");
		//$JUnit-BEGIN$
		//suite.addTestSuite(ContentAssistProposalTest.class);
		//suite.addTestSuite(SemanticHighlightTest.class);
		//$JUnit-END$
		return suite;
	}

}
