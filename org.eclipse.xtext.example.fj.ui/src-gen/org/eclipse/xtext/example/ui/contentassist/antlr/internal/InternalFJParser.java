package org.eclipse.xtext.example.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.xtext.parsetree.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.eclipse.xtext.example.services.FJGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalFJParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'int'", "'boolean'", "'String'", "'true'", "'false'", "'class'", "'{'", "'}'", "'extends'", "';'", "'('", "')'", "','", "'return'", "'.'", "'new'", "'this'"
    };
    public static final int RULE_ID=4;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalFJParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFJParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFJParser.tokenNames; }
    public String getGrammarFileName() { return "../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g"; }


     
     	private FJGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(FJGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleProgram"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:62:1: entryRuleProgram : ruleProgram EOF ;
    public final void entryRuleProgram() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:63:1: ( ruleProgram EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:64:1: ruleProgram EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramRule()); 
            }
            pushFollow(FOLLOW_ruleProgram_in_entryRuleProgram67);
            ruleProgram();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProgram74); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProgram"


    // $ANTLR start "ruleProgram"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:71:1: ruleProgram : ( ( rule__Program__Group__0 ) ) ;
    public final void ruleProgram() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:75:2: ( ( ( rule__Program__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:76:1: ( ( rule__Program__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:76:1: ( ( rule__Program__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:77:1: ( rule__Program__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:78:1: ( rule__Program__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:78:2: rule__Program__Group__0
            {
            pushFollow(FOLLOW_rule__Program__Group__0_in_ruleProgram100);
            rule__Program__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProgram"


    // $ANTLR start "entryRuleType"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:90:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:91:1: ( ruleType EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:92:1: ruleType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypeRule()); 
            }
            pushFollow(FOLLOW_ruleType_in_entryRuleType127);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypeRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleType134); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:99:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:103:2: ( ( ( rule__Type__Alternatives ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:104:1: ( ( rule__Type__Alternatives ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:104:1: ( ( rule__Type__Alternatives ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:105:1: ( rule__Type__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypeAccess().getAlternatives()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:106:1: ( rule__Type__Alternatives )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:106:2: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_rule__Type__Alternatives_in_ruleType160);
            rule__Type__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypeAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleBasicType"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:118:1: entryRuleBasicType : ruleBasicType EOF ;
    public final void entryRuleBasicType() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:119:1: ( ruleBasicType EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:120:1: ruleBasicType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicTypeRule()); 
            }
            pushFollow(FOLLOW_ruleBasicType_in_entryRuleBasicType187);
            ruleBasicType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicTypeRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBasicType194); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBasicType"


    // $ANTLR start "ruleBasicType"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:127:1: ruleBasicType : ( ( rule__BasicType__BasicAssignment ) ) ;
    public final void ruleBasicType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:131:2: ( ( ( rule__BasicType__BasicAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:132:1: ( ( rule__BasicType__BasicAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:132:1: ( ( rule__BasicType__BasicAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:133:1: ( rule__BasicType__BasicAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicTypeAccess().getBasicAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:134:1: ( rule__BasicType__BasicAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:134:2: rule__BasicType__BasicAssignment
            {
            pushFollow(FOLLOW_rule__BasicType__BasicAssignment_in_ruleBasicType220);
            rule__BasicType__BasicAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicTypeAccess().getBasicAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBasicType"


    // $ANTLR start "entryRuleClassType"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:146:1: entryRuleClassType : ruleClassType EOF ;
    public final void entryRuleClassType() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:147:1: ( ruleClassType EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:148:1: ruleClassType EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeRule()); 
            }
            pushFollow(FOLLOW_ruleClassType_in_entryRuleClassType247);
            ruleClassType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleClassType254); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassType"


    // $ANTLR start "ruleClassType"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:155:1: ruleClassType : ( ( rule__ClassType__ClassrefAssignment ) ) ;
    public final void ruleClassType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:159:2: ( ( ( rule__ClassType__ClassrefAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:160:1: ( ( rule__ClassType__ClassrefAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:160:1: ( ( rule__ClassType__ClassrefAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:161:1: ( rule__ClassType__ClassrefAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeAccess().getClassrefAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:162:1: ( rule__ClassType__ClassrefAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:162:2: rule__ClassType__ClassrefAssignment
            {
            pushFollow(FOLLOW_rule__ClassType__ClassrefAssignment_in_ruleClassType280);
            rule__ClassType__ClassrefAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeAccess().getClassrefAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassType"


    // $ANTLR start "entryRuleClass"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:176:1: entryRuleClass : ruleClass EOF ;
    public final void entryRuleClass() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:177:1: ( ruleClass EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:178:1: ruleClass EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassRule()); 
            }
            pushFollow(FOLLOW_ruleClass_in_entryRuleClass309);
            ruleClass();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleClass316); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClass"


    // $ANTLR start "ruleClass"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:185:1: ruleClass : ( ( rule__Class__Group__0 ) ) ;
    public final void ruleClass() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:189:2: ( ( ( rule__Class__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:190:1: ( ( rule__Class__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:190:1: ( ( rule__Class__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:191:1: ( rule__Class__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:192:1: ( rule__Class__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:192:2: rule__Class__Group__0
            {
            pushFollow(FOLLOW_rule__Class__Group__0_in_ruleClass342);
            rule__Class__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClass"


    // $ANTLR start "entryRuleField"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:204:1: entryRuleField : ruleField EOF ;
    public final void entryRuleField() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:205:1: ( ruleField EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:206:1: ruleField EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldRule()); 
            }
            pushFollow(FOLLOW_ruleField_in_entryRuleField369);
            ruleField();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleField376); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleField"


    // $ANTLR start "ruleField"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:213:1: ruleField : ( ( rule__Field__Group__0 ) ) ;
    public final void ruleField() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:217:2: ( ( ( rule__Field__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:218:1: ( ( rule__Field__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:218:1: ( ( rule__Field__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:219:1: ( rule__Field__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:220:1: ( rule__Field__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:220:2: rule__Field__Group__0
            {
            pushFollow(FOLLOW_rule__Field__Group__0_in_ruleField402);
            rule__Field__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleField"


    // $ANTLR start "entryRuleParameter"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:232:1: entryRuleParameter : ruleParameter EOF ;
    public final void entryRuleParameter() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:233:1: ( ruleParameter EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:234:1: ruleParameter EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterRule()); 
            }
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter429);
            ruleParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter436); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:241:1: ruleParameter : ( ( rule__Parameter__Group__0 ) ) ;
    public final void ruleParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:245:2: ( ( ( rule__Parameter__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:246:1: ( ( rule__Parameter__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:246:1: ( ( rule__Parameter__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:247:1: ( rule__Parameter__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:248:1: ( rule__Parameter__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:248:2: rule__Parameter__Group__0
            {
            pushFollow(FOLLOW_rule__Parameter__Group__0_in_ruleParameter462);
            rule__Parameter__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleMethod"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:260:1: entryRuleMethod : ruleMethod EOF ;
    public final void entryRuleMethod() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:261:1: ( ruleMethod EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:262:1: ruleMethod EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodRule()); 
            }
            pushFollow(FOLLOW_ruleMethod_in_entryRuleMethod489);
            ruleMethod();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMethod496); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethod"


    // $ANTLR start "ruleMethod"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:269:1: ruleMethod : ( ( rule__Method__Group__0 ) ) ;
    public final void ruleMethod() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:273:2: ( ( ( rule__Method__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:274:1: ( ( rule__Method__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:274:1: ( ( rule__Method__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:275:1: ( rule__Method__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:276:1: ( rule__Method__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:276:2: rule__Method__Group__0
            {
            pushFollow(FOLLOW_rule__Method__Group__0_in_ruleMethod522);
            rule__Method__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethod"


    // $ANTLR start "entryRuleMethodBody"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:288:1: entryRuleMethodBody : ruleMethodBody EOF ;
    public final void entryRuleMethodBody() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:289:1: ( ruleMethodBody EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:290:1: ruleMethodBody EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyRule()); 
            }
            pushFollow(FOLLOW_ruleMethodBody_in_entryRuleMethodBody549);
            ruleMethodBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMethodBody556); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethodBody"


    // $ANTLR start "ruleMethodBody"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:297:1: ruleMethodBody : ( ( rule__MethodBody__Group__0 ) ) ;
    public final void ruleMethodBody() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:301:2: ( ( ( rule__MethodBody__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:302:1: ( ( rule__MethodBody__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:302:1: ( ( rule__MethodBody__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:303:1: ( rule__MethodBody__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:304:1: ( rule__MethodBody__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:304:2: rule__MethodBody__Group__0
            {
            pushFollow(FOLLOW_rule__MethodBody__Group__0_in_ruleMethodBody582);
            rule__MethodBody__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethodBody"


    // $ANTLR start "entryRuleExpression"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:316:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:317:1: ( ruleExpression EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:318:1: ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression609);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression616); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:325:1: ruleExpression : ( ( rule__Expression__Group__0 ) ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:329:2: ( ( ( rule__Expression__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:330:1: ( ( rule__Expression__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:330:1: ( ( rule__Expression__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:331:1: ( rule__Expression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:332:1: ( rule__Expression__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:332:2: rule__Expression__Group__0
            {
            pushFollow(FOLLOW_rule__Expression__Group__0_in_ruleExpression642);
            rule__Expression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleMessage"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:344:1: entryRuleMessage : ruleMessage EOF ;
    public final void entryRuleMessage() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:345:1: ( ruleMessage EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:346:1: ruleMessage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMessageRule()); 
            }
            pushFollow(FOLLOW_ruleMessage_in_entryRuleMessage669);
            ruleMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMessageRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessage676); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:353:1: ruleMessage : ( ( rule__Message__Alternatives ) ) ;
    public final void ruleMessage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:357:2: ( ( ( rule__Message__Alternatives ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:358:1: ( ( rule__Message__Alternatives ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:358:1: ( ( rule__Message__Alternatives ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:359:1: ( rule__Message__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMessageAccess().getAlternatives()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:360:1: ( rule__Message__Alternatives )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:360:2: rule__Message__Alternatives
            {
            pushFollow(FOLLOW_rule__Message__Alternatives_in_ruleMessage702);
            rule__Message__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMessageAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleMethodCall"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:372:1: entryRuleMethodCall : ruleMethodCall EOF ;
    public final void entryRuleMethodCall() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:373:1: ( ruleMethodCall EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:374:1: ruleMethodCall EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallRule()); 
            }
            pushFollow(FOLLOW_ruleMethodCall_in_entryRuleMethodCall729);
            ruleMethodCall();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMethodCall736); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMethodCall"


    // $ANTLR start "ruleMethodCall"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:381:1: ruleMethodCall : ( ( rule__MethodCall__Group__0 ) ) ;
    public final void ruleMethodCall() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:385:2: ( ( ( rule__MethodCall__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:386:1: ( ( rule__MethodCall__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:386:1: ( ( rule__MethodCall__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:387:1: ( rule__MethodCall__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:388:1: ( rule__MethodCall__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:388:2: rule__MethodCall__Group__0
            {
            pushFollow(FOLLOW_rule__MethodCall__Group__0_in_ruleMethodCall762);
            rule__MethodCall__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMethodCall"


    // $ANTLR start "entryRuleFieldSelection"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:400:1: entryRuleFieldSelection : ruleFieldSelection EOF ;
    public final void entryRuleFieldSelection() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:401:1: ( ruleFieldSelection EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:402:1: ruleFieldSelection EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionRule()); 
            }
            pushFollow(FOLLOW_ruleFieldSelection_in_entryRuleFieldSelection789);
            ruleFieldSelection();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFieldSelection796); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFieldSelection"


    // $ANTLR start "ruleFieldSelection"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:409:1: ruleFieldSelection : ( ( rule__FieldSelection__NameAssignment ) ) ;
    public final void ruleFieldSelection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:413:2: ( ( ( rule__FieldSelection__NameAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:414:1: ( ( rule__FieldSelection__NameAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:414:1: ( ( rule__FieldSelection__NameAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:415:1: ( rule__FieldSelection__NameAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionAccess().getNameAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:416:1: ( rule__FieldSelection__NameAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:416:2: rule__FieldSelection__NameAssignment
            {
            pushFollow(FOLLOW_rule__FieldSelection__NameAssignment_in_ruleFieldSelection822);
            rule__FieldSelection__NameAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionAccess().getNameAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFieldSelection"


    // $ANTLR start "entryRuleTerminalExpression"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:428:1: entryRuleTerminalExpression : ruleTerminalExpression EOF ;
    public final void entryRuleTerminalExpression() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:429:1: ( ruleTerminalExpression EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:430:1: ruleTerminalExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleTerminalExpression_in_entryRuleTerminalExpression849);
            ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalExpressionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTerminalExpression856); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTerminalExpression"


    // $ANTLR start "ruleTerminalExpression"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:437:1: ruleTerminalExpression : ( ( rule__TerminalExpression__Alternatives ) ) ;
    public final void ruleTerminalExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:441:2: ( ( ( rule__TerminalExpression__Alternatives ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:442:1: ( ( rule__TerminalExpression__Alternatives ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:442:1: ( ( rule__TerminalExpression__Alternatives ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:443:1: ( rule__TerminalExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTerminalExpressionAccess().getAlternatives()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:444:1: ( rule__TerminalExpression__Alternatives )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:444:2: rule__TerminalExpression__Alternatives
            {
            pushFollow(FOLLOW_rule__TerminalExpression__Alternatives_in_ruleTerminalExpression882);
            rule__TerminalExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTerminalExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTerminalExpression"


    // $ANTLR start "entryRuleThis"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:456:1: entryRuleThis : ruleThis EOF ;
    public final void entryRuleThis() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:457:1: ( ruleThis EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:458:1: ruleThis EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisRule()); 
            }
            pushFollow(FOLLOW_ruleThis_in_entryRuleThis909);
            ruleThis();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleThis916); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleThis"


    // $ANTLR start "ruleThis"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:465:1: ruleThis : ( ( rule__This__VariableAssignment ) ) ;
    public final void ruleThis() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:469:2: ( ( ( rule__This__VariableAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:470:1: ( ( rule__This__VariableAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:470:1: ( ( rule__This__VariableAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:471:1: ( rule__This__VariableAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisAccess().getVariableAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:472:1: ( rule__This__VariableAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:472:2: rule__This__VariableAssignment
            {
            pushFollow(FOLLOW_rule__This__VariableAssignment_in_ruleThis942);
            rule__This__VariableAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisAccess().getVariableAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleThis"


    // $ANTLR start "entryRuleVariable"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:484:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:485:1: ( ruleVariable EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:486:1: ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_ruleVariable_in_entryRuleVariable969);
            ruleVariable();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariable976); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:493:1: ruleVariable : ( ( rule__Variable__ParamrefAssignment ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:497:2: ( ( ( rule__Variable__ParamrefAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:498:1: ( ( rule__Variable__ParamrefAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:498:1: ( ( rule__Variable__ParamrefAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:499:1: ( rule__Variable__ParamrefAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getParamrefAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:500:1: ( rule__Variable__ParamrefAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:500:2: rule__Variable__ParamrefAssignment
            {
            pushFollow(FOLLOW_rule__Variable__ParamrefAssignment_in_ruleVariable1002);
            rule__Variable__ParamrefAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getParamrefAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleNew"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:512:1: entryRuleNew : ruleNew EOF ;
    public final void entryRuleNew() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:513:1: ( ruleNew EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:514:1: ruleNew EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewRule()); 
            }
            pushFollow(FOLLOW_ruleNew_in_entryRuleNew1029);
            ruleNew();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNew1036); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNew"


    // $ANTLR start "ruleNew"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:521:1: ruleNew : ( ( rule__New__Group__0 ) ) ;
    public final void ruleNew() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:525:2: ( ( ( rule__New__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:526:1: ( ( rule__New__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:526:1: ( ( rule__New__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:527:1: ( rule__New__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:528:1: ( rule__New__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:528:2: rule__New__Group__0
            {
            pushFollow(FOLLOW_rule__New__Group__0_in_ruleNew1062);
            rule__New__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNew"


    // $ANTLR start "entryRuleCast"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:540:1: entryRuleCast : ruleCast EOF ;
    public final void entryRuleCast() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:541:1: ( ruleCast EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:542:1: ruleCast EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastRule()); 
            }
            pushFollow(FOLLOW_ruleCast_in_entryRuleCast1089);
            ruleCast();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCast1096); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCast"


    // $ANTLR start "ruleCast"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:549:1: ruleCast : ( ( rule__Cast__Group__0 ) ) ;
    public final void ruleCast() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:553:2: ( ( ( rule__Cast__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:554:1: ( ( rule__Cast__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:554:1: ( ( rule__Cast__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:555:1: ( rule__Cast__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:556:1: ( rule__Cast__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:556:2: rule__Cast__Group__0
            {
            pushFollow(FOLLOW_rule__Cast__Group__0_in_ruleCast1122);
            rule__Cast__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCast"


    // $ANTLR start "entryRuleParen"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:568:1: entryRuleParen : ruleParen EOF ;
    public final void entryRuleParen() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:569:1: ( ruleParen EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:570:1: ruleParen EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenRule()); 
            }
            pushFollow(FOLLOW_ruleParen_in_entryRuleParen1149);
            ruleParen();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleParen1156); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParen"


    // $ANTLR start "ruleParen"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:577:1: ruleParen : ( ( rule__Paren__Group__0 ) ) ;
    public final void ruleParen() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:581:2: ( ( ( rule__Paren__Group__0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:582:1: ( ( rule__Paren__Group__0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:582:1: ( ( rule__Paren__Group__0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:583:1: ( rule__Paren__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getGroup()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:584:1: ( rule__Paren__Group__0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:584:2: rule__Paren__Group__0
            {
            pushFollow(FOLLOW_rule__Paren__Group__0_in_ruleParen1182);
            rule__Paren__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParen"


    // $ANTLR start "entryRuleConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:596:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:597:1: ( ruleConstant EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:598:1: ruleConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantRule()); 
            }
            pushFollow(FOLLOW_ruleConstant_in_entryRuleConstant1209);
            ruleConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstant1216); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:605:1: ruleConstant : ( ( rule__Constant__Alternatives ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:609:2: ( ( ( rule__Constant__Alternatives ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:610:1: ( ( rule__Constant__Alternatives ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:610:1: ( ( rule__Constant__Alternatives ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:611:1: ( rule__Constant__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantAccess().getAlternatives()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:612:1: ( rule__Constant__Alternatives )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:612:2: rule__Constant__Alternatives
            {
            pushFollow(FOLLOW_rule__Constant__Alternatives_in_ruleConstant1242);
            rule__Constant__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleStringConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:624:1: entryRuleStringConstant : ruleStringConstant EOF ;
    public final void entryRuleStringConstant() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:625:1: ( ruleStringConstant EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:626:1: ruleStringConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringConstantRule()); 
            }
            pushFollow(FOLLOW_ruleStringConstant_in_entryRuleStringConstant1269);
            ruleStringConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringConstantRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStringConstant1276); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringConstant"


    // $ANTLR start "ruleStringConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:633:1: ruleStringConstant : ( ( rule__StringConstant__ConstantAssignment ) ) ;
    public final void ruleStringConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:637:2: ( ( ( rule__StringConstant__ConstantAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:638:1: ( ( rule__StringConstant__ConstantAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:638:1: ( ( rule__StringConstant__ConstantAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:639:1: ( rule__StringConstant__ConstantAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringConstantAccess().getConstantAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:640:1: ( rule__StringConstant__ConstantAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:640:2: rule__StringConstant__ConstantAssignment
            {
            pushFollow(FOLLOW_rule__StringConstant__ConstantAssignment_in_ruleStringConstant1302);
            rule__StringConstant__ConstantAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringConstantAccess().getConstantAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringConstant"


    // $ANTLR start "entryRuleIntConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:652:1: entryRuleIntConstant : ruleIntConstant EOF ;
    public final void entryRuleIntConstant() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:653:1: ( ruleIntConstant EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:654:1: ruleIntConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntConstantRule()); 
            }
            pushFollow(FOLLOW_ruleIntConstant_in_entryRuleIntConstant1329);
            ruleIntConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntConstantRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntConstant1336); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntConstant"


    // $ANTLR start "ruleIntConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:661:1: ruleIntConstant : ( ( rule__IntConstant__ConstantAssignment ) ) ;
    public final void ruleIntConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:665:2: ( ( ( rule__IntConstant__ConstantAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:666:1: ( ( rule__IntConstant__ConstantAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:666:1: ( ( rule__IntConstant__ConstantAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:667:1: ( rule__IntConstant__ConstantAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntConstantAccess().getConstantAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:668:1: ( rule__IntConstant__ConstantAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:668:2: rule__IntConstant__ConstantAssignment
            {
            pushFollow(FOLLOW_rule__IntConstant__ConstantAssignment_in_ruleIntConstant1362);
            rule__IntConstant__ConstantAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntConstantAccess().getConstantAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntConstant"


    // $ANTLR start "entryRuleBoolConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:680:1: entryRuleBoolConstant : ruleBoolConstant EOF ;
    public final void entryRuleBoolConstant() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:681:1: ( ruleBoolConstant EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:682:1: ruleBoolConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBoolConstantRule()); 
            }
            pushFollow(FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant1389);
            ruleBoolConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBoolConstantRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBoolConstant1396); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBoolConstant"


    // $ANTLR start "ruleBoolConstant"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:689:1: ruleBoolConstant : ( ( rule__BoolConstant__ConstantAssignment ) ) ;
    public final void ruleBoolConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:693:2: ( ( ( rule__BoolConstant__ConstantAssignment ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:694:1: ( ( rule__BoolConstant__ConstantAssignment ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:694:1: ( ( rule__BoolConstant__ConstantAssignment ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:695:1: ( rule__BoolConstant__ConstantAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBoolConstantAccess().getConstantAssignment()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:696:1: ( rule__BoolConstant__ConstantAssignment )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:696:2: rule__BoolConstant__ConstantAssignment
            {
            pushFollow(FOLLOW_rule__BoolConstant__ConstantAssignment_in_ruleBoolConstant1422);
            rule__BoolConstant__ConstantAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBoolConstantAccess().getConstantAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBoolConstant"


    // $ANTLR start "entryRuleArgument"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:708:1: entryRuleArgument : ruleArgument EOF ;
    public final void entryRuleArgument() throws RecognitionException {
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:709:1: ( ruleArgument EOF )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:710:1: ruleArgument EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getArgumentRule()); 
            }
            pushFollow(FOLLOW_ruleArgument_in_entryRuleArgument1449);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getArgumentRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleArgument1456); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArgument"


    // $ANTLR start "ruleArgument"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:717:1: ruleArgument : ( ruleExpression ) ;
    public final void ruleArgument() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:721:2: ( ( ruleExpression ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:722:1: ( ruleExpression )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:722:1: ( ruleExpression )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:723:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getArgumentAccess().getExpressionParserRuleCall()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleArgument1482);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getArgumentAccess().getExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArgument"


    // $ANTLR start "rule__Type__Alternatives"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:736:1: rule__Type__Alternatives : ( ( ruleBasicType ) | ( ruleClassType ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:740:1: ( ( ruleBasicType ) | ( ruleClassType ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=11 && LA1_0<=13)) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_ID) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:741:1: ( ruleBasicType )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:741:1: ( ruleBasicType )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:742:1: ruleBasicType
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypeAccess().getBasicTypeParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleBasicType_in_rule__Type__Alternatives1517);
                    ruleBasicType();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypeAccess().getBasicTypeParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:747:6: ( ruleClassType )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:747:6: ( ruleClassType )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:748:1: ruleClassType
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTypeAccess().getClassTypeParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleClassType_in_rule__Type__Alternatives1534);
                    ruleClassType();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTypeAccess().getClassTypeParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__BasicType__BasicAlternatives_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:758:1: rule__BasicType__BasicAlternatives_0 : ( ( 'int' ) | ( 'boolean' ) | ( 'String' ) );
    public final void rule__BasicType__BasicAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:762:1: ( ( 'int' ) | ( 'boolean' ) | ( 'String' ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:763:1: ( 'int' )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:763:1: ( 'int' )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:764:1: 'int'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicTypeAccess().getBasicIntKeyword_0_0()); 
                    }
                    match(input,11,FOLLOW_11_in_rule__BasicType__BasicAlternatives_01567); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicTypeAccess().getBasicIntKeyword_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:771:6: ( 'boolean' )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:771:6: ( 'boolean' )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:772:1: 'boolean'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicTypeAccess().getBasicBooleanKeyword_0_1()); 
                    }
                    match(input,12,FOLLOW_12_in_rule__BasicType__BasicAlternatives_01587); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicTypeAccess().getBasicBooleanKeyword_0_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:779:6: ( 'String' )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:779:6: ( 'String' )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:780:1: 'String'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicTypeAccess().getBasicStringKeyword_0_2()); 
                    }
                    match(input,13,FOLLOW_13_in_rule__BasicType__BasicAlternatives_01607); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicTypeAccess().getBasicStringKeyword_0_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicType__BasicAlternatives_0"


    // $ANTLR start "rule__Message__Alternatives"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:793:1: rule__Message__Alternatives : ( ( ruleMethodCall ) | ( ruleFieldSelection ) );
    public final void rule__Message__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:797:1: ( ( ruleMethodCall ) | ( ruleFieldSelection ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==21) ) {
                    alt3=1;
                }
                else if ( (LA3_1==EOF||LA3_1==20||(LA3_1>=22 && LA3_1<=23)||LA3_1==25) ) {
                    alt3=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:798:1: ( ruleMethodCall )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:798:1: ( ruleMethodCall )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:799:1: ruleMethodCall
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMessageAccess().getMethodCallParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleMethodCall_in_rule__Message__Alternatives1642);
                    ruleMethodCall();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMessageAccess().getMethodCallParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:804:6: ( ruleFieldSelection )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:804:6: ( ruleFieldSelection )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:805:1: ruleFieldSelection
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMessageAccess().getFieldSelectionParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleFieldSelection_in_rule__Message__Alternatives1659);
                    ruleFieldSelection();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMessageAccess().getFieldSelectionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__Alternatives"


    // $ANTLR start "rule__TerminalExpression__Alternatives"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:815:1: rule__TerminalExpression__Alternatives : ( ( ruleThis ) | ( ruleVariable ) | ( ruleNew ) | ( ruleCast ) | ( ruleConstant ) | ( ruleParen ) );
    public final void rule__TerminalExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:819:1: ( ( ruleThis ) | ( ruleVariable ) | ( ruleNew ) | ( ruleCast ) | ( ruleConstant ) | ( ruleParen ) )
            int alt4=6;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:820:1: ( ruleThis )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:820:1: ( ruleThis )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:821:1: ruleThis
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getThisParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleThis_in_rule__TerminalExpression__Alternatives1691);
                    ruleThis();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getThisParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:826:6: ( ruleVariable )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:826:6: ( ruleVariable )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:827:1: ruleVariable
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getVariableParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleVariable_in_rule__TerminalExpression__Alternatives1708);
                    ruleVariable();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getVariableParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:832:6: ( ruleNew )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:832:6: ( ruleNew )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:833:1: ruleNew
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getNewParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_ruleNew_in_rule__TerminalExpression__Alternatives1725);
                    ruleNew();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getNewParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:838:6: ( ruleCast )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:838:6: ( ruleCast )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:839:1: ruleCast
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getCastParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_ruleCast_in_rule__TerminalExpression__Alternatives1742);
                    ruleCast();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getCastParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:844:6: ( ruleConstant )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:844:6: ( ruleConstant )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:845:1: ruleConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getConstantParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_ruleConstant_in_rule__TerminalExpression__Alternatives1759);
                    ruleConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getConstantParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:850:6: ( ruleParen )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:850:6: ( ruleParen )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:851:1: ruleParen
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTerminalExpressionAccess().getParenParserRuleCall_5()); 
                    }
                    pushFollow(FOLLOW_ruleParen_in_rule__TerminalExpression__Alternatives1776);
                    ruleParen();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTerminalExpressionAccess().getParenParserRuleCall_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TerminalExpression__Alternatives"


    // $ANTLR start "rule__Constant__Alternatives"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:861:1: rule__Constant__Alternatives : ( ( ruleIntConstant ) | ( ruleBoolConstant ) | ( ruleStringConstant ) );
    public final void rule__Constant__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:865:1: ( ( ruleIntConstant ) | ( ruleBoolConstant ) | ( ruleStringConstant ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt5=1;
                }
                break;
            case 14:
            case 15:
                {
                alt5=2;
                }
                break;
            case RULE_STRING:
                {
                alt5=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:866:1: ( ruleIntConstant )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:866:1: ( ruleIntConstant )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:867:1: ruleIntConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getIntConstantParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleIntConstant_in_rule__Constant__Alternatives1808);
                    ruleIntConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getIntConstantParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:872:6: ( ruleBoolConstant )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:872:6: ( ruleBoolConstant )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:873:1: ruleBoolConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getBoolConstantParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleBoolConstant_in_rule__Constant__Alternatives1825);
                    ruleBoolConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getBoolConstantParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:878:6: ( ruleStringConstant )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:878:6: ( ruleStringConstant )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:879:1: ruleStringConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getStringConstantParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_ruleStringConstant_in_rule__Constant__Alternatives1842);
                    ruleStringConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getStringConstantParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Alternatives"


    // $ANTLR start "rule__BoolConstant__ConstantAlternatives_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:889:1: rule__BoolConstant__ConstantAlternatives_0 : ( ( 'true' ) | ( 'false' ) );
    public final void rule__BoolConstant__ConstantAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:893:1: ( ( 'true' ) | ( 'false' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==14) ) {
                alt6=1;
            }
            else if ( (LA6_0==15) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:894:1: ( 'true' )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:894:1: ( 'true' )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:895:1: 'true'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBoolConstantAccess().getConstantTrueKeyword_0_0()); 
                    }
                    match(input,14,FOLLOW_14_in_rule__BoolConstant__ConstantAlternatives_01875); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBoolConstantAccess().getConstantTrueKeyword_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:902:6: ( 'false' )
                    {
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:902:6: ( 'false' )
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:903:1: 'false'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBoolConstantAccess().getConstantFalseKeyword_0_1()); 
                    }
                    match(input,15,FOLLOW_15_in_rule__BoolConstant__ConstantAlternatives_01895); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBoolConstantAccess().getConstantFalseKeyword_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoolConstant__ConstantAlternatives_0"


    // $ANTLR start "rule__Program__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:917:1: rule__Program__Group__0 : rule__Program__Group__0__Impl rule__Program__Group__1 ;
    public final void rule__Program__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:921:1: ( rule__Program__Group__0__Impl rule__Program__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:922:2: rule__Program__Group__0__Impl rule__Program__Group__1
            {
            pushFollow(FOLLOW_rule__Program__Group__0__Impl_in_rule__Program__Group__01927);
            rule__Program__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Program__Group__1_in_rule__Program__Group__01930);
            rule__Program__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__0"


    // $ANTLR start "rule__Program__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:929:1: rule__Program__Group__0__Impl : ( ( rule__Program__ClassesAssignment_0 )* ) ;
    public final void rule__Program__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:933:1: ( ( ( rule__Program__ClassesAssignment_0 )* ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:934:1: ( ( rule__Program__ClassesAssignment_0 )* )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:934:1: ( ( rule__Program__ClassesAssignment_0 )* )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:935:1: ( rule__Program__ClassesAssignment_0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getClassesAssignment_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:936:1: ( rule__Program__ClassesAssignment_0 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:936:2: rule__Program__ClassesAssignment_0
            	    {
            	    pushFollow(FOLLOW_rule__Program__ClassesAssignment_0_in_rule__Program__Group__0__Impl1957);
            	    rule__Program__ClassesAssignment_0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getClassesAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__0__Impl"


    // $ANTLR start "rule__Program__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:946:1: rule__Program__Group__1 : rule__Program__Group__1__Impl ;
    public final void rule__Program__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:950:1: ( rule__Program__Group__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:951:2: rule__Program__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Program__Group__1__Impl_in_rule__Program__Group__11988);
            rule__Program__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__1"


    // $ANTLR start "rule__Program__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:957:1: rule__Program__Group__1__Impl : ( ( rule__Program__MainAssignment_1 )? ) ;
    public final void rule__Program__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:961:1: ( ( ( rule__Program__MainAssignment_1 )? ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:962:1: ( ( rule__Program__MainAssignment_1 )? )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:962:1: ( ( rule__Program__MainAssignment_1 )? )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:963:1: ( rule__Program__MainAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getMainAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:964:1: ( rule__Program__MainAssignment_1 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( ((LA8_0>=RULE_ID && LA8_0<=RULE_INT)||(LA8_0>=14 && LA8_0<=15)||LA8_0==21||(LA8_0>=26 && LA8_0<=27)) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:964:2: rule__Program__MainAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Program__MainAssignment_1_in_rule__Program__Group__1__Impl2015);
                    rule__Program__MainAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getMainAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__Group__1__Impl"


    // $ANTLR start "rule__Class__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:978:1: rule__Class__Group__0 : rule__Class__Group__0__Impl rule__Class__Group__1 ;
    public final void rule__Class__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:982:1: ( rule__Class__Group__0__Impl rule__Class__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:983:2: rule__Class__Group__0__Impl rule__Class__Group__1
            {
            pushFollow(FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__02050);
            rule__Class__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Class__Group__1_in_rule__Class__Group__02053);
            rule__Class__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0"


    // $ANTLR start "rule__Class__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:990:1: rule__Class__Group__0__Impl : ( 'class' ) ;
    public final void rule__Class__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:994:1: ( ( 'class' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:995:1: ( 'class' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:995:1: ( 'class' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:996:1: 'class'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getClassKeyword_0()); 
            }
            match(input,16,FOLLOW_16_in_rule__Class__Group__0__Impl2081); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getClassKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__0__Impl"


    // $ANTLR start "rule__Class__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1009:1: rule__Class__Group__1 : rule__Class__Group__1__Impl rule__Class__Group__2 ;
    public final void rule__Class__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1013:1: ( rule__Class__Group__1__Impl rule__Class__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1014:2: rule__Class__Group__1__Impl rule__Class__Group__2
            {
            pushFollow(FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__12112);
            rule__Class__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Class__Group__2_in_rule__Class__Group__12115);
            rule__Class__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1"


    // $ANTLR start "rule__Class__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1021:1: rule__Class__Group__1__Impl : ( ( rule__Class__NameAssignment_1 ) ) ;
    public final void rule__Class__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1025:1: ( ( ( rule__Class__NameAssignment_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1026:1: ( ( rule__Class__NameAssignment_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1026:1: ( ( rule__Class__NameAssignment_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1027:1: ( rule__Class__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getNameAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1028:1: ( rule__Class__NameAssignment_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1028:2: rule__Class__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Class__NameAssignment_1_in_rule__Class__Group__1__Impl2142);
            rule__Class__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__1__Impl"


    // $ANTLR start "rule__Class__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1038:1: rule__Class__Group__2 : rule__Class__Group__2__Impl rule__Class__Group__3 ;
    public final void rule__Class__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1042:1: ( rule__Class__Group__2__Impl rule__Class__Group__3 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1043:2: rule__Class__Group__2__Impl rule__Class__Group__3
            {
            pushFollow(FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__22172);
            rule__Class__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Class__Group__3_in_rule__Class__Group__22175);
            rule__Class__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2"


    // $ANTLR start "rule__Class__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1050:1: rule__Class__Group__2__Impl : ( ( rule__Class__Group_2__0 )? ) ;
    public final void rule__Class__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1054:1: ( ( ( rule__Class__Group_2__0 )? ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1055:1: ( ( rule__Class__Group_2__0 )? )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1055:1: ( ( rule__Class__Group_2__0 )? )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1056:1: ( rule__Class__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getGroup_2()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1057:1: ( rule__Class__Group_2__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1057:2: rule__Class__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Class__Group_2__0_in_rule__Class__Group__2__Impl2202);
                    rule__Class__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__2__Impl"


    // $ANTLR start "rule__Class__Group__3"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1067:1: rule__Class__Group__3 : rule__Class__Group__3__Impl rule__Class__Group__4 ;
    public final void rule__Class__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1071:1: ( rule__Class__Group__3__Impl rule__Class__Group__4 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1072:2: rule__Class__Group__3__Impl rule__Class__Group__4
            {
            pushFollow(FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__32233);
            rule__Class__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Class__Group__4_in_rule__Class__Group__32236);
            rule__Class__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3"


    // $ANTLR start "rule__Class__Group__3__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1079:1: rule__Class__Group__3__Impl : ( '{' ) ;
    public final void rule__Class__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1083:1: ( ( '{' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1084:1: ( '{' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1084:1: ( '{' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1085:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,17,FOLLOW_17_in_rule__Class__Group__3__Impl2264); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__3__Impl"


    // $ANTLR start "rule__Class__Group__4"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1098:1: rule__Class__Group__4 : rule__Class__Group__4__Impl rule__Class__Group__5 ;
    public final void rule__Class__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1102:1: ( rule__Class__Group__4__Impl rule__Class__Group__5 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1103:2: rule__Class__Group__4__Impl rule__Class__Group__5
            {
            pushFollow(FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__42295);
            rule__Class__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Class__Group__5_in_rule__Class__Group__42298);
            rule__Class__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4"


    // $ANTLR start "rule__Class__Group__4__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1110:1: rule__Class__Group__4__Impl : ( ( rule__Class__FieldsAssignment_4 )* ) ;
    public final void rule__Class__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1114:1: ( ( ( rule__Class__FieldsAssignment_4 )* ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1115:1: ( ( rule__Class__FieldsAssignment_4 )* )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1115:1: ( ( rule__Class__FieldsAssignment_4 )* )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1116:1: ( rule__Class__FieldsAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getFieldsAssignment_4()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1117:1: ( rule__Class__FieldsAssignment_4 )*
            loop10:
            do {
                int alt10=2;
                switch ( input.LA(1) ) {
                case 11:
                    {
                    int LA10_1 = input.LA(2);

                    if ( (LA10_1==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;
                case 12:
                    {
                    int LA10_2 = input.LA(2);

                    if ( (LA10_2==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;
                case 13:
                    {
                    int LA10_3 = input.LA(2);

                    if ( (LA10_3==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;
                case RULE_ID:
                    {
                    int LA10_4 = input.LA(2);

                    if ( (LA10_4==RULE_ID) ) {
                        int LA10_6 = input.LA(3);

                        if ( (LA10_6==20) ) {
                            alt10=1;
                        }


                    }


                    }
                    break;

                }

                switch (alt10) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1117:2: rule__Class__FieldsAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__Class__FieldsAssignment_4_in_rule__Class__Group__4__Impl2325);
            	    rule__Class__FieldsAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getFieldsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__4__Impl"


    // $ANTLR start "rule__Class__Group__5"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1127:1: rule__Class__Group__5 : rule__Class__Group__5__Impl rule__Class__Group__6 ;
    public final void rule__Class__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1131:1: ( rule__Class__Group__5__Impl rule__Class__Group__6 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1132:2: rule__Class__Group__5__Impl rule__Class__Group__6
            {
            pushFollow(FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__52356);
            rule__Class__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Class__Group__6_in_rule__Class__Group__52359);
            rule__Class__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5"


    // $ANTLR start "rule__Class__Group__5__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1139:1: rule__Class__Group__5__Impl : ( ( rule__Class__MethodsAssignment_5 )* ) ;
    public final void rule__Class__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1143:1: ( ( ( rule__Class__MethodsAssignment_5 )* ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1144:1: ( ( rule__Class__MethodsAssignment_5 )* )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1144:1: ( ( rule__Class__MethodsAssignment_5 )* )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1145:1: ( rule__Class__MethodsAssignment_5 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getMethodsAssignment_5()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1146:1: ( rule__Class__MethodsAssignment_5 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID||(LA11_0>=11 && LA11_0<=13)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1146:2: rule__Class__MethodsAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__Class__MethodsAssignment_5_in_rule__Class__Group__5__Impl2386);
            	    rule__Class__MethodsAssignment_5();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getMethodsAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__5__Impl"


    // $ANTLR start "rule__Class__Group__6"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1156:1: rule__Class__Group__6 : rule__Class__Group__6__Impl ;
    public final void rule__Class__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1160:1: ( rule__Class__Group__6__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1161:2: rule__Class__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Class__Group__6__Impl_in_rule__Class__Group__62417);
            rule__Class__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__6"


    // $ANTLR start "rule__Class__Group__6__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1167:1: rule__Class__Group__6__Impl : ( '}' ) ;
    public final void rule__Class__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1171:1: ( ( '}' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1172:1: ( '}' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1172:1: ( '}' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1173:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6()); 
            }
            match(input,18,FOLLOW_18_in_rule__Class__Group__6__Impl2445); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group__6__Impl"


    // $ANTLR start "rule__Class__Group_2__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1200:1: rule__Class__Group_2__0 : rule__Class__Group_2__0__Impl rule__Class__Group_2__1 ;
    public final void rule__Class__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1204:1: ( rule__Class__Group_2__0__Impl rule__Class__Group_2__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1205:2: rule__Class__Group_2__0__Impl rule__Class__Group_2__1
            {
            pushFollow(FOLLOW_rule__Class__Group_2__0__Impl_in_rule__Class__Group_2__02490);
            rule__Class__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Class__Group_2__1_in_rule__Class__Group_2__02493);
            rule__Class__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__0"


    // $ANTLR start "rule__Class__Group_2__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1212:1: rule__Class__Group_2__0__Impl : ( 'extends' ) ;
    public final void rule__Class__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1216:1: ( ( 'extends' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1217:1: ( 'extends' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1217:1: ( 'extends' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1218:1: 'extends'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsKeyword_2_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__Class__Group_2__0__Impl2521); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__0__Impl"


    // $ANTLR start "rule__Class__Group_2__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1231:1: rule__Class__Group_2__1 : rule__Class__Group_2__1__Impl ;
    public final void rule__Class__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1235:1: ( rule__Class__Group_2__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1236:2: rule__Class__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Class__Group_2__1__Impl_in_rule__Class__Group_2__12552);
            rule__Class__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__1"


    // $ANTLR start "rule__Class__Group_2__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1242:1: rule__Class__Group_2__1__Impl : ( ( rule__Class__ExtendsAssignment_2_1 ) ) ;
    public final void rule__Class__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1246:1: ( ( ( rule__Class__ExtendsAssignment_2_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1247:1: ( ( rule__Class__ExtendsAssignment_2_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1247:1: ( ( rule__Class__ExtendsAssignment_2_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1248:1: ( rule__Class__ExtendsAssignment_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsAssignment_2_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1249:1: ( rule__Class__ExtendsAssignment_2_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1249:2: rule__Class__ExtendsAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Class__ExtendsAssignment_2_1_in_rule__Class__Group_2__1__Impl2579);
            rule__Class__ExtendsAssignment_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__Group_2__1__Impl"


    // $ANTLR start "rule__Field__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1263:1: rule__Field__Group__0 : rule__Field__Group__0__Impl rule__Field__Group__1 ;
    public final void rule__Field__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1267:1: ( rule__Field__Group__0__Impl rule__Field__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1268:2: rule__Field__Group__0__Impl rule__Field__Group__1
            {
            pushFollow(FOLLOW_rule__Field__Group__0__Impl_in_rule__Field__Group__02613);
            rule__Field__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Field__Group__1_in_rule__Field__Group__02616);
            rule__Field__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__0"


    // $ANTLR start "rule__Field__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1275:1: rule__Field__Group__0__Impl : ( ( rule__Field__TypeAssignment_0 ) ) ;
    public final void rule__Field__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1279:1: ( ( ( rule__Field__TypeAssignment_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1280:1: ( ( rule__Field__TypeAssignment_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1280:1: ( ( rule__Field__TypeAssignment_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1281:1: ( rule__Field__TypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getTypeAssignment_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1282:1: ( rule__Field__TypeAssignment_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1282:2: rule__Field__TypeAssignment_0
            {
            pushFollow(FOLLOW_rule__Field__TypeAssignment_0_in_rule__Field__Group__0__Impl2643);
            rule__Field__TypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getTypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__0__Impl"


    // $ANTLR start "rule__Field__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1292:1: rule__Field__Group__1 : rule__Field__Group__1__Impl rule__Field__Group__2 ;
    public final void rule__Field__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1296:1: ( rule__Field__Group__1__Impl rule__Field__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1297:2: rule__Field__Group__1__Impl rule__Field__Group__2
            {
            pushFollow(FOLLOW_rule__Field__Group__1__Impl_in_rule__Field__Group__12673);
            rule__Field__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Field__Group__2_in_rule__Field__Group__12676);
            rule__Field__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__1"


    // $ANTLR start "rule__Field__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1304:1: rule__Field__Group__1__Impl : ( ( rule__Field__NameAssignment_1 ) ) ;
    public final void rule__Field__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1308:1: ( ( ( rule__Field__NameAssignment_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1309:1: ( ( rule__Field__NameAssignment_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1309:1: ( ( rule__Field__NameAssignment_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1310:1: ( rule__Field__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getNameAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1311:1: ( rule__Field__NameAssignment_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1311:2: rule__Field__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Field__NameAssignment_1_in_rule__Field__Group__1__Impl2703);
            rule__Field__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__1__Impl"


    // $ANTLR start "rule__Field__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1321:1: rule__Field__Group__2 : rule__Field__Group__2__Impl ;
    public final void rule__Field__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1325:1: ( rule__Field__Group__2__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1326:2: rule__Field__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Field__Group__2__Impl_in_rule__Field__Group__22733);
            rule__Field__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__2"


    // $ANTLR start "rule__Field__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1332:1: rule__Field__Group__2__Impl : ( ';' ) ;
    public final void rule__Field__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1336:1: ( ( ';' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1337:1: ( ';' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1337:1: ( ';' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1338:1: ';'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getSemicolonKeyword_2()); 
            }
            match(input,20,FOLLOW_20_in_rule__Field__Group__2__Impl2761); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getSemicolonKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__Group__2__Impl"


    // $ANTLR start "rule__Parameter__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1357:1: rule__Parameter__Group__0 : rule__Parameter__Group__0__Impl rule__Parameter__Group__1 ;
    public final void rule__Parameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1361:1: ( rule__Parameter__Group__0__Impl rule__Parameter__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1362:2: rule__Parameter__Group__0__Impl rule__Parameter__Group__1
            {
            pushFollow(FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__02798);
            rule__Parameter__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__02801);
            rule__Parameter__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0"


    // $ANTLR start "rule__Parameter__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1369:1: rule__Parameter__Group__0__Impl : ( ( rule__Parameter__TypeAssignment_0 ) ) ;
    public final void rule__Parameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1373:1: ( ( ( rule__Parameter__TypeAssignment_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1374:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1374:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1375:1: ( rule__Parameter__TypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getTypeAssignment_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1376:1: ( rule__Parameter__TypeAssignment_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1376:2: rule__Parameter__TypeAssignment_0
            {
            pushFollow(FOLLOW_rule__Parameter__TypeAssignment_0_in_rule__Parameter__Group__0__Impl2828);
            rule__Parameter__TypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getTypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0__Impl"


    // $ANTLR start "rule__Parameter__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1386:1: rule__Parameter__Group__1 : rule__Parameter__Group__1__Impl ;
    public final void rule__Parameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1390:1: ( rule__Parameter__Group__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1391:2: rule__Parameter__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__12858);
            rule__Parameter__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1"


    // $ANTLR start "rule__Parameter__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1397:1: rule__Parameter__Group__1__Impl : ( ( rule__Parameter__NameAssignment_1 ) ) ;
    public final void rule__Parameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1401:1: ( ( ( rule__Parameter__NameAssignment_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1402:1: ( ( rule__Parameter__NameAssignment_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1402:1: ( ( rule__Parameter__NameAssignment_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1403:1: ( rule__Parameter__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1404:1: ( rule__Parameter__NameAssignment_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1404:2: rule__Parameter__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl2885);
            rule__Parameter__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1__Impl"


    // $ANTLR start "rule__Method__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1418:1: rule__Method__Group__0 : rule__Method__Group__0__Impl rule__Method__Group__1 ;
    public final void rule__Method__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1422:1: ( rule__Method__Group__0__Impl rule__Method__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1423:2: rule__Method__Group__0__Impl rule__Method__Group__1
            {
            pushFollow(FOLLOW_rule__Method__Group__0__Impl_in_rule__Method__Group__02919);
            rule__Method__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group__1_in_rule__Method__Group__02922);
            rule__Method__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__0"


    // $ANTLR start "rule__Method__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1430:1: rule__Method__Group__0__Impl : ( ( rule__Method__ReturntypeAssignment_0 ) ) ;
    public final void rule__Method__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1434:1: ( ( ( rule__Method__ReturntypeAssignment_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1435:1: ( ( rule__Method__ReturntypeAssignment_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1435:1: ( ( rule__Method__ReturntypeAssignment_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1436:1: ( rule__Method__ReturntypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getReturntypeAssignment_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1437:1: ( rule__Method__ReturntypeAssignment_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1437:2: rule__Method__ReturntypeAssignment_0
            {
            pushFollow(FOLLOW_rule__Method__ReturntypeAssignment_0_in_rule__Method__Group__0__Impl2949);
            rule__Method__ReturntypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getReturntypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__0__Impl"


    // $ANTLR start "rule__Method__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1447:1: rule__Method__Group__1 : rule__Method__Group__1__Impl rule__Method__Group__2 ;
    public final void rule__Method__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1451:1: ( rule__Method__Group__1__Impl rule__Method__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1452:2: rule__Method__Group__1__Impl rule__Method__Group__2
            {
            pushFollow(FOLLOW_rule__Method__Group__1__Impl_in_rule__Method__Group__12979);
            rule__Method__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group__2_in_rule__Method__Group__12982);
            rule__Method__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__1"


    // $ANTLR start "rule__Method__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1459:1: rule__Method__Group__1__Impl : ( ( rule__Method__NameAssignment_1 ) ) ;
    public final void rule__Method__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1463:1: ( ( ( rule__Method__NameAssignment_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1464:1: ( ( rule__Method__NameAssignment_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1464:1: ( ( rule__Method__NameAssignment_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1465:1: ( rule__Method__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getNameAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1466:1: ( rule__Method__NameAssignment_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1466:2: rule__Method__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Method__NameAssignment_1_in_rule__Method__Group__1__Impl3009);
            rule__Method__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__1__Impl"


    // $ANTLR start "rule__Method__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1476:1: rule__Method__Group__2 : rule__Method__Group__2__Impl rule__Method__Group__3 ;
    public final void rule__Method__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1480:1: ( rule__Method__Group__2__Impl rule__Method__Group__3 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1481:2: rule__Method__Group__2__Impl rule__Method__Group__3
            {
            pushFollow(FOLLOW_rule__Method__Group__2__Impl_in_rule__Method__Group__23039);
            rule__Method__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group__3_in_rule__Method__Group__23042);
            rule__Method__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__2"


    // $ANTLR start "rule__Method__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1488:1: rule__Method__Group__2__Impl : ( '(' ) ;
    public final void rule__Method__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1492:1: ( ( '(' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1493:1: ( '(' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1493:1: ( '(' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1494:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getLeftParenthesisKeyword_2()); 
            }
            match(input,21,FOLLOW_21_in_rule__Method__Group__2__Impl3070); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getLeftParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__2__Impl"


    // $ANTLR start "rule__Method__Group__3"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1507:1: rule__Method__Group__3 : rule__Method__Group__3__Impl rule__Method__Group__4 ;
    public final void rule__Method__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1511:1: ( rule__Method__Group__3__Impl rule__Method__Group__4 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1512:2: rule__Method__Group__3__Impl rule__Method__Group__4
            {
            pushFollow(FOLLOW_rule__Method__Group__3__Impl_in_rule__Method__Group__33101);
            rule__Method__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group__4_in_rule__Method__Group__33104);
            rule__Method__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__3"


    // $ANTLR start "rule__Method__Group__3__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1519:1: rule__Method__Group__3__Impl : ( ( rule__Method__Group_3__0 )? ) ;
    public final void rule__Method__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1523:1: ( ( ( rule__Method__Group_3__0 )? ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1524:1: ( ( rule__Method__Group_3__0 )? )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1524:1: ( ( rule__Method__Group_3__0 )? )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1525:1: ( rule__Method__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getGroup_3()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1526:1: ( rule__Method__Group_3__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID||(LA12_0>=11 && LA12_0<=13)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1526:2: rule__Method__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__Method__Group_3__0_in_rule__Method__Group__3__Impl3131);
                    rule__Method__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__3__Impl"


    // $ANTLR start "rule__Method__Group__4"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1536:1: rule__Method__Group__4 : rule__Method__Group__4__Impl rule__Method__Group__5 ;
    public final void rule__Method__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1540:1: ( rule__Method__Group__4__Impl rule__Method__Group__5 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1541:2: rule__Method__Group__4__Impl rule__Method__Group__5
            {
            pushFollow(FOLLOW_rule__Method__Group__4__Impl_in_rule__Method__Group__43162);
            rule__Method__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group__5_in_rule__Method__Group__43165);
            rule__Method__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__4"


    // $ANTLR start "rule__Method__Group__4__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1548:1: rule__Method__Group__4__Impl : ( ')' ) ;
    public final void rule__Method__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1552:1: ( ( ')' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1553:1: ( ')' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1553:1: ( ')' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1554:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,22,FOLLOW_22_in_rule__Method__Group__4__Impl3193); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__4__Impl"


    // $ANTLR start "rule__Method__Group__5"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1567:1: rule__Method__Group__5 : rule__Method__Group__5__Impl rule__Method__Group__6 ;
    public final void rule__Method__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1571:1: ( rule__Method__Group__5__Impl rule__Method__Group__6 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1572:2: rule__Method__Group__5__Impl rule__Method__Group__6
            {
            pushFollow(FOLLOW_rule__Method__Group__5__Impl_in_rule__Method__Group__53224);
            rule__Method__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group__6_in_rule__Method__Group__53227);
            rule__Method__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__5"


    // $ANTLR start "rule__Method__Group__5__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1579:1: rule__Method__Group__5__Impl : ( '{' ) ;
    public final void rule__Method__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1583:1: ( ( '{' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1584:1: ( '{' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1584:1: ( '{' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1585:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getLeftCurlyBracketKeyword_5()); 
            }
            match(input,17,FOLLOW_17_in_rule__Method__Group__5__Impl3255); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getLeftCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__5__Impl"


    // $ANTLR start "rule__Method__Group__6"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1598:1: rule__Method__Group__6 : rule__Method__Group__6__Impl rule__Method__Group__7 ;
    public final void rule__Method__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1602:1: ( rule__Method__Group__6__Impl rule__Method__Group__7 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1603:2: rule__Method__Group__6__Impl rule__Method__Group__7
            {
            pushFollow(FOLLOW_rule__Method__Group__6__Impl_in_rule__Method__Group__63286);
            rule__Method__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group__7_in_rule__Method__Group__63289);
            rule__Method__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__6"


    // $ANTLR start "rule__Method__Group__6__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1610:1: rule__Method__Group__6__Impl : ( ( rule__Method__BodyAssignment_6 ) ) ;
    public final void rule__Method__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1614:1: ( ( ( rule__Method__BodyAssignment_6 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1615:1: ( ( rule__Method__BodyAssignment_6 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1615:1: ( ( rule__Method__BodyAssignment_6 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1616:1: ( rule__Method__BodyAssignment_6 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getBodyAssignment_6()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1617:1: ( rule__Method__BodyAssignment_6 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1617:2: rule__Method__BodyAssignment_6
            {
            pushFollow(FOLLOW_rule__Method__BodyAssignment_6_in_rule__Method__Group__6__Impl3316);
            rule__Method__BodyAssignment_6();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getBodyAssignment_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__6__Impl"


    // $ANTLR start "rule__Method__Group__7"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1627:1: rule__Method__Group__7 : rule__Method__Group__7__Impl ;
    public final void rule__Method__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1631:1: ( rule__Method__Group__7__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1632:2: rule__Method__Group__7__Impl
            {
            pushFollow(FOLLOW_rule__Method__Group__7__Impl_in_rule__Method__Group__73346);
            rule__Method__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__7"


    // $ANTLR start "rule__Method__Group__7__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1638:1: rule__Method__Group__7__Impl : ( '}' ) ;
    public final void rule__Method__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1642:1: ( ( '}' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1643:1: ( '}' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1643:1: ( '}' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1644:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getRightCurlyBracketKeyword_7()); 
            }
            match(input,18,FOLLOW_18_in_rule__Method__Group__7__Impl3374); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getRightCurlyBracketKeyword_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group__7__Impl"


    // $ANTLR start "rule__Method__Group_3__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1673:1: rule__Method__Group_3__0 : rule__Method__Group_3__0__Impl rule__Method__Group_3__1 ;
    public final void rule__Method__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1677:1: ( rule__Method__Group_3__0__Impl rule__Method__Group_3__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1678:2: rule__Method__Group_3__0__Impl rule__Method__Group_3__1
            {
            pushFollow(FOLLOW_rule__Method__Group_3__0__Impl_in_rule__Method__Group_3__03421);
            rule__Method__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group_3__1_in_rule__Method__Group_3__03424);
            rule__Method__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__0"


    // $ANTLR start "rule__Method__Group_3__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1685:1: rule__Method__Group_3__0__Impl : ( ( rule__Method__ParamsAssignment_3_0 ) ) ;
    public final void rule__Method__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1689:1: ( ( ( rule__Method__ParamsAssignment_3_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1690:1: ( ( rule__Method__ParamsAssignment_3_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1690:1: ( ( rule__Method__ParamsAssignment_3_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1691:1: ( rule__Method__ParamsAssignment_3_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsAssignment_3_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1692:1: ( rule__Method__ParamsAssignment_3_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1692:2: rule__Method__ParamsAssignment_3_0
            {
            pushFollow(FOLLOW_rule__Method__ParamsAssignment_3_0_in_rule__Method__Group_3__0__Impl3451);
            rule__Method__ParamsAssignment_3_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsAssignment_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__0__Impl"


    // $ANTLR start "rule__Method__Group_3__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1702:1: rule__Method__Group_3__1 : rule__Method__Group_3__1__Impl ;
    public final void rule__Method__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1706:1: ( rule__Method__Group_3__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1707:2: rule__Method__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__Method__Group_3__1__Impl_in_rule__Method__Group_3__13481);
            rule__Method__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__1"


    // $ANTLR start "rule__Method__Group_3__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1713:1: rule__Method__Group_3__1__Impl : ( ( rule__Method__Group_3_1__0 )* ) ;
    public final void rule__Method__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1717:1: ( ( ( rule__Method__Group_3_1__0 )* ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1718:1: ( ( rule__Method__Group_3_1__0 )* )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1718:1: ( ( rule__Method__Group_3_1__0 )* )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1719:1: ( rule__Method__Group_3_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getGroup_3_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1720:1: ( rule__Method__Group_3_1__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==23) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1720:2: rule__Method__Group_3_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Method__Group_3_1__0_in_rule__Method__Group_3__1__Impl3508);
            	    rule__Method__Group_3_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getGroup_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3__1__Impl"


    // $ANTLR start "rule__Method__Group_3_1__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1734:1: rule__Method__Group_3_1__0 : rule__Method__Group_3_1__0__Impl rule__Method__Group_3_1__1 ;
    public final void rule__Method__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1738:1: ( rule__Method__Group_3_1__0__Impl rule__Method__Group_3_1__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1739:2: rule__Method__Group_3_1__0__Impl rule__Method__Group_3_1__1
            {
            pushFollow(FOLLOW_rule__Method__Group_3_1__0__Impl_in_rule__Method__Group_3_1__03543);
            rule__Method__Group_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Method__Group_3_1__1_in_rule__Method__Group_3_1__03546);
            rule__Method__Group_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__0"


    // $ANTLR start "rule__Method__Group_3_1__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1746:1: rule__Method__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__Method__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1750:1: ( ( ',' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1751:1: ( ',' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1751:1: ( ',' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1752:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getCommaKeyword_3_1_0()); 
            }
            match(input,23,FOLLOW_23_in_rule__Method__Group_3_1__0__Impl3574); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getCommaKeyword_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__0__Impl"


    // $ANTLR start "rule__Method__Group_3_1__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1765:1: rule__Method__Group_3_1__1 : rule__Method__Group_3_1__1__Impl ;
    public final void rule__Method__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1769:1: ( rule__Method__Group_3_1__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1770:2: rule__Method__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Method__Group_3_1__1__Impl_in_rule__Method__Group_3_1__13605);
            rule__Method__Group_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__1"


    // $ANTLR start "rule__Method__Group_3_1__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1776:1: rule__Method__Group_3_1__1__Impl : ( ( rule__Method__ParamsAssignment_3_1_1 ) ) ;
    public final void rule__Method__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1780:1: ( ( ( rule__Method__ParamsAssignment_3_1_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1781:1: ( ( rule__Method__ParamsAssignment_3_1_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1781:1: ( ( rule__Method__ParamsAssignment_3_1_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1782:1: ( rule__Method__ParamsAssignment_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsAssignment_3_1_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1783:1: ( rule__Method__ParamsAssignment_3_1_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1783:2: rule__Method__ParamsAssignment_3_1_1
            {
            pushFollow(FOLLOW_rule__Method__ParamsAssignment_3_1_1_in_rule__Method__Group_3_1__1__Impl3632);
            rule__Method__ParamsAssignment_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsAssignment_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__Group_3_1__1__Impl"


    // $ANTLR start "rule__MethodBody__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1797:1: rule__MethodBody__Group__0 : rule__MethodBody__Group__0__Impl rule__MethodBody__Group__1 ;
    public final void rule__MethodBody__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1801:1: ( rule__MethodBody__Group__0__Impl rule__MethodBody__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1802:2: rule__MethodBody__Group__0__Impl rule__MethodBody__Group__1
            {
            pushFollow(FOLLOW_rule__MethodBody__Group__0__Impl_in_rule__MethodBody__Group__03666);
            rule__MethodBody__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__MethodBody__Group__1_in_rule__MethodBody__Group__03669);
            rule__MethodBody__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__0"


    // $ANTLR start "rule__MethodBody__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1809:1: rule__MethodBody__Group__0__Impl : ( 'return' ) ;
    public final void rule__MethodBody__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1813:1: ( ( 'return' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1814:1: ( 'return' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1814:1: ( 'return' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1815:1: 'return'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getReturnKeyword_0()); 
            }
            match(input,24,FOLLOW_24_in_rule__MethodBody__Group__0__Impl3697); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getReturnKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__0__Impl"


    // $ANTLR start "rule__MethodBody__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1828:1: rule__MethodBody__Group__1 : rule__MethodBody__Group__1__Impl rule__MethodBody__Group__2 ;
    public final void rule__MethodBody__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1832:1: ( rule__MethodBody__Group__1__Impl rule__MethodBody__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1833:2: rule__MethodBody__Group__1__Impl rule__MethodBody__Group__2
            {
            pushFollow(FOLLOW_rule__MethodBody__Group__1__Impl_in_rule__MethodBody__Group__13728);
            rule__MethodBody__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__MethodBody__Group__2_in_rule__MethodBody__Group__13731);
            rule__MethodBody__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__1"


    // $ANTLR start "rule__MethodBody__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1840:1: rule__MethodBody__Group__1__Impl : ( ( rule__MethodBody__ExpressionAssignment_1 ) ) ;
    public final void rule__MethodBody__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1844:1: ( ( ( rule__MethodBody__ExpressionAssignment_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1845:1: ( ( rule__MethodBody__ExpressionAssignment_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1845:1: ( ( rule__MethodBody__ExpressionAssignment_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1846:1: ( rule__MethodBody__ExpressionAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getExpressionAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1847:1: ( rule__MethodBody__ExpressionAssignment_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1847:2: rule__MethodBody__ExpressionAssignment_1
            {
            pushFollow(FOLLOW_rule__MethodBody__ExpressionAssignment_1_in_rule__MethodBody__Group__1__Impl3758);
            rule__MethodBody__ExpressionAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getExpressionAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__1__Impl"


    // $ANTLR start "rule__MethodBody__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1857:1: rule__MethodBody__Group__2 : rule__MethodBody__Group__2__Impl ;
    public final void rule__MethodBody__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1861:1: ( rule__MethodBody__Group__2__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1862:2: rule__MethodBody__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__MethodBody__Group__2__Impl_in_rule__MethodBody__Group__23788);
            rule__MethodBody__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__2"


    // $ANTLR start "rule__MethodBody__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1868:1: rule__MethodBody__Group__2__Impl : ( ';' ) ;
    public final void rule__MethodBody__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1872:1: ( ( ';' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1873:1: ( ';' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1873:1: ( ';' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1874:1: ';'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getSemicolonKeyword_2()); 
            }
            match(input,20,FOLLOW_20_in_rule__MethodBody__Group__2__Impl3816); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getSemicolonKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__Group__2__Impl"


    // $ANTLR start "rule__Expression__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1893:1: rule__Expression__Group__0 : rule__Expression__Group__0__Impl rule__Expression__Group__1 ;
    public final void rule__Expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1897:1: ( rule__Expression__Group__0__Impl rule__Expression__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1898:2: rule__Expression__Group__0__Impl rule__Expression__Group__1
            {
            pushFollow(FOLLOW_rule__Expression__Group__0__Impl_in_rule__Expression__Group__03853);
            rule__Expression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Expression__Group__1_in_rule__Expression__Group__03856);
            rule__Expression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0"


    // $ANTLR start "rule__Expression__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1905:1: rule__Expression__Group__0__Impl : ( ruleTerminalExpression ) ;
    public final void rule__Expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1909:1: ( ( ruleTerminalExpression ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1910:1: ( ruleTerminalExpression )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1910:1: ( ruleTerminalExpression )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1911:1: ruleTerminalExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getTerminalExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_ruleTerminalExpression_in_rule__Expression__Group__0__Impl3883);
            ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getTerminalExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0__Impl"


    // $ANTLR start "rule__Expression__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1922:1: rule__Expression__Group__1 : rule__Expression__Group__1__Impl ;
    public final void rule__Expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1926:1: ( rule__Expression__Group__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1927:2: rule__Expression__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Expression__Group__1__Impl_in_rule__Expression__Group__13912);
            rule__Expression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1"


    // $ANTLR start "rule__Expression__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1933:1: rule__Expression__Group__1__Impl : ( ( rule__Expression__Group_1__0 )* ) ;
    public final void rule__Expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1937:1: ( ( ( rule__Expression__Group_1__0 )* ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1938:1: ( ( rule__Expression__Group_1__0 )* )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1938:1: ( ( rule__Expression__Group_1__0 )* )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1939:1: ( rule__Expression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getGroup_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1940:1: ( rule__Expression__Group_1__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==25) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1940:2: rule__Expression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Expression__Group_1__0_in_rule__Expression__Group__1__Impl3939);
            	    rule__Expression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1__Impl"


    // $ANTLR start "rule__Expression__Group_1__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1954:1: rule__Expression__Group_1__0 : rule__Expression__Group_1__0__Impl rule__Expression__Group_1__1 ;
    public final void rule__Expression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1958:1: ( rule__Expression__Group_1__0__Impl rule__Expression__Group_1__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1959:2: rule__Expression__Group_1__0__Impl rule__Expression__Group_1__1
            {
            pushFollow(FOLLOW_rule__Expression__Group_1__0__Impl_in_rule__Expression__Group_1__03974);
            rule__Expression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Expression__Group_1__1_in_rule__Expression__Group_1__03977);
            rule__Expression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__0"


    // $ANTLR start "rule__Expression__Group_1__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1966:1: rule__Expression__Group_1__0__Impl : ( () ) ;
    public final void rule__Expression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1970:1: ( ( () ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1971:1: ( () )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1971:1: ( () )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1972:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getSelectionReceiverAction_1_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1973:1: ()
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1975:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getSelectionReceiverAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__0__Impl"


    // $ANTLR start "rule__Expression__Group_1__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1985:1: rule__Expression__Group_1__1 : rule__Expression__Group_1__1__Impl rule__Expression__Group_1__2 ;
    public final void rule__Expression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1989:1: ( rule__Expression__Group_1__1__Impl rule__Expression__Group_1__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1990:2: rule__Expression__Group_1__1__Impl rule__Expression__Group_1__2
            {
            pushFollow(FOLLOW_rule__Expression__Group_1__1__Impl_in_rule__Expression__Group_1__14035);
            rule__Expression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Expression__Group_1__2_in_rule__Expression__Group_1__14038);
            rule__Expression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__1"


    // $ANTLR start "rule__Expression__Group_1__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:1997:1: rule__Expression__Group_1__1__Impl : ( '.' ) ;
    public final void rule__Expression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2001:1: ( ( '.' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2002:1: ( '.' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2002:1: ( '.' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2003:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getFullStopKeyword_1_1()); 
            }
            match(input,25,FOLLOW_25_in_rule__Expression__Group_1__1__Impl4066); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getFullStopKeyword_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__1__Impl"


    // $ANTLR start "rule__Expression__Group_1__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2016:1: rule__Expression__Group_1__2 : rule__Expression__Group_1__2__Impl ;
    public final void rule__Expression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2020:1: ( rule__Expression__Group_1__2__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2021:2: rule__Expression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_rule__Expression__Group_1__2__Impl_in_rule__Expression__Group_1__24097);
            rule__Expression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__2"


    // $ANTLR start "rule__Expression__Group_1__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2027:1: rule__Expression__Group_1__2__Impl : ( ( rule__Expression__MessageAssignment_1_2 ) ) ;
    public final void rule__Expression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2031:1: ( ( ( rule__Expression__MessageAssignment_1_2 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2032:1: ( ( rule__Expression__MessageAssignment_1_2 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2032:1: ( ( rule__Expression__MessageAssignment_1_2 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2033:1: ( rule__Expression__MessageAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getMessageAssignment_1_2()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2034:1: ( rule__Expression__MessageAssignment_1_2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2034:2: rule__Expression__MessageAssignment_1_2
            {
            pushFollow(FOLLOW_rule__Expression__MessageAssignment_1_2_in_rule__Expression__Group_1__2__Impl4124);
            rule__Expression__MessageAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getMessageAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group_1__2__Impl"


    // $ANTLR start "rule__MethodCall__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2050:1: rule__MethodCall__Group__0 : rule__MethodCall__Group__0__Impl rule__MethodCall__Group__1 ;
    public final void rule__MethodCall__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2054:1: ( rule__MethodCall__Group__0__Impl rule__MethodCall__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2055:2: rule__MethodCall__Group__0__Impl rule__MethodCall__Group__1
            {
            pushFollow(FOLLOW_rule__MethodCall__Group__0__Impl_in_rule__MethodCall__Group__04160);
            rule__MethodCall__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__MethodCall__Group__1_in_rule__MethodCall__Group__04163);
            rule__MethodCall__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__0"


    // $ANTLR start "rule__MethodCall__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2062:1: rule__MethodCall__Group__0__Impl : ( ( rule__MethodCall__NameAssignment_0 ) ) ;
    public final void rule__MethodCall__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2066:1: ( ( ( rule__MethodCall__NameAssignment_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2067:1: ( ( rule__MethodCall__NameAssignment_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2067:1: ( ( rule__MethodCall__NameAssignment_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2068:1: ( rule__MethodCall__NameAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getNameAssignment_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2069:1: ( rule__MethodCall__NameAssignment_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2069:2: rule__MethodCall__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__MethodCall__NameAssignment_0_in_rule__MethodCall__Group__0__Impl4190);
            rule__MethodCall__NameAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getNameAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__0__Impl"


    // $ANTLR start "rule__MethodCall__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2079:1: rule__MethodCall__Group__1 : rule__MethodCall__Group__1__Impl rule__MethodCall__Group__2 ;
    public final void rule__MethodCall__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2083:1: ( rule__MethodCall__Group__1__Impl rule__MethodCall__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2084:2: rule__MethodCall__Group__1__Impl rule__MethodCall__Group__2
            {
            pushFollow(FOLLOW_rule__MethodCall__Group__1__Impl_in_rule__MethodCall__Group__14220);
            rule__MethodCall__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__MethodCall__Group__2_in_rule__MethodCall__Group__14223);
            rule__MethodCall__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__1"


    // $ANTLR start "rule__MethodCall__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2091:1: rule__MethodCall__Group__1__Impl : ( '(' ) ;
    public final void rule__MethodCall__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2095:1: ( ( '(' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2096:1: ( '(' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2096:1: ( '(' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2097:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,21,FOLLOW_21_in_rule__MethodCall__Group__1__Impl4251); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__1__Impl"


    // $ANTLR start "rule__MethodCall__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2110:1: rule__MethodCall__Group__2 : rule__MethodCall__Group__2__Impl rule__MethodCall__Group__3 ;
    public final void rule__MethodCall__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2114:1: ( rule__MethodCall__Group__2__Impl rule__MethodCall__Group__3 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2115:2: rule__MethodCall__Group__2__Impl rule__MethodCall__Group__3
            {
            pushFollow(FOLLOW_rule__MethodCall__Group__2__Impl_in_rule__MethodCall__Group__24282);
            rule__MethodCall__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__MethodCall__Group__3_in_rule__MethodCall__Group__24285);
            rule__MethodCall__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__2"


    // $ANTLR start "rule__MethodCall__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2122:1: rule__MethodCall__Group__2__Impl : ( ( rule__MethodCall__Group_2__0 )? ) ;
    public final void rule__MethodCall__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2126:1: ( ( ( rule__MethodCall__Group_2__0 )? ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2127:1: ( ( rule__MethodCall__Group_2__0 )? )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2127:1: ( ( rule__MethodCall__Group_2__0 )? )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2128:1: ( rule__MethodCall__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getGroup_2()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2129:1: ( rule__MethodCall__Group_2__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=RULE_ID && LA15_0<=RULE_INT)||(LA15_0>=14 && LA15_0<=15)||LA15_0==21||(LA15_0>=26 && LA15_0<=27)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2129:2: rule__MethodCall__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__MethodCall__Group_2__0_in_rule__MethodCall__Group__2__Impl4312);
                    rule__MethodCall__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__2__Impl"


    // $ANTLR start "rule__MethodCall__Group__3"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2139:1: rule__MethodCall__Group__3 : rule__MethodCall__Group__3__Impl ;
    public final void rule__MethodCall__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2143:1: ( rule__MethodCall__Group__3__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2144:2: rule__MethodCall__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__MethodCall__Group__3__Impl_in_rule__MethodCall__Group__34343);
            rule__MethodCall__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__3"


    // $ANTLR start "rule__MethodCall__Group__3__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2150:1: rule__MethodCall__Group__3__Impl : ( ')' ) ;
    public final void rule__MethodCall__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2154:1: ( ( ')' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2155:1: ( ')' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2155:1: ( ')' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2156:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,22,FOLLOW_22_in_rule__MethodCall__Group__3__Impl4371); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group__3__Impl"


    // $ANTLR start "rule__MethodCall__Group_2__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2177:1: rule__MethodCall__Group_2__0 : rule__MethodCall__Group_2__0__Impl rule__MethodCall__Group_2__1 ;
    public final void rule__MethodCall__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2181:1: ( rule__MethodCall__Group_2__0__Impl rule__MethodCall__Group_2__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2182:2: rule__MethodCall__Group_2__0__Impl rule__MethodCall__Group_2__1
            {
            pushFollow(FOLLOW_rule__MethodCall__Group_2__0__Impl_in_rule__MethodCall__Group_2__04410);
            rule__MethodCall__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__MethodCall__Group_2__1_in_rule__MethodCall__Group_2__04413);
            rule__MethodCall__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__0"


    // $ANTLR start "rule__MethodCall__Group_2__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2189:1: rule__MethodCall__Group_2__0__Impl : ( ( rule__MethodCall__ArgsAssignment_2_0 ) ) ;
    public final void rule__MethodCall__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2193:1: ( ( ( rule__MethodCall__ArgsAssignment_2_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2194:1: ( ( rule__MethodCall__ArgsAssignment_2_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2194:1: ( ( rule__MethodCall__ArgsAssignment_2_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2195:1: ( rule__MethodCall__ArgsAssignment_2_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsAssignment_2_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2196:1: ( rule__MethodCall__ArgsAssignment_2_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2196:2: rule__MethodCall__ArgsAssignment_2_0
            {
            pushFollow(FOLLOW_rule__MethodCall__ArgsAssignment_2_0_in_rule__MethodCall__Group_2__0__Impl4440);
            rule__MethodCall__ArgsAssignment_2_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsAssignment_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__0__Impl"


    // $ANTLR start "rule__MethodCall__Group_2__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2206:1: rule__MethodCall__Group_2__1 : rule__MethodCall__Group_2__1__Impl ;
    public final void rule__MethodCall__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2210:1: ( rule__MethodCall__Group_2__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2211:2: rule__MethodCall__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__MethodCall__Group_2__1__Impl_in_rule__MethodCall__Group_2__14470);
            rule__MethodCall__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__1"


    // $ANTLR start "rule__MethodCall__Group_2__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2217:1: rule__MethodCall__Group_2__1__Impl : ( ( rule__MethodCall__Group_2_1__0 )* ) ;
    public final void rule__MethodCall__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2221:1: ( ( ( rule__MethodCall__Group_2_1__0 )* ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2222:1: ( ( rule__MethodCall__Group_2_1__0 )* )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2222:1: ( ( rule__MethodCall__Group_2_1__0 )* )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2223:1: ( rule__MethodCall__Group_2_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getGroup_2_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2224:1: ( rule__MethodCall__Group_2_1__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==23) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2224:2: rule__MethodCall__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_rule__MethodCall__Group_2_1__0_in_rule__MethodCall__Group_2__1__Impl4497);
            	    rule__MethodCall__Group_2_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getGroup_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2__1__Impl"


    // $ANTLR start "rule__MethodCall__Group_2_1__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2238:1: rule__MethodCall__Group_2_1__0 : rule__MethodCall__Group_2_1__0__Impl rule__MethodCall__Group_2_1__1 ;
    public final void rule__MethodCall__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2242:1: ( rule__MethodCall__Group_2_1__0__Impl rule__MethodCall__Group_2_1__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2243:2: rule__MethodCall__Group_2_1__0__Impl rule__MethodCall__Group_2_1__1
            {
            pushFollow(FOLLOW_rule__MethodCall__Group_2_1__0__Impl_in_rule__MethodCall__Group_2_1__04532);
            rule__MethodCall__Group_2_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__MethodCall__Group_2_1__1_in_rule__MethodCall__Group_2_1__04535);
            rule__MethodCall__Group_2_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__0"


    // $ANTLR start "rule__MethodCall__Group_2_1__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2250:1: rule__MethodCall__Group_2_1__0__Impl : ( ',' ) ;
    public final void rule__MethodCall__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2254:1: ( ( ',' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2255:1: ( ',' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2255:1: ( ',' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2256:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getCommaKeyword_2_1_0()); 
            }
            match(input,23,FOLLOW_23_in_rule__MethodCall__Group_2_1__0__Impl4563); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getCommaKeyword_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__0__Impl"


    // $ANTLR start "rule__MethodCall__Group_2_1__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2269:1: rule__MethodCall__Group_2_1__1 : rule__MethodCall__Group_2_1__1__Impl ;
    public final void rule__MethodCall__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2273:1: ( rule__MethodCall__Group_2_1__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2274:2: rule__MethodCall__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_rule__MethodCall__Group_2_1__1__Impl_in_rule__MethodCall__Group_2_1__14594);
            rule__MethodCall__Group_2_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__1"


    // $ANTLR start "rule__MethodCall__Group_2_1__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2280:1: rule__MethodCall__Group_2_1__1__Impl : ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) ) ;
    public final void rule__MethodCall__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2284:1: ( ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2285:1: ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2285:1: ( ( rule__MethodCall__ArgsAssignment_2_1_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2286:1: ( rule__MethodCall__ArgsAssignment_2_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsAssignment_2_1_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2287:1: ( rule__MethodCall__ArgsAssignment_2_1_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2287:2: rule__MethodCall__ArgsAssignment_2_1_1
            {
            pushFollow(FOLLOW_rule__MethodCall__ArgsAssignment_2_1_1_in_rule__MethodCall__Group_2_1__1__Impl4621);
            rule__MethodCall__ArgsAssignment_2_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsAssignment_2_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__Group_2_1__1__Impl"


    // $ANTLR start "rule__New__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2301:1: rule__New__Group__0 : rule__New__Group__0__Impl rule__New__Group__1 ;
    public final void rule__New__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2305:1: ( rule__New__Group__0__Impl rule__New__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2306:2: rule__New__Group__0__Impl rule__New__Group__1
            {
            pushFollow(FOLLOW_rule__New__Group__0__Impl_in_rule__New__Group__04655);
            rule__New__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__New__Group__1_in_rule__New__Group__04658);
            rule__New__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__0"


    // $ANTLR start "rule__New__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2313:1: rule__New__Group__0__Impl : ( 'new' ) ;
    public final void rule__New__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2317:1: ( ( 'new' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2318:1: ( 'new' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2318:1: ( 'new' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2319:1: 'new'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getNewKeyword_0()); 
            }
            match(input,26,FOLLOW_26_in_rule__New__Group__0__Impl4686); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getNewKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__0__Impl"


    // $ANTLR start "rule__New__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2332:1: rule__New__Group__1 : rule__New__Group__1__Impl rule__New__Group__2 ;
    public final void rule__New__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2336:1: ( rule__New__Group__1__Impl rule__New__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2337:2: rule__New__Group__1__Impl rule__New__Group__2
            {
            pushFollow(FOLLOW_rule__New__Group__1__Impl_in_rule__New__Group__14717);
            rule__New__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__New__Group__2_in_rule__New__Group__14720);
            rule__New__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__1"


    // $ANTLR start "rule__New__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2344:1: rule__New__Group__1__Impl : ( ( rule__New__TypeAssignment_1 ) ) ;
    public final void rule__New__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2348:1: ( ( ( rule__New__TypeAssignment_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2349:1: ( ( rule__New__TypeAssignment_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2349:1: ( ( rule__New__TypeAssignment_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2350:1: ( rule__New__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getTypeAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2351:1: ( rule__New__TypeAssignment_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2351:2: rule__New__TypeAssignment_1
            {
            pushFollow(FOLLOW_rule__New__TypeAssignment_1_in_rule__New__Group__1__Impl4747);
            rule__New__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__1__Impl"


    // $ANTLR start "rule__New__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2361:1: rule__New__Group__2 : rule__New__Group__2__Impl rule__New__Group__3 ;
    public final void rule__New__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2365:1: ( rule__New__Group__2__Impl rule__New__Group__3 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2366:2: rule__New__Group__2__Impl rule__New__Group__3
            {
            pushFollow(FOLLOW_rule__New__Group__2__Impl_in_rule__New__Group__24777);
            rule__New__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__New__Group__3_in_rule__New__Group__24780);
            rule__New__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__2"


    // $ANTLR start "rule__New__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2373:1: rule__New__Group__2__Impl : ( '(' ) ;
    public final void rule__New__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2377:1: ( ( '(' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2378:1: ( '(' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2378:1: ( '(' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2379:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getLeftParenthesisKeyword_2()); 
            }
            match(input,21,FOLLOW_21_in_rule__New__Group__2__Impl4808); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getLeftParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__2__Impl"


    // $ANTLR start "rule__New__Group__3"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2392:1: rule__New__Group__3 : rule__New__Group__3__Impl rule__New__Group__4 ;
    public final void rule__New__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2396:1: ( rule__New__Group__3__Impl rule__New__Group__4 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2397:2: rule__New__Group__3__Impl rule__New__Group__4
            {
            pushFollow(FOLLOW_rule__New__Group__3__Impl_in_rule__New__Group__34839);
            rule__New__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__New__Group__4_in_rule__New__Group__34842);
            rule__New__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__3"


    // $ANTLR start "rule__New__Group__3__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2404:1: rule__New__Group__3__Impl : ( ( rule__New__Group_3__0 )? ) ;
    public final void rule__New__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2408:1: ( ( ( rule__New__Group_3__0 )? ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2409:1: ( ( rule__New__Group_3__0 )? )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2409:1: ( ( rule__New__Group_3__0 )? )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2410:1: ( rule__New__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getGroup_3()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2411:1: ( rule__New__Group_3__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=RULE_ID && LA17_0<=RULE_INT)||(LA17_0>=14 && LA17_0<=15)||LA17_0==21||(LA17_0>=26 && LA17_0<=27)) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2411:2: rule__New__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__New__Group_3__0_in_rule__New__Group__3__Impl4869);
                    rule__New__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__3__Impl"


    // $ANTLR start "rule__New__Group__4"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2421:1: rule__New__Group__4 : rule__New__Group__4__Impl ;
    public final void rule__New__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2425:1: ( rule__New__Group__4__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2426:2: rule__New__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__New__Group__4__Impl_in_rule__New__Group__44900);
            rule__New__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__4"


    // $ANTLR start "rule__New__Group__4__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2432:1: rule__New__Group__4__Impl : ( ')' ) ;
    public final void rule__New__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2436:1: ( ( ')' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2437:1: ( ')' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2437:1: ( ')' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2438:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,22,FOLLOW_22_in_rule__New__Group__4__Impl4928); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group__4__Impl"


    // $ANTLR start "rule__New__Group_3__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2461:1: rule__New__Group_3__0 : rule__New__Group_3__0__Impl rule__New__Group_3__1 ;
    public final void rule__New__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2465:1: ( rule__New__Group_3__0__Impl rule__New__Group_3__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2466:2: rule__New__Group_3__0__Impl rule__New__Group_3__1
            {
            pushFollow(FOLLOW_rule__New__Group_3__0__Impl_in_rule__New__Group_3__04969);
            rule__New__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__New__Group_3__1_in_rule__New__Group_3__04972);
            rule__New__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__0"


    // $ANTLR start "rule__New__Group_3__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2473:1: rule__New__Group_3__0__Impl : ( ( rule__New__ArgsAssignment_3_0 ) ) ;
    public final void rule__New__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2477:1: ( ( ( rule__New__ArgsAssignment_3_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2478:1: ( ( rule__New__ArgsAssignment_3_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2478:1: ( ( rule__New__ArgsAssignment_3_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2479:1: ( rule__New__ArgsAssignment_3_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsAssignment_3_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2480:1: ( rule__New__ArgsAssignment_3_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2480:2: rule__New__ArgsAssignment_3_0
            {
            pushFollow(FOLLOW_rule__New__ArgsAssignment_3_0_in_rule__New__Group_3__0__Impl4999);
            rule__New__ArgsAssignment_3_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsAssignment_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__0__Impl"


    // $ANTLR start "rule__New__Group_3__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2490:1: rule__New__Group_3__1 : rule__New__Group_3__1__Impl ;
    public final void rule__New__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2494:1: ( rule__New__Group_3__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2495:2: rule__New__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__New__Group_3__1__Impl_in_rule__New__Group_3__15029);
            rule__New__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__1"


    // $ANTLR start "rule__New__Group_3__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2501:1: rule__New__Group_3__1__Impl : ( ( rule__New__Group_3_1__0 )* ) ;
    public final void rule__New__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2505:1: ( ( ( rule__New__Group_3_1__0 )* ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2506:1: ( ( rule__New__Group_3_1__0 )* )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2506:1: ( ( rule__New__Group_3_1__0 )* )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2507:1: ( rule__New__Group_3_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getGroup_3_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2508:1: ( rule__New__Group_3_1__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==23) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2508:2: rule__New__Group_3_1__0
            	    {
            	    pushFollow(FOLLOW_rule__New__Group_3_1__0_in_rule__New__Group_3__1__Impl5056);
            	    rule__New__Group_3_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getGroup_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3__1__Impl"


    // $ANTLR start "rule__New__Group_3_1__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2522:1: rule__New__Group_3_1__0 : rule__New__Group_3_1__0__Impl rule__New__Group_3_1__1 ;
    public final void rule__New__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2526:1: ( rule__New__Group_3_1__0__Impl rule__New__Group_3_1__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2527:2: rule__New__Group_3_1__0__Impl rule__New__Group_3_1__1
            {
            pushFollow(FOLLOW_rule__New__Group_3_1__0__Impl_in_rule__New__Group_3_1__05091);
            rule__New__Group_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__New__Group_3_1__1_in_rule__New__Group_3_1__05094);
            rule__New__Group_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__0"


    // $ANTLR start "rule__New__Group_3_1__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2534:1: rule__New__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__New__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2538:1: ( ( ',' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2539:1: ( ',' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2539:1: ( ',' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2540:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getCommaKeyword_3_1_0()); 
            }
            match(input,23,FOLLOW_23_in_rule__New__Group_3_1__0__Impl5122); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getCommaKeyword_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__0__Impl"


    // $ANTLR start "rule__New__Group_3_1__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2553:1: rule__New__Group_3_1__1 : rule__New__Group_3_1__1__Impl ;
    public final void rule__New__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2557:1: ( rule__New__Group_3_1__1__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2558:2: rule__New__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_rule__New__Group_3_1__1__Impl_in_rule__New__Group_3_1__15153);
            rule__New__Group_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__1"


    // $ANTLR start "rule__New__Group_3_1__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2564:1: rule__New__Group_3_1__1__Impl : ( ( rule__New__ArgsAssignment_3_1_1 ) ) ;
    public final void rule__New__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2568:1: ( ( ( rule__New__ArgsAssignment_3_1_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2569:1: ( ( rule__New__ArgsAssignment_3_1_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2569:1: ( ( rule__New__ArgsAssignment_3_1_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2570:1: ( rule__New__ArgsAssignment_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsAssignment_3_1_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2571:1: ( rule__New__ArgsAssignment_3_1_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2571:2: rule__New__ArgsAssignment_3_1_1
            {
            pushFollow(FOLLOW_rule__New__ArgsAssignment_3_1_1_in_rule__New__Group_3_1__1__Impl5180);
            rule__New__ArgsAssignment_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsAssignment_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__Group_3_1__1__Impl"


    // $ANTLR start "rule__Cast__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2585:1: rule__Cast__Group__0 : rule__Cast__Group__0__Impl rule__Cast__Group__1 ;
    public final void rule__Cast__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2589:1: ( rule__Cast__Group__0__Impl rule__Cast__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2590:2: rule__Cast__Group__0__Impl rule__Cast__Group__1
            {
            pushFollow(FOLLOW_rule__Cast__Group__0__Impl_in_rule__Cast__Group__05214);
            rule__Cast__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Cast__Group__1_in_rule__Cast__Group__05217);
            rule__Cast__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__0"


    // $ANTLR start "rule__Cast__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2597:1: rule__Cast__Group__0__Impl : ( '(' ) ;
    public final void rule__Cast__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2601:1: ( ( '(' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2602:1: ( '(' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2602:1: ( '(' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2603:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,21,FOLLOW_21_in_rule__Cast__Group__0__Impl5245); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__0__Impl"


    // $ANTLR start "rule__Cast__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2616:1: rule__Cast__Group__1 : rule__Cast__Group__1__Impl rule__Cast__Group__2 ;
    public final void rule__Cast__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2620:1: ( rule__Cast__Group__1__Impl rule__Cast__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2621:2: rule__Cast__Group__1__Impl rule__Cast__Group__2
            {
            pushFollow(FOLLOW_rule__Cast__Group__1__Impl_in_rule__Cast__Group__15276);
            rule__Cast__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Cast__Group__2_in_rule__Cast__Group__15279);
            rule__Cast__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__1"


    // $ANTLR start "rule__Cast__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2628:1: rule__Cast__Group__1__Impl : ( ( rule__Cast__TypeAssignment_1 ) ) ;
    public final void rule__Cast__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2632:1: ( ( ( rule__Cast__TypeAssignment_1 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2633:1: ( ( rule__Cast__TypeAssignment_1 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2633:1: ( ( rule__Cast__TypeAssignment_1 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2634:1: ( rule__Cast__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getTypeAssignment_1()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2635:1: ( rule__Cast__TypeAssignment_1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2635:2: rule__Cast__TypeAssignment_1
            {
            pushFollow(FOLLOW_rule__Cast__TypeAssignment_1_in_rule__Cast__Group__1__Impl5306);
            rule__Cast__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__1__Impl"


    // $ANTLR start "rule__Cast__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2645:1: rule__Cast__Group__2 : rule__Cast__Group__2__Impl rule__Cast__Group__3 ;
    public final void rule__Cast__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2649:1: ( rule__Cast__Group__2__Impl rule__Cast__Group__3 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2650:2: rule__Cast__Group__2__Impl rule__Cast__Group__3
            {
            pushFollow(FOLLOW_rule__Cast__Group__2__Impl_in_rule__Cast__Group__25336);
            rule__Cast__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Cast__Group__3_in_rule__Cast__Group__25339);
            rule__Cast__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__2"


    // $ANTLR start "rule__Cast__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2657:1: rule__Cast__Group__2__Impl : ( ')' ) ;
    public final void rule__Cast__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2661:1: ( ( ')' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2662:1: ( ')' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2662:1: ( ')' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2663:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getRightParenthesisKeyword_2()); 
            }
            match(input,22,FOLLOW_22_in_rule__Cast__Group__2__Impl5367); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getRightParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__2__Impl"


    // $ANTLR start "rule__Cast__Group__3"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2676:1: rule__Cast__Group__3 : rule__Cast__Group__3__Impl ;
    public final void rule__Cast__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2680:1: ( rule__Cast__Group__3__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2681:2: rule__Cast__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Cast__Group__3__Impl_in_rule__Cast__Group__35398);
            rule__Cast__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__3"


    // $ANTLR start "rule__Cast__Group__3__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2687:1: rule__Cast__Group__3__Impl : ( ( rule__Cast__ObjectAssignment_3 ) ) ;
    public final void rule__Cast__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2691:1: ( ( ( rule__Cast__ObjectAssignment_3 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2692:1: ( ( rule__Cast__ObjectAssignment_3 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2692:1: ( ( rule__Cast__ObjectAssignment_3 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2693:1: ( rule__Cast__ObjectAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getObjectAssignment_3()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2694:1: ( rule__Cast__ObjectAssignment_3 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2694:2: rule__Cast__ObjectAssignment_3
            {
            pushFollow(FOLLOW_rule__Cast__ObjectAssignment_3_in_rule__Cast__Group__3__Impl5425);
            rule__Cast__ObjectAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getObjectAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__Group__3__Impl"


    // $ANTLR start "rule__Paren__Group__0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2712:1: rule__Paren__Group__0 : rule__Paren__Group__0__Impl rule__Paren__Group__1 ;
    public final void rule__Paren__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2716:1: ( rule__Paren__Group__0__Impl rule__Paren__Group__1 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2717:2: rule__Paren__Group__0__Impl rule__Paren__Group__1
            {
            pushFollow(FOLLOW_rule__Paren__Group__0__Impl_in_rule__Paren__Group__05463);
            rule__Paren__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Paren__Group__1_in_rule__Paren__Group__05466);
            rule__Paren__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__0"


    // $ANTLR start "rule__Paren__Group__0__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2724:1: rule__Paren__Group__0__Impl : ( '(' ) ;
    public final void rule__Paren__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2728:1: ( ( '(' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2729:1: ( '(' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2729:1: ( '(' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2730:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,21,FOLLOW_21_in_rule__Paren__Group__0__Impl5494); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__0__Impl"


    // $ANTLR start "rule__Paren__Group__1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2743:1: rule__Paren__Group__1 : rule__Paren__Group__1__Impl rule__Paren__Group__2 ;
    public final void rule__Paren__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2747:1: ( rule__Paren__Group__1__Impl rule__Paren__Group__2 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2748:2: rule__Paren__Group__1__Impl rule__Paren__Group__2
            {
            pushFollow(FOLLOW_rule__Paren__Group__1__Impl_in_rule__Paren__Group__15525);
            rule__Paren__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Paren__Group__2_in_rule__Paren__Group__15528);
            rule__Paren__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__1"


    // $ANTLR start "rule__Paren__Group__1__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2755:1: rule__Paren__Group__1__Impl : ( ruleExpression ) ;
    public final void rule__Paren__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2759:1: ( ( ruleExpression ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2760:1: ( ruleExpression )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2760:1: ( ruleExpression )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2761:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getExpressionParserRuleCall_1()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_rule__Paren__Group__1__Impl5555);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getExpressionParserRuleCall_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__1__Impl"


    // $ANTLR start "rule__Paren__Group__2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2772:1: rule__Paren__Group__2 : rule__Paren__Group__2__Impl ;
    public final void rule__Paren__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2776:1: ( rule__Paren__Group__2__Impl )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2777:2: rule__Paren__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Paren__Group__2__Impl_in_rule__Paren__Group__25584);
            rule__Paren__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__2"


    // $ANTLR start "rule__Paren__Group__2__Impl"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2783:1: rule__Paren__Group__2__Impl : ( ')' ) ;
    public final void rule__Paren__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2787:1: ( ( ')' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2788:1: ( ')' )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2788:1: ( ')' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2789:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParenAccess().getRightParenthesisKeyword_2()); 
            }
            match(input,22,FOLLOW_22_in_rule__Paren__Group__2__Impl5612); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParenAccess().getRightParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Paren__Group__2__Impl"


    // $ANTLR start "rule__Program__ClassesAssignment_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2809:1: rule__Program__ClassesAssignment_0 : ( ruleClass ) ;
    public final void rule__Program__ClassesAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2813:1: ( ( ruleClass ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2814:1: ( ruleClass )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2814:1: ( ruleClass )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2815:1: ruleClass
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getClassesClassParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleClass_in_rule__Program__ClassesAssignment_05654);
            ruleClass();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getClassesClassParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__ClassesAssignment_0"


    // $ANTLR start "rule__Program__MainAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2824:1: rule__Program__MainAssignment_1 : ( ruleExpression ) ;
    public final void rule__Program__MainAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2828:1: ( ( ruleExpression ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2829:1: ( ruleExpression )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2829:1: ( ruleExpression )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2830:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProgramAccess().getMainExpressionParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_rule__Program__MainAssignment_15685);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProgramAccess().getMainExpressionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Program__MainAssignment_1"


    // $ANTLR start "rule__BasicType__BasicAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2839:1: rule__BasicType__BasicAssignment : ( ( rule__BasicType__BasicAlternatives_0 ) ) ;
    public final void rule__BasicType__BasicAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2843:1: ( ( ( rule__BasicType__BasicAlternatives_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2844:1: ( ( rule__BasicType__BasicAlternatives_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2844:1: ( ( rule__BasicType__BasicAlternatives_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2845:1: ( rule__BasicType__BasicAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicTypeAccess().getBasicAlternatives_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2846:1: ( rule__BasicType__BasicAlternatives_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2846:2: rule__BasicType__BasicAlternatives_0
            {
            pushFollow(FOLLOW_rule__BasicType__BasicAlternatives_0_in_rule__BasicType__BasicAssignment5716);
            rule__BasicType__BasicAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicTypeAccess().getBasicAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicType__BasicAssignment"


    // $ANTLR start "rule__ClassType__ClassrefAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2855:1: rule__ClassType__ClassrefAssignment : ( ( RULE_ID ) ) ;
    public final void rule__ClassType__ClassrefAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2859:1: ( ( ( RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2860:1: ( ( RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2860:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2861:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeAccess().getClassrefClassCrossReference_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2862:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2863:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassTypeAccess().getClassrefClassIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ClassType__ClassrefAssignment5753); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeAccess().getClassrefClassIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassTypeAccess().getClassrefClassCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassType__ClassrefAssignment"


    // $ANTLR start "rule__Class__NameAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2874:1: rule__Class__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Class__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2878:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2879:1: ( RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2879:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2880:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Class__NameAssignment_15788); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__NameAssignment_1"


    // $ANTLR start "rule__Class__ExtendsAssignment_2_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2889:1: rule__Class__ExtendsAssignment_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__Class__ExtendsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2893:1: ( ( ( RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2894:1: ( ( RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2894:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2895:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsClassCrossReference_2_1_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2896:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2897:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getExtendsClassIDTerminalRuleCall_2_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Class__ExtendsAssignment_2_15823); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsClassIDTerminalRuleCall_2_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getExtendsClassCrossReference_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__ExtendsAssignment_2_1"


    // $ANTLR start "rule__Class__FieldsAssignment_4"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2908:1: rule__Class__FieldsAssignment_4 : ( ruleField ) ;
    public final void rule__Class__FieldsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2912:1: ( ( ruleField ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2913:1: ( ruleField )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2913:1: ( ruleField )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2914:1: ruleField
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getFieldsFieldParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleField_in_rule__Class__FieldsAssignment_45858);
            ruleField();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getFieldsFieldParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__FieldsAssignment_4"


    // $ANTLR start "rule__Class__MethodsAssignment_5"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2923:1: rule__Class__MethodsAssignment_5 : ( ruleMethod ) ;
    public final void rule__Class__MethodsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2927:1: ( ( ruleMethod ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2928:1: ( ruleMethod )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2928:1: ( ruleMethod )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2929:1: ruleMethod
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getClassAccess().getMethodsMethodParserRuleCall_5_0()); 
            }
            pushFollow(FOLLOW_ruleMethod_in_rule__Class__MethodsAssignment_55889);
            ruleMethod();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getClassAccess().getMethodsMethodParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Class__MethodsAssignment_5"


    // $ANTLR start "rule__Field__TypeAssignment_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2938:1: rule__Field__TypeAssignment_0 : ( ruleType ) ;
    public final void rule__Field__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2942:1: ( ( ruleType ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2943:1: ( ruleType )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2943:1: ( ruleType )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2944:1: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getTypeTypeParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleType_in_rule__Field__TypeAssignment_05920);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getTypeTypeParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__TypeAssignment_0"


    // $ANTLR start "rule__Field__NameAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2953:1: rule__Field__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Field__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2957:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2958:1: ( RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2958:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2959:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Field__NameAssignment_15951); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Field__NameAssignment_1"


    // $ANTLR start "rule__Parameter__TypeAssignment_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2968:1: rule__Parameter__TypeAssignment_0 : ( ruleType ) ;
    public final void rule__Parameter__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2972:1: ( ( ruleType ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2973:1: ( ruleType )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2973:1: ( ruleType )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2974:1: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getTypeTypeParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleType_in_rule__Parameter__TypeAssignment_05982);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getTypeTypeParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__TypeAssignment_0"


    // $ANTLR start "rule__Parameter__NameAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2983:1: rule__Parameter__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Parameter__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2987:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2988:1: ( RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2988:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2989:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_16013); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__NameAssignment_1"


    // $ANTLR start "rule__Method__ReturntypeAssignment_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:2998:1: rule__Method__ReturntypeAssignment_0 : ( ruleType ) ;
    public final void rule__Method__ReturntypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3002:1: ( ( ruleType ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3003:1: ( ruleType )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3003:1: ( ruleType )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3004:1: ruleType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getReturntypeTypeParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleType_in_rule__Method__ReturntypeAssignment_06044);
            ruleType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getReturntypeTypeParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ReturntypeAssignment_0"


    // $ANTLR start "rule__Method__NameAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3013:1: rule__Method__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Method__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3017:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3018:1: ( RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3018:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3019:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Method__NameAssignment_16075); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__NameAssignment_1"


    // $ANTLR start "rule__Method__ParamsAssignment_3_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3028:1: rule__Method__ParamsAssignment_3_0 : ( ruleParameter ) ;
    public final void rule__Method__ParamsAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3032:1: ( ( ruleParameter ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3033:1: ( ruleParameter )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3033:1: ( ruleParameter )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3034:1: ruleParameter
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_0_0()); 
            }
            pushFollow(FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_06106);
            ruleParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ParamsAssignment_3_0"


    // $ANTLR start "rule__Method__ParamsAssignment_3_1_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3043:1: rule__Method__ParamsAssignment_3_1_1 : ( ruleParameter ) ;
    public final void rule__Method__ParamsAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3047:1: ( ( ruleParameter ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3048:1: ( ruleParameter )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3048:1: ( ruleParameter )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3049:1: ruleParameter
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_1_1_0()); 
            }
            pushFollow(FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_1_16137);
            ruleParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__ParamsAssignment_3_1_1"


    // $ANTLR start "rule__Method__BodyAssignment_6"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3058:1: rule__Method__BodyAssignment_6 : ( ruleMethodBody ) ;
    public final void rule__Method__BodyAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3062:1: ( ( ruleMethodBody ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3063:1: ( ruleMethodBody )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3063:1: ( ruleMethodBody )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3064:1: ruleMethodBody
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodAccess().getBodyMethodBodyParserRuleCall_6_0()); 
            }
            pushFollow(FOLLOW_ruleMethodBody_in_rule__Method__BodyAssignment_66168);
            ruleMethodBody();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodAccess().getBodyMethodBodyParserRuleCall_6_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Method__BodyAssignment_6"


    // $ANTLR start "rule__MethodBody__ExpressionAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3073:1: rule__MethodBody__ExpressionAssignment_1 : ( ruleExpression ) ;
    public final void rule__MethodBody__ExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3077:1: ( ( ruleExpression ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3078:1: ( ruleExpression )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3078:1: ( ruleExpression )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3079:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodBodyAccess().getExpressionExpressionParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_rule__MethodBody__ExpressionAssignment_16199);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodBodyAccess().getExpressionExpressionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodBody__ExpressionAssignment_1"


    // $ANTLR start "rule__Expression__MessageAssignment_1_2"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3088:1: rule__Expression__MessageAssignment_1_2 : ( ruleMessage ) ;
    public final void rule__Expression__MessageAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3092:1: ( ( ruleMessage ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3093:1: ( ruleMessage )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3093:1: ( ruleMessage )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3094:1: ruleMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getMessageMessageParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_ruleMessage_in_rule__Expression__MessageAssignment_1_26230);
            ruleMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getMessageMessageParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__MessageAssignment_1_2"


    // $ANTLR start "rule__MethodCall__NameAssignment_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3103:1: rule__MethodCall__NameAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__MethodCall__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3107:1: ( ( ( RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3108:1: ( ( RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3108:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3109:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getNameMethodCrossReference_0_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3110:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3111:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getNameMethodIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__MethodCall__NameAssignment_06265); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getNameMethodIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getNameMethodCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__NameAssignment_0"


    // $ANTLR start "rule__MethodCall__ArgsAssignment_2_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3122:1: rule__MethodCall__ArgsAssignment_2_0 : ( ruleArgument ) ;
    public final void rule__MethodCall__ArgsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3126:1: ( ( ruleArgument ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3127:1: ( ruleArgument )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3127:1: ( ruleArgument )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3128:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_0_0()); 
            }
            pushFollow(FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_06300);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__ArgsAssignment_2_0"


    // $ANTLR start "rule__MethodCall__ArgsAssignment_2_1_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3137:1: rule__MethodCall__ArgsAssignment_2_1_1 : ( ruleArgument ) ;
    public final void rule__MethodCall__ArgsAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3141:1: ( ( ruleArgument ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3142:1: ( ruleArgument )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3142:1: ( ruleArgument )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3143:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_1_1_0()); 
            }
            pushFollow(FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_1_16331);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MethodCall__ArgsAssignment_2_1_1"


    // $ANTLR start "rule__FieldSelection__NameAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3152:1: rule__FieldSelection__NameAssignment : ( ( RULE_ID ) ) ;
    public final void rule__FieldSelection__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3156:1: ( ( ( RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3157:1: ( ( RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3157:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3158:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionAccess().getNameFieldCrossReference_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3159:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3160:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFieldSelectionAccess().getNameFieldIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__FieldSelection__NameAssignment6366); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionAccess().getNameFieldIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFieldSelectionAccess().getNameFieldCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FieldSelection__NameAssignment"


    // $ANTLR start "rule__This__VariableAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3171:1: rule__This__VariableAssignment : ( ( 'this' ) ) ;
    public final void rule__This__VariableAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3175:1: ( ( ( 'this' ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3176:1: ( ( 'this' ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3176:1: ( ( 'this' ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3177:1: ( 'this' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3178:1: ( 'this' )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3179:1: 'this'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }
            match(input,27,FOLLOW_27_in_rule__This__VariableAssignment6406); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThisAccess().getVariableThisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__This__VariableAssignment"


    // $ANTLR start "rule__Variable__ParamrefAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3194:1: rule__Variable__ParamrefAssignment : ( ( RULE_ID ) ) ;
    public final void rule__Variable__ParamrefAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3198:1: ( ( ( RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3199:1: ( ( RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3199:1: ( ( RULE_ID ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3200:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getParamrefParameterCrossReference_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3201:1: ( RULE_ID )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3202:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getParamrefParameterIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Variable__ParamrefAssignment6449); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getParamrefParameterIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getParamrefParameterCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__ParamrefAssignment"


    // $ANTLR start "rule__New__TypeAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3213:1: rule__New__TypeAssignment_1 : ( ruleClassType ) ;
    public final void rule__New__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3217:1: ( ( ruleClassType ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3218:1: ( ruleClassType )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3218:1: ( ruleClassType )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3219:1: ruleClassType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleClassType_in_rule__New__TypeAssignment_16484);
            ruleClassType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__TypeAssignment_1"


    // $ANTLR start "rule__New__ArgsAssignment_3_0"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3228:1: rule__New__ArgsAssignment_3_0 : ( ruleArgument ) ;
    public final void rule__New__ArgsAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3232:1: ( ( ruleArgument ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3233:1: ( ruleArgument )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3233:1: ( ruleArgument )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3234:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_0_0()); 
            }
            pushFollow(FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_06515);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__ArgsAssignment_3_0"


    // $ANTLR start "rule__New__ArgsAssignment_3_1_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3243:1: rule__New__ArgsAssignment_3_1_1 : ( ruleArgument ) ;
    public final void rule__New__ArgsAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3247:1: ( ( ruleArgument ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3248:1: ( ruleArgument )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3248:1: ( ruleArgument )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3249:1: ruleArgument
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_1_1_0()); 
            }
            pushFollow(FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_1_16546);
            ruleArgument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__New__ArgsAssignment_3_1_1"


    // $ANTLR start "rule__Cast__TypeAssignment_1"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3258:1: rule__Cast__TypeAssignment_1 : ( ruleClassType ) ;
    public final void rule__Cast__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3262:1: ( ( ruleClassType ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3263:1: ( ruleClassType )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3263:1: ( ruleClassType )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3264:1: ruleClassType
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleClassType_in_rule__Cast__TypeAssignment_16577);
            ruleClassType();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getTypeClassTypeParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__TypeAssignment_1"


    // $ANTLR start "rule__Cast__ObjectAssignment_3"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3273:1: rule__Cast__ObjectAssignment_3 : ( ruleTerminalExpression ) ;
    public final void rule__Cast__ObjectAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3277:1: ( ( ruleTerminalExpression ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3278:1: ( ruleTerminalExpression )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3278:1: ( ruleTerminalExpression )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3279:1: ruleTerminalExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCastAccess().getObjectTerminalExpressionParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleTerminalExpression_in_rule__Cast__ObjectAssignment_36608);
            ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCastAccess().getObjectTerminalExpressionParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cast__ObjectAssignment_3"


    // $ANTLR start "rule__StringConstant__ConstantAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3288:1: rule__StringConstant__ConstantAssignment : ( RULE_STRING ) ;
    public final void rule__StringConstant__ConstantAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3292:1: ( ( RULE_STRING ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3293:1: ( RULE_STRING )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3293:1: ( RULE_STRING )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3294:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringConstantAccess().getConstantSTRINGTerminalRuleCall_0()); 
            }
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__StringConstant__ConstantAssignment6639); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringConstantAccess().getConstantSTRINGTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringConstant__ConstantAssignment"


    // $ANTLR start "rule__IntConstant__ConstantAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3303:1: rule__IntConstant__ConstantAssignment : ( RULE_INT ) ;
    public final void rule__IntConstant__ConstantAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3307:1: ( ( RULE_INT ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3308:1: ( RULE_INT )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3308:1: ( RULE_INT )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3309:1: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntConstantAccess().getConstantINTTerminalRuleCall_0()); 
            }
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__IntConstant__ConstantAssignment6670); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntConstantAccess().getConstantINTTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntConstant__ConstantAssignment"


    // $ANTLR start "rule__BoolConstant__ConstantAssignment"
    // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3318:1: rule__BoolConstant__ConstantAssignment : ( ( rule__BoolConstant__ConstantAlternatives_0 ) ) ;
    public final void rule__BoolConstant__ConstantAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3322:1: ( ( ( rule__BoolConstant__ConstantAlternatives_0 ) ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3323:1: ( ( rule__BoolConstant__ConstantAlternatives_0 ) )
            {
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3323:1: ( ( rule__BoolConstant__ConstantAlternatives_0 ) )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3324:1: ( rule__BoolConstant__ConstantAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBoolConstantAccess().getConstantAlternatives_0()); 
            }
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3325:1: ( rule__BoolConstant__ConstantAlternatives_0 )
            // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:3325:2: rule__BoolConstant__ConstantAlternatives_0
            {
            pushFollow(FOLLOW_rule__BoolConstant__ConstantAlternatives_0_in_rule__BoolConstant__ConstantAssignment6701);
            rule__BoolConstant__ConstantAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBoolConstantAccess().getConstantAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoolConstant__ConstantAssignment"

    // $ANTLR start synpred8_InternalFJ
    public final void synpred8_InternalFJ_fragment() throws RecognitionException {   
        // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:838:6: ( ( ruleCast ) )
        // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:838:6: ( ruleCast )
        {
        // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:838:6: ( ruleCast )
        // ../org.eclipse.xtext.example.fj.ui/src-gen/org/eclipse/xtext/example/ui/contentassist/antlr/internal/InternalFJ.g:839:1: ruleCast
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getTerminalExpressionAccess().getCastParserRuleCall_3()); 
        }
        pushFollow(FOLLOW_ruleCast_in_synpred8_InternalFJ1742);
        ruleCast();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred8_InternalFJ

    // Delegated rules

    public final boolean synpred8_InternalFJ() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_InternalFJ_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA4 dfa4 = new DFA4(this);
    static final String DFA4_eotS =
        "\13\uffff";
    static final String DFA4_eofS =
        "\13\uffff";
    static final String DFA4_minS =
        "\1\4\3\uffff\1\0\6\uffff";
    static final String DFA4_maxS =
        "\1\33\3\uffff\1\0\6\uffff";
    static final String DFA4_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\uffff\1\5\3\uffff\1\4\1\6";
    static final String DFA4_specialS =
        "\4\uffff\1\0\6\uffff}>";
    static final String[] DFA4_transitionS = {
            "\1\2\2\5\7\uffff\2\5\5\uffff\1\4\4\uffff\1\3\1\1",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "815:1: rule__TerminalExpression__Alternatives : ( ( ruleThis ) | ( ruleVariable ) | ( ruleNew ) | ( ruleCast ) | ( ruleConstant ) | ( ruleParen ) );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA4_4 = input.LA(1);

                         
                        int index4_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred8_InternalFJ()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index4_4);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 4, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleProgram_in_entryRuleProgram67 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProgram74 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Program__Group__0_in_ruleProgram100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType127 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__Alternatives_in_ruleType160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicType_in_entryRuleBasicType187 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBasicType194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicType__BasicAssignment_in_ruleBasicType220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClassType_in_entryRuleClassType247 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClassType254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ClassType__ClassrefAssignment_in_ruleClassType280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_entryRuleClass309 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClass316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__0_in_ruleClass342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleField_in_entryRuleField369 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleField376 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Field__Group__0_in_ruleField402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter429 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__0_in_ruleParameter462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethod_in_entryRuleMethod489 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMethod496 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__0_in_ruleMethod522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethodBody_in_entryRuleMethodBody549 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMethodBody556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodBody__Group__0_in_ruleMethodBody582 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression609 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group__0_in_ruleExpression642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage669 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessage676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__Alternatives_in_ruleMessage702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethodCall_in_entryRuleMethodCall729 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMethodCall736 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__0_in_ruleMethodCall762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFieldSelection_in_entryRuleFieldSelection789 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFieldSelection796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FieldSelection__NameAssignment_in_ruleFieldSelection822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerminalExpression_in_entryRuleTerminalExpression849 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTerminalExpression856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TerminalExpression__Alternatives_in_ruleTerminalExpression882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleThis_in_entryRuleThis909 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleThis916 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__This__VariableAssignment_in_ruleThis942 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_entryRuleVariable969 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariable976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Variable__ParamrefAssignment_in_ruleVariable1002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNew_in_entryRuleNew1029 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNew1036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group__0_in_ruleNew1062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCast_in_entryRuleCast1089 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCast1096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Cast__Group__0_in_ruleCast1122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParen_in_entryRuleParen1149 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParen1156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Paren__Group__0_in_ruleParen1182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant1209 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstant1216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constant__Alternatives_in_ruleConstant1242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringConstant_in_entryRuleStringConstant1269 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStringConstant1276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__StringConstant__ConstantAssignment_in_ruleStringConstant1302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntConstant_in_entryRuleIntConstant1329 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntConstant1336 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IntConstant__ConstantAssignment_in_ruleIntConstant1362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant1389 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBoolConstant1396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BoolConstant__ConstantAssignment_in_ruleBoolConstant1422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArgument_in_entryRuleArgument1449 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArgument1456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleArgument1482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicType_in_rule__Type__Alternatives1517 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClassType_in_rule__Type__Alternatives1534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__BasicType__BasicAlternatives_01567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__BasicType__BasicAlternatives_01587 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__BasicType__BasicAlternatives_01607 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethodCall_in_rule__Message__Alternatives1642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFieldSelection_in_rule__Message__Alternatives1659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleThis_in_rule__TerminalExpression__Alternatives1691 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_rule__TerminalExpression__Alternatives1708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNew_in_rule__TerminalExpression__Alternatives1725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCast_in_rule__TerminalExpression__Alternatives1742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_rule__TerminalExpression__Alternatives1759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParen_in_rule__TerminalExpression__Alternatives1776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntConstant_in_rule__Constant__Alternatives1808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolConstant_in_rule__Constant__Alternatives1825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringConstant_in_rule__Constant__Alternatives1842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__BoolConstant__ConstantAlternatives_01875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__BoolConstant__ConstantAlternatives_01895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Program__Group__0__Impl_in_rule__Program__Group__01927 = new BitSet(new long[]{0x000000000C20C070L});
    public static final BitSet FOLLOW_rule__Program__Group__1_in_rule__Program__Group__01930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Program__ClassesAssignment_0_in_rule__Program__Group__0__Impl1957 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Program__Group__1__Impl_in_rule__Program__Group__11988 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Program__MainAssignment_1_in_rule__Program__Group__1__Impl2015 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__0__Impl_in_rule__Class__Group__02050 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Class__Group__1_in_rule__Class__Group__02053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Class__Group__0__Impl2081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__1__Impl_in_rule__Class__Group__12112 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_rule__Class__Group__2_in_rule__Class__Group__12115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__NameAssignment_1_in_rule__Class__Group__1__Impl2142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__2__Impl_in_rule__Class__Group__22172 = new BitSet(new long[]{0x00000000000A0000L});
    public static final BitSet FOLLOW_rule__Class__Group__3_in_rule__Class__Group__22175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group_2__0_in_rule__Class__Group__2__Impl2202 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__3__Impl_in_rule__Class__Group__32233 = new BitSet(new long[]{0x0000000000043810L});
    public static final BitSet FOLLOW_rule__Class__Group__4_in_rule__Class__Group__32236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Class__Group__3__Impl2264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group__4__Impl_in_rule__Class__Group__42295 = new BitSet(new long[]{0x0000000000043810L});
    public static final BitSet FOLLOW_rule__Class__Group__5_in_rule__Class__Group__42298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__FieldsAssignment_4_in_rule__Class__Group__4__Impl2325 = new BitSet(new long[]{0x0000000000003812L});
    public static final BitSet FOLLOW_rule__Class__Group__5__Impl_in_rule__Class__Group__52356 = new BitSet(new long[]{0x0000000000043810L});
    public static final BitSet FOLLOW_rule__Class__Group__6_in_rule__Class__Group__52359 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__MethodsAssignment_5_in_rule__Class__Group__5__Impl2386 = new BitSet(new long[]{0x0000000000003812L});
    public static final BitSet FOLLOW_rule__Class__Group__6__Impl_in_rule__Class__Group__62417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Class__Group__6__Impl2445 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group_2__0__Impl_in_rule__Class__Group_2__02490 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Class__Group_2__1_in_rule__Class__Group_2__02493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Class__Group_2__0__Impl2521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__Group_2__1__Impl_in_rule__Class__Group_2__12552 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Class__ExtendsAssignment_2_1_in_rule__Class__Group_2__1__Impl2579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Field__Group__0__Impl_in_rule__Field__Group__02613 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Field__Group__1_in_rule__Field__Group__02616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Field__TypeAssignment_0_in_rule__Field__Group__0__Impl2643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Field__Group__1__Impl_in_rule__Field__Group__12673 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Field__Group__2_in_rule__Field__Group__12676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Field__NameAssignment_1_in_rule__Field__Group__1__Impl2703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Field__Group__2__Impl_in_rule__Field__Group__22733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Field__Group__2__Impl2761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__0__Impl_in_rule__Parameter__Group__02798 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Parameter__Group__1_in_rule__Parameter__Group__02801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__TypeAssignment_0_in_rule__Parameter__Group__0__Impl2828 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__Group__1__Impl_in_rule__Parameter__Group__12858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Parameter__NameAssignment_1_in_rule__Parameter__Group__1__Impl2885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__0__Impl_in_rule__Method__Group__02919 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Method__Group__1_in_rule__Method__Group__02922 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__ReturntypeAssignment_0_in_rule__Method__Group__0__Impl2949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__1__Impl_in_rule__Method__Group__12979 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Method__Group__2_in_rule__Method__Group__12982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__NameAssignment_1_in_rule__Method__Group__1__Impl3009 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__2__Impl_in_rule__Method__Group__23039 = new BitSet(new long[]{0x0000000000403810L});
    public static final BitSet FOLLOW_rule__Method__Group__3_in_rule__Method__Group__23042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Method__Group__2__Impl3070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__3__Impl_in_rule__Method__Group__33101 = new BitSet(new long[]{0x0000000000403810L});
    public static final BitSet FOLLOW_rule__Method__Group__4_in_rule__Method__Group__33104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group_3__0_in_rule__Method__Group__3__Impl3131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__4__Impl_in_rule__Method__Group__43162 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Method__Group__5_in_rule__Method__Group__43165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Method__Group__4__Impl3193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__5__Impl_in_rule__Method__Group__53224 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__Method__Group__6_in_rule__Method__Group__53227 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Method__Group__5__Impl3255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__6__Impl_in_rule__Method__Group__63286 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Method__Group__7_in_rule__Method__Group__63289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__BodyAssignment_6_in_rule__Method__Group__6__Impl3316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group__7__Impl_in_rule__Method__Group__73346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Method__Group__7__Impl3374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group_3__0__Impl_in_rule__Method__Group_3__03421 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Method__Group_3__1_in_rule__Method__Group_3__03424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__ParamsAssignment_3_0_in_rule__Method__Group_3__0__Impl3451 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group_3__1__Impl_in_rule__Method__Group_3__13481 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group_3_1__0_in_rule__Method__Group_3__1__Impl3508 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_rule__Method__Group_3_1__0__Impl_in_rule__Method__Group_3_1__03543 = new BitSet(new long[]{0x0000000000003810L});
    public static final BitSet FOLLOW_rule__Method__Group_3_1__1_in_rule__Method__Group_3_1__03546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Method__Group_3_1__0__Impl3574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__Group_3_1__1__Impl_in_rule__Method__Group_3_1__13605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Method__ParamsAssignment_3_1_1_in_rule__Method__Group_3_1__1__Impl3632 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodBody__Group__0__Impl_in_rule__MethodBody__Group__03666 = new BitSet(new long[]{0x000000000C20C070L});
    public static final BitSet FOLLOW_rule__MethodBody__Group__1_in_rule__MethodBody__Group__03669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__MethodBody__Group__0__Impl3697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodBody__Group__1__Impl_in_rule__MethodBody__Group__13728 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__MethodBody__Group__2_in_rule__MethodBody__Group__13731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodBody__ExpressionAssignment_1_in_rule__MethodBody__Group__1__Impl3758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodBody__Group__2__Impl_in_rule__MethodBody__Group__23788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__MethodBody__Group__2__Impl3816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group__0__Impl_in_rule__Expression__Group__03853 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__Expression__Group__1_in_rule__Expression__Group__03856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerminalExpression_in_rule__Expression__Group__0__Impl3883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group__1__Impl_in_rule__Expression__Group__13912 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group_1__0_in_rule__Expression__Group__1__Impl3939 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_rule__Expression__Group_1__0__Impl_in_rule__Expression__Group_1__03974 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__Expression__Group_1__1_in_rule__Expression__Group_1__03977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group_1__1__Impl_in_rule__Expression__Group_1__14035 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Expression__Group_1__2_in_rule__Expression__Group_1__14038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__Expression__Group_1__1__Impl4066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group_1__2__Impl_in_rule__Expression__Group_1__24097 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__MessageAssignment_1_2_in_rule__Expression__Group_1__2__Impl4124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__0__Impl_in_rule__MethodCall__Group__04160 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__1_in_rule__MethodCall__Group__04163 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__NameAssignment_0_in_rule__MethodCall__Group__0__Impl4190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__1__Impl_in_rule__MethodCall__Group__14220 = new BitSet(new long[]{0x000000000C60C070L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__2_in_rule__MethodCall__Group__14223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__MethodCall__Group__1__Impl4251 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__2__Impl_in_rule__MethodCall__Group__24282 = new BitSet(new long[]{0x000000000C60C070L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__3_in_rule__MethodCall__Group__24285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2__0_in_rule__MethodCall__Group__2__Impl4312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group__3__Impl_in_rule__MethodCall__Group__34343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__MethodCall__Group__3__Impl4371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2__0__Impl_in_rule__MethodCall__Group_2__04410 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2__1_in_rule__MethodCall__Group_2__04413 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__ArgsAssignment_2_0_in_rule__MethodCall__Group_2__0__Impl4440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2__1__Impl_in_rule__MethodCall__Group_2__14470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__0_in_rule__MethodCall__Group_2__1__Impl4497 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__0__Impl_in_rule__MethodCall__Group_2_1__04532 = new BitSet(new long[]{0x000000000C20C070L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__1_in_rule__MethodCall__Group_2_1__04535 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__MethodCall__Group_2_1__0__Impl4563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__Group_2_1__1__Impl_in_rule__MethodCall__Group_2_1__14594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MethodCall__ArgsAssignment_2_1_1_in_rule__MethodCall__Group_2_1__1__Impl4621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group__0__Impl_in_rule__New__Group__04655 = new BitSet(new long[]{0x0000000000003810L});
    public static final BitSet FOLLOW_rule__New__Group__1_in_rule__New__Group__04658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__New__Group__0__Impl4686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group__1__Impl_in_rule__New__Group__14717 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__New__Group__2_in_rule__New__Group__14720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__TypeAssignment_1_in_rule__New__Group__1__Impl4747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group__2__Impl_in_rule__New__Group__24777 = new BitSet(new long[]{0x000000000C60C070L});
    public static final BitSet FOLLOW_rule__New__Group__3_in_rule__New__Group__24780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__New__Group__2__Impl4808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group__3__Impl_in_rule__New__Group__34839 = new BitSet(new long[]{0x000000000C60C070L});
    public static final BitSet FOLLOW_rule__New__Group__4_in_rule__New__Group__34842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group_3__0_in_rule__New__Group__3__Impl4869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group__4__Impl_in_rule__New__Group__44900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__New__Group__4__Impl4928 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group_3__0__Impl_in_rule__New__Group_3__04969 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__New__Group_3__1_in_rule__New__Group_3__04972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__ArgsAssignment_3_0_in_rule__New__Group_3__0__Impl4999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group_3__1__Impl_in_rule__New__Group_3__15029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group_3_1__0_in_rule__New__Group_3__1__Impl5056 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_rule__New__Group_3_1__0__Impl_in_rule__New__Group_3_1__05091 = new BitSet(new long[]{0x000000000C20C070L});
    public static final BitSet FOLLOW_rule__New__Group_3_1__1_in_rule__New__Group_3_1__05094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__New__Group_3_1__0__Impl5122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__Group_3_1__1__Impl_in_rule__New__Group_3_1__15153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__New__ArgsAssignment_3_1_1_in_rule__New__Group_3_1__1__Impl5180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Cast__Group__0__Impl_in_rule__Cast__Group__05214 = new BitSet(new long[]{0x0000000000003810L});
    public static final BitSet FOLLOW_rule__Cast__Group__1_in_rule__Cast__Group__05217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Cast__Group__0__Impl5245 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Cast__Group__1__Impl_in_rule__Cast__Group__15276 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Cast__Group__2_in_rule__Cast__Group__15279 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Cast__TypeAssignment_1_in_rule__Cast__Group__1__Impl5306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Cast__Group__2__Impl_in_rule__Cast__Group__25336 = new BitSet(new long[]{0x000000000C20C070L});
    public static final BitSet FOLLOW_rule__Cast__Group__3_in_rule__Cast__Group__25339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Cast__Group__2__Impl5367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Cast__Group__3__Impl_in_rule__Cast__Group__35398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Cast__ObjectAssignment_3_in_rule__Cast__Group__3__Impl5425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Paren__Group__0__Impl_in_rule__Paren__Group__05463 = new BitSet(new long[]{0x000000000C20C070L});
    public static final BitSet FOLLOW_rule__Paren__Group__1_in_rule__Paren__Group__05466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Paren__Group__0__Impl5494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Paren__Group__1__Impl_in_rule__Paren__Group__15525 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Paren__Group__2_in_rule__Paren__Group__15528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Paren__Group__1__Impl5555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Paren__Group__2__Impl_in_rule__Paren__Group__25584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Paren__Group__2__Impl5612 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_rule__Program__ClassesAssignment_05654 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Program__MainAssignment_15685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicType__BasicAlternatives_0_in_rule__BasicType__BasicAssignment5716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ClassType__ClassrefAssignment5753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Class__NameAssignment_15788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Class__ExtendsAssignment_2_15823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleField_in_rule__Class__FieldsAssignment_45858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethod_in_rule__Class__MethodsAssignment_55889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__Field__TypeAssignment_05920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Field__NameAssignment_15951 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__Parameter__TypeAssignment_05982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Parameter__NameAssignment_16013 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__Method__ReturntypeAssignment_06044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Method__NameAssignment_16075 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_06106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_rule__Method__ParamsAssignment_3_1_16137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethodBody_in_rule__Method__BodyAssignment_66168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__MethodBody__ExpressionAssignment_16199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_rule__Expression__MessageAssignment_1_26230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__MethodCall__NameAssignment_06265 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_06300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArgument_in_rule__MethodCall__ArgsAssignment_2_1_16331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__FieldSelection__NameAssignment6366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__This__VariableAssignment6406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Variable__ParamrefAssignment6449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClassType_in_rule__New__TypeAssignment_16484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_06515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArgument_in_rule__New__ArgsAssignment_3_1_16546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClassType_in_rule__Cast__TypeAssignment_16577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerminalExpression_in_rule__Cast__ObjectAssignment_36608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__StringConstant__ConstantAssignment6639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__IntConstant__ConstantAssignment6670 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BoolConstant__ConstantAlternatives_0_in_rule__BoolConstant__ConstantAssignment6701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCast_in_synpred8_InternalFJ1742 = new BitSet(new long[]{0x0000000000000002L});

}