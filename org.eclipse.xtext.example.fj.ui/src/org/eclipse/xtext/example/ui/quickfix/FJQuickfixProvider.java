
package org.eclipse.xtext.example.ui.quickfix;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.validation.FJJavaValidator;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.xtext.ui.editor.model.edit.IModification;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.util.Strings;


public class FJQuickfixProvider extends DefaultQuickfixProvider {

	@Fix(FJJavaValidator.INVALID_CLASS_NAME)
	public void capitalizeName(final Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, "Capitalize name", "Capitalize the name.", "upcase.png", new IModification() {
			public void apply(IModificationContext context) throws BadLocationException {
				IXtextDocument xtextDocument = context.getXtextDocument();
				String firstLetter = xtextDocument.get(issue.getOffset(), 1);
				xtextDocument.replace(issue.getOffset(), 1, firstLetter.toUpperCase());
			}
		});
	}

	@Fix(FJJavaValidator.DUPLICATE_CLASS_NAMES)
	public void removeDuplicateClass(final Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, "Remove class", "Remove the duplicate class.", "class_obj.gif", 
			new ISemanticModification() {
				public void apply(EObject element, IModificationContext context) {
					Class duplicateClass = (Class)element;
					Program prog = (Program)duplicateClass.eContainer();
					prog.getClasses().remove(duplicateClass);
				}
			}
		);
	}
}
