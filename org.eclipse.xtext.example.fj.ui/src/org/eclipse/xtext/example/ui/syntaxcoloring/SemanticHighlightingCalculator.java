/*******************************************************************************
 * Copyright (c) 2009 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.eclipse.xtext.example.ui.syntaxcoloring;

import java.util.List;

import org.eclipse.xtext.example.fj.FieldSelection;
import org.eclipse.xtext.example.fj.FjPackage;
import org.eclipse.xtext.example.fj.Program;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.ILeafNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator;

/**
 * Performs semantic highlighting (highlight differently field names
 * in all field selections expressions)
 * 
 * @author bettini
 *
 */
public class SemanticHighlightingCalculator implements
		ISemanticHighlightingCalculator {

	public void provideHighlightingFor(final XtextResource resource,
			IHighlightedPositionAcceptor acceptor) {
		if (resource == null || resource.getContents().size() == 0)
			return;
		Program program = (Program) resource.getContents().get(0);

		// retrieve all the field selections in a program
		List<FieldSelection> fieldSelections = EcoreUtil2.getAllContentsOfType(
				program, FieldSelection.class);
		for (FieldSelection fieldSelection : fieldSelections) {
			// retrieve the node representing the field name in field selection
			ILeafNode nodeToHighlight = getFirstFeatureNode(fieldSelection,
					FjPackage.Literals.FIELD_SELECTION__NAME.getName());
			// highlight the field reference (field name in a field selection)
			acceptor.addPosition(nodeToHighlight.getOffset(), nodeToHighlight
					.getLength(), SemanticHighlightingConfiguration.FIELD_REF);
		}

		/*
		Iterable<AbstractNode> allNodes = NodeUtil.getAllContents(resource
				.getParseResult().getRootNode());
		for (AbstractNode abstractNode : allNodes) {
			if (abstractNode.getGrammarElement() instanceof CrossReference) {
				highlightNode(abstractNode,
						SemanticHighlightingConfiguration.FIELD_REF, acceptor);
			}
		}
		*/
	}

	/**
	 * Taken from {@link http://blogs.itemis.de/stundzig/archives/467}.
	 * 
	 * Adapted from Sebastian Zarnekow's semantic highlighting implementation
	 * navigate to the parse node corresponding to the semantic object and fetch
	 * the leaf node that corresponds to the first feature with the given name.
	 * 
	 * @param semantic
	 * @param feature
	 * @return
	 */
	public ILeafNode getFirstFeatureNode(EObject semantic, String feature) {
		ICompositeNode node = NodeModelUtils.getNode(semantic);
		if (node != null) {
			if (feature == null)
				return null;
			for (INode child : node.getChildren()) {
				if (child instanceof ILeafNode) {
					if (feature.equals(((ILeafNode) child).getText())) {
						return (ILeafNode) child;
					}
				}
			}
		}
		return null;
	}

}