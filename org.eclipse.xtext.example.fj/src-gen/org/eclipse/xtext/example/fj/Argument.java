/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getArgument()
 * @model
 * @generated
 */
public interface Argument extends EObject
{
} // Argument
