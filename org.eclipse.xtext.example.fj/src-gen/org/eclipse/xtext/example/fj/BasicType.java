/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.xtext.example.fj.BasicType#getBasic <em>Basic</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getBasicType()
 * @model
 * @generated
 */
public interface BasicType extends Type
{
  /**
   * Returns the value of the '<em><b>Basic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Basic</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Basic</em>' attribute.
   * @see #setBasic(String)
   * @see org.eclipse.xtext.example.fj.FjPackage#getBasicType_Basic()
   * @model
   * @generated
   */
  String getBasic();

  /**
   * Sets the value of the '{@link org.eclipse.xtext.example.fj.BasicType#getBasic <em>Basic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Basic</em>' attribute.
   * @see #getBasic()
   * @generated
   */
  void setBasic(String value);

} // BasicType
