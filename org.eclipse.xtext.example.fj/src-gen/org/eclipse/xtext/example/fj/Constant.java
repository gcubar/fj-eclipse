/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getConstant()
 * @model
 * @generated
 */
public interface Constant extends Expression
{
} // Constant
