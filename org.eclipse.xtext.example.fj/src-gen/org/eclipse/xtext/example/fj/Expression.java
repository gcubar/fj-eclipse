/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends Argument
{
} // Expression
