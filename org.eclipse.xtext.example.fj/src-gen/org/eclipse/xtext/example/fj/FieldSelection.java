/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.xtext.example.fj.FieldSelection#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getFieldSelection()
 * @model
 * @generated
 */
public interface FieldSelection extends Message
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(Field)
   * @see org.eclipse.xtext.example.fj.FjPackage#getFieldSelection_Name()
   * @model
   * @generated
   */
  Field getName();

  /**
   * Sets the value of the '{@link org.eclipse.xtext.example.fj.FieldSelection#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(Field value);

} // FieldSelection
