/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getMessage()
 * @model
 * @generated
 */
public interface Message extends EObject
{
} // Message
