/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.xtext.example.fj.MethodCall#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.xtext.example.fj.MethodCall#getArgs <em>Args</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getMethodCall()
 * @model
 * @generated
 */
public interface MethodCall extends Message
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(Method)
   * @see org.eclipse.xtext.example.fj.FjPackage#getMethodCall_Name()
   * @model
   * @generated
   */
  Method getName();

  /**
   * Sets the value of the '{@link org.eclipse.xtext.example.fj.MethodCall#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(Method value);

  /**
   * Returns the value of the '<em><b>Args</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.xtext.example.fj.Argument}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Args</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Args</em>' containment reference list.
   * @see org.eclipse.xtext.example.fj.FjPackage#getMethodCall_Args()
   * @model containment="true"
   * @generated
   */
  EList<Argument> getArgs();

} // MethodCall
