/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends TypedElement
{
} // Parameter
