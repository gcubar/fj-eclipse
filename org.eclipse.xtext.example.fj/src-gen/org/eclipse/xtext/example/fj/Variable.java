/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.xtext.example.fj.Variable#getParamref <em>Paramref</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.xtext.example.fj.FjPackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends Expression
{
  /**
   * Returns the value of the '<em><b>Paramref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Paramref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Paramref</em>' reference.
   * @see #setParamref(Parameter)
   * @see org.eclipse.xtext.example.fj.FjPackage#getVariable_Paramref()
   * @model
   * @generated
   */
  Parameter getParamref();

  /**
   * Sets the value of the '{@link org.eclipse.xtext.example.fj.Variable#getParamref <em>Paramref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Paramref</em>' reference.
   * @see #getParamref()
   * @generated
   */
  void setParamref(Parameter value);

} // Variable
