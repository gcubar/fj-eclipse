/**
 * <copyright>
 * </copyright>
 *
 */
package org.eclipse.xtext.example.fj.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.xtext.example.fj.Argument;
import org.eclipse.xtext.example.fj.BasicType;
import org.eclipse.xtext.example.fj.BoolConstant;
import org.eclipse.xtext.example.fj.Cast;
import org.eclipse.xtext.example.fj.ClassType;
import org.eclipse.xtext.example.fj.Constant;
import org.eclipse.xtext.example.fj.Expression;
import org.eclipse.xtext.example.fj.Field;
import org.eclipse.xtext.example.fj.FieldSelection;
import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.FjPackage;
import org.eclipse.xtext.example.fj.IntConstant;
import org.eclipse.xtext.example.fj.Message;
import org.eclipse.xtext.example.fj.Method;
import org.eclipse.xtext.example.fj.MethodBody;
import org.eclipse.xtext.example.fj.MethodCall;
import org.eclipse.xtext.example.fj.New;
import org.eclipse.xtext.example.fj.Parameter;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.fj.Selection;
import org.eclipse.xtext.example.fj.StringConstant;
import org.eclipse.xtext.example.fj.This;
import org.eclipse.xtext.example.fj.Type;
import org.eclipse.xtext.example.fj.TypedElement;
import org.eclipse.xtext.example.fj.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FjFactoryImpl extends EFactoryImpl implements FjFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static FjFactory init()
  {
    try
    {
      FjFactory theFjFactory = (FjFactory)EPackage.Registry.INSTANCE.getEFactory("http://example.xtext.org/FJ"); 
      if (theFjFactory != null)
      {
        return theFjFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new FjFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FjFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case FjPackage.PROGRAM: return createProgram();
      case FjPackage.TYPE: return createType();
      case FjPackage.BASIC_TYPE: return createBasicType();
      case FjPackage.CLASS_TYPE: return createClassType();
      case FjPackage.TYPED_ELEMENT: return createTypedElement();
      case FjPackage.CLASS: return createClass();
      case FjPackage.FIELD: return createField();
      case FjPackage.PARAMETER: return createParameter();
      case FjPackage.METHOD: return createMethod();
      case FjPackage.METHOD_BODY: return createMethodBody();
      case FjPackage.EXPRESSION: return createExpression();
      case FjPackage.MESSAGE: return createMessage();
      case FjPackage.METHOD_CALL: return createMethodCall();
      case FjPackage.FIELD_SELECTION: return createFieldSelection();
      case FjPackage.THIS: return createThis();
      case FjPackage.VARIABLE: return createVariable();
      case FjPackage.NEW: return createNew();
      case FjPackage.CAST: return createCast();
      case FjPackage.CONSTANT: return createConstant();
      case FjPackage.STRING_CONSTANT: return createStringConstant();
      case FjPackage.INT_CONSTANT: return createIntConstant();
      case FjPackage.BOOL_CONSTANT: return createBoolConstant();
      case FjPackage.ARGUMENT: return createArgument();
      case FjPackage.SELECTION: return createSelection();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Program createProgram()
  {
    ProgramImpl program = new ProgramImpl();
    return program;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BasicType createBasicType()
  {
    BasicTypeImpl basicType = new BasicTypeImpl();
    return basicType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ClassType createClassType()
  {
    ClassTypeImpl classType = new ClassTypeImpl();
    return classType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypedElement createTypedElement()
  {
    TypedElementImpl typedElement = new TypedElementImpl();
    return typedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public org.eclipse.xtext.example.fj.Class createClass()
  {
    ClassImpl class_ = new ClassImpl();
    return class_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Field createField()
  {
    FieldImpl field = new FieldImpl();
    return field;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Method createMethod()
  {
    MethodImpl method = new MethodImpl();
    return method;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodBody createMethodBody()
  {
    MethodBodyImpl methodBody = new MethodBodyImpl();
    return methodBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Message createMessage()
  {
    MessageImpl message = new MessageImpl();
    return message;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodCall createMethodCall()
  {
    MethodCallImpl methodCall = new MethodCallImpl();
    return methodCall;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldSelection createFieldSelection()
  {
    FieldSelectionImpl fieldSelection = new FieldSelectionImpl();
    return fieldSelection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public This createThis()
  {
    ThisImpl this_ = new ThisImpl();
    return this_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public New createNew()
  {
    NewImpl new_ = new NewImpl();
    return new_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Cast createCast()
  {
    CastImpl cast = new CastImpl();
    return cast;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Constant createConstant()
  {
    ConstantImpl constant = new ConstantImpl();
    return constant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StringConstant createStringConstant()
  {
    StringConstantImpl stringConstant = new StringConstantImpl();
    return stringConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntConstant createIntConstant()
  {
    IntConstantImpl intConstant = new IntConstantImpl();
    return intConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolConstant createBoolConstant()
  {
    BoolConstantImpl boolConstant = new BoolConstantImpl();
    return boolConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Argument createArgument()
  {
    ArgumentImpl argument = new ArgumentImpl();
    return argument;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Selection createSelection()
  {
    SelectionImpl selection = new SelectionImpl();
    return selection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FjPackage getFjPackage()
  {
    return (FjPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static FjPackage getPackage()
  {
    return FjPackage.eINSTANCE;
  }

} //FjFactoryImpl
