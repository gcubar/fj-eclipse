package org.eclipse.xtext.example.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.eclipse.xtext.example.services.FJGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalFJParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'int'", "'boolean'", "'String'", "'class'", "'extends'", "'{'", "'}'", "';'", "'('", "','", "')'", "'return'", "'.'", "'this'", "'new'", "'true'", "'false'"
    };
    public static final int RULE_ID=4;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalFJParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFJParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFJParser.tokenNames; }
    public String getGrammarFileName() { return "../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */
     
     	private FJGrammarAccess grammarAccess;
     	
        public InternalFJParser(TokenStream input, FJGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Program";	
       	}
       	
       	@Override
       	protected FJGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleProgram"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:73:1: entryRuleProgram returns [EObject current=null] : iv_ruleProgram= ruleProgram EOF ;
    public final EObject entryRuleProgram() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProgram = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:74:2: (iv_ruleProgram= ruleProgram EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:75:2: iv_ruleProgram= ruleProgram EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProgramRule()); 
            }
            pushFollow(FOLLOW_ruleProgram_in_entryRuleProgram81);
            iv_ruleProgram=ruleProgram();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProgram; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProgram91); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProgram"


    // $ANTLR start "ruleProgram"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:82:1: ruleProgram returns [EObject current=null] : ( ( (lv_classes_0_0= ruleClass ) )* ( (lv_main_1_0= ruleExpression ) )? ) ;
    public final EObject ruleProgram() throws RecognitionException {
        EObject current = null;

        EObject lv_classes_0_0 = null;

        EObject lv_main_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:85:28: ( ( ( (lv_classes_0_0= ruleClass ) )* ( (lv_main_1_0= ruleExpression ) )? ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:86:1: ( ( (lv_classes_0_0= ruleClass ) )* ( (lv_main_1_0= ruleExpression ) )? )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:86:1: ( ( (lv_classes_0_0= ruleClass ) )* ( (lv_main_1_0= ruleExpression ) )? )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:86:2: ( (lv_classes_0_0= ruleClass ) )* ( (lv_main_1_0= ruleExpression ) )?
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:86:2: ( (lv_classes_0_0= ruleClass ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:87:1: (lv_classes_0_0= ruleClass )
            	    {
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:87:1: (lv_classes_0_0= ruleClass )
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:88:3: lv_classes_0_0= ruleClass
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getClassesClassParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleClass_in_ruleProgram137);
            	    lv_classes_0_0=ruleClass();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"classes",
            	              		lv_classes_0_0, 
            	              		"Class");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:104:3: ( (lv_main_1_0= ruleExpression ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>=RULE_ID && LA2_0<=RULE_INT)||LA2_0==19||(LA2_0>=24 && LA2_0<=27)) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:105:1: (lv_main_1_0= ruleExpression )
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:105:1: (lv_main_1_0= ruleExpression )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:106:3: lv_main_1_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getProgramAccess().getMainExpressionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleProgram159);
                    lv_main_1_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getProgramRule());
                      	        }
                             		set(
                             			current, 
                             			"main",
                              		lv_main_1_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProgram"


    // $ANTLR start "entryRuleType"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:130:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:131:2: (iv_ruleType= ruleType EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:132:2: iv_ruleType= ruleType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeRule()); 
            }
            pushFollow(FOLLOW_ruleType_in_entryRuleType196);
            iv_ruleType=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleType206); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:139:1: ruleType returns [EObject current=null] : (this_BasicType_0= ruleBasicType | this_ClassType_1= ruleClassType ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_BasicType_0 = null;

        EObject this_ClassType_1 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:142:28: ( (this_BasicType_0= ruleBasicType | this_ClassType_1= ruleClassType ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:143:1: (this_BasicType_0= ruleBasicType | this_ClassType_1= ruleClassType )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:143:1: (this_BasicType_0= ruleBasicType | this_ClassType_1= ruleClassType )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( ((LA3_0>=11 && LA3_0<=13)) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:144:2: this_BasicType_0= ruleBasicType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getBasicTypeParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBasicType_in_ruleType256);
                    this_BasicType_0=ruleBasicType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BasicType_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:157:2: this_ClassType_1= ruleClassType
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeAccess().getClassTypeParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleClassType_in_ruleType286);
                    this_ClassType_1=ruleClassType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ClassType_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleBasicType"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:176:1: entryRuleBasicType returns [EObject current=null] : iv_ruleBasicType= ruleBasicType EOF ;
    public final EObject entryRuleBasicType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicType = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:177:2: (iv_ruleBasicType= ruleBasicType EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:178:2: iv_ruleBasicType= ruleBasicType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicTypeRule()); 
            }
            pushFollow(FOLLOW_ruleBasicType_in_entryRuleBasicType321);
            iv_ruleBasicType=ruleBasicType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBasicType331); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicType"


    // $ANTLR start "ruleBasicType"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:185:1: ruleBasicType returns [EObject current=null] : ( ( (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' ) ) ) ;
    public final EObject ruleBasicType() throws RecognitionException {
        EObject current = null;

        Token lv_basic_0_1=null;
        Token lv_basic_0_2=null;
        Token lv_basic_0_3=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:188:28: ( ( ( (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' ) ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:189:1: ( ( (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' ) ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:189:1: ( ( (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:190:1: ( (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:190:1: ( (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:191:1: (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:191:1: (lv_basic_0_1= 'int' | lv_basic_0_2= 'boolean' | lv_basic_0_3= 'String' )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt4=1;
                }
                break;
            case 12:
                {
                alt4=2;
                }
                break;
            case 13:
                {
                alt4=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:192:3: lv_basic_0_1= 'int'
                    {
                    lv_basic_0_1=(Token)match(input,11,FOLLOW_11_in_ruleBasicType375); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_basic_0_1, grammarAccess.getBasicTypeAccess().getBasicIntKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getBasicTypeRule());
                      	        }
                             		setWithLastConsumed(current, "basic", lv_basic_0_1, null);
                      	    
                    }

                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:204:8: lv_basic_0_2= 'boolean'
                    {
                    lv_basic_0_2=(Token)match(input,12,FOLLOW_12_in_ruleBasicType404); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_basic_0_2, grammarAccess.getBasicTypeAccess().getBasicBooleanKeyword_0_1());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getBasicTypeRule());
                      	        }
                             		setWithLastConsumed(current, "basic", lv_basic_0_2, null);
                      	    
                    }

                    }
                    break;
                case 3 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:216:8: lv_basic_0_3= 'String'
                    {
                    lv_basic_0_3=(Token)match(input,13,FOLLOW_13_in_ruleBasicType433); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_basic_0_3, grammarAccess.getBasicTypeAccess().getBasicStringKeyword_0_2());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getBasicTypeRule());
                      	        }
                             		setWithLastConsumed(current, "basic", lv_basic_0_3, null);
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicType"


    // $ANTLR start "entryRuleClassType"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:239:1: entryRuleClassType returns [EObject current=null] : iv_ruleClassType= ruleClassType EOF ;
    public final EObject entryRuleClassType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassType = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:240:2: (iv_ruleClassType= ruleClassType EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:241:2: iv_ruleClassType= ruleClassType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassTypeRule()); 
            }
            pushFollow(FOLLOW_ruleClassType_in_entryRuleClassType484);
            iv_ruleClassType=ruleClassType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClassType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleClassType494); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassType"


    // $ANTLR start "ruleClassType"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:248:1: ruleClassType returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleClassType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:251:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:252:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:252:1: ( (otherlv_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:253:1: (otherlv_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:253:1: (otherlv_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:254:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getClassTypeRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleClassType542); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getClassTypeAccess().getClassrefClassCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassType"


    // $ANTLR start "entryRuleClass"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:278:1: entryRuleClass returns [EObject current=null] : iv_ruleClass= ruleClass EOF ;
    public final EObject entryRuleClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClass = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:279:2: (iv_ruleClass= ruleClass EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:280:2: iv_ruleClass= ruleClass EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClassRule()); 
            }
            pushFollow(FOLLOW_ruleClass_in_entryRuleClass579);
            iv_ruleClass=ruleClass();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClass; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleClass589); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClass"


    // $ANTLR start "ruleClass"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:287:1: ruleClass returns [EObject current=null] : (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_fields_5_0= ruleField ) )* ( (lv_methods_6_0= ruleMethod ) )* otherlv_7= '}' ) ;
    public final EObject ruleClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        EObject lv_fields_5_0 = null;

        EObject lv_methods_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:290:28: ( (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_fields_5_0= ruleField ) )* ( (lv_methods_6_0= ruleMethod ) )* otherlv_7= '}' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:291:1: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_fields_5_0= ruleField ) )* ( (lv_methods_6_0= ruleMethod ) )* otherlv_7= '}' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:291:1: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_fields_5_0= ruleField ) )* ( (lv_methods_6_0= ruleMethod ) )* otherlv_7= '}' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:291:3: otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( (lv_fields_5_0= ruleField ) )* ( (lv_methods_6_0= ruleMethod ) )* otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleClass626); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getClassAccess().getClassKeyword_0());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:295:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:296:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:296:1: (lv_name_1_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:297:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleClass643); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getClassAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getClassRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:313:2: (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:313:4: otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleClass661); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getClassAccess().getExtendsKeyword_2_0());
                          
                    }
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:317:1: ( (otherlv_3= RULE_ID ) )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:318:1: (otherlv_3= RULE_ID )
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:318:1: (otherlv_3= RULE_ID )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:319:3: otherlv_3= RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       
                      		  /* */ 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getClassRule());
                      	        }
                              
                    }
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleClass685); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_3, grammarAccess.getClassAccess().getExtendsClassCrossReference_2_1_0()); 
                      	
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleClass699); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getClassAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:337:1: ( (lv_fields_5_0= ruleField ) )*
            loop6:
            do {
                int alt6=2;
                switch ( input.LA(1) ) {
                case 11:
                    {
                    int LA6_1 = input.LA(2);

                    if ( (LA6_1==RULE_ID) ) {
                        int LA6_6 = input.LA(3);

                        if ( (LA6_6==18) ) {
                            alt6=1;
                        }


                    }


                    }
                    break;
                case 12:
                    {
                    int LA6_2 = input.LA(2);

                    if ( (LA6_2==RULE_ID) ) {
                        int LA6_6 = input.LA(3);

                        if ( (LA6_6==18) ) {
                            alt6=1;
                        }


                    }


                    }
                    break;
                case 13:
                    {
                    int LA6_3 = input.LA(2);

                    if ( (LA6_3==RULE_ID) ) {
                        int LA6_6 = input.LA(3);

                        if ( (LA6_6==18) ) {
                            alt6=1;
                        }


                    }


                    }
                    break;
                case RULE_ID:
                    {
                    int LA6_4 = input.LA(2);

                    if ( (LA6_4==RULE_ID) ) {
                        int LA6_6 = input.LA(3);

                        if ( (LA6_6==18) ) {
                            alt6=1;
                        }


                    }


                    }
                    break;

                }

                switch (alt6) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:338:1: (lv_fields_5_0= ruleField )
            	    {
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:338:1: (lv_fields_5_0= ruleField )
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:339:3: lv_fields_5_0= ruleField
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getClassAccess().getFieldsFieldParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleField_in_ruleClass720);
            	    lv_fields_5_0=ruleField();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getClassRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fields",
            	              		lv_fields_5_0, 
            	              		"Field");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:355:3: ( (lv_methods_6_0= ruleMethod ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID||(LA7_0>=11 && LA7_0<=13)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:356:1: (lv_methods_6_0= ruleMethod )
            	    {
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:356:1: (lv_methods_6_0= ruleMethod )
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:357:3: lv_methods_6_0= ruleMethod
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getClassAccess().getMethodsMethodParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleMethod_in_ruleClass742);
            	    lv_methods_6_0=ruleMethod();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getClassRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"methods",
            	              		lv_methods_6_0, 
            	              		"Method");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_7=(Token)match(input,17,FOLLOW_17_in_ruleClass755); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getClassAccess().getRightCurlyBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClass"


    // $ANTLR start "entryRuleField"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:385:1: entryRuleField returns [EObject current=null] : iv_ruleField= ruleField EOF ;
    public final EObject entryRuleField() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleField = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:386:2: (iv_ruleField= ruleField EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:387:2: iv_ruleField= ruleField EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldRule()); 
            }
            pushFollow(FOLLOW_ruleField_in_entryRuleField791);
            iv_ruleField=ruleField();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleField; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleField801); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleField"


    // $ANTLR start "ruleField"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:394:1: ruleField returns [EObject current=null] : ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) ;
    public final EObject ruleField() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_type_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:397:28: ( ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:398:1: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:398:1: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:398:2: ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';'
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:398:2: ( (lv_type_0_0= ruleType ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:399:1: (lv_type_0_0= ruleType )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:399:1: (lv_type_0_0= ruleType )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:400:3: lv_type_0_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFieldAccess().getTypeTypeParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleType_in_ruleField847);
            lv_type_0_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFieldRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_0_0, 
                      		"Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:416:2: ( (lv_name_1_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:417:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:417:1: (lv_name_1_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:418:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleField864); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getFieldAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getFieldRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleField881); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getFieldAccess().getSemicolonKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleField"


    // $ANTLR start "entryRuleParameter"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:446:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:447:2: (iv_ruleParameter= ruleParameter EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:448:2: iv_ruleParameter= ruleParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterRule()); 
            }
            pushFollow(FOLLOW_ruleParameter_in_entryRuleParameter917);
            iv_ruleParameter=ruleParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameter; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleParameter927); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:455:1: ruleParameter returns [EObject current=null] : ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        EObject lv_type_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:458:28: ( ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:459:1: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:459:1: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:459:2: ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:459:2: ( (lv_type_0_0= ruleType ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:460:1: (lv_type_0_0= ruleType )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:460:1: (lv_type_0_0= ruleType )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:461:3: lv_type_0_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParameterAccess().getTypeTypeParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleType_in_ruleParameter973);
            lv_type_0_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParameterRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_0_0, 
                      		"Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:477:2: ( (lv_name_1_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:478:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:478:1: (lv_name_1_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:479:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleParameter990); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getParameterRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleMethod"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:503:1: entryRuleMethod returns [EObject current=null] : iv_ruleMethod= ruleMethod EOF ;
    public final EObject entryRuleMethod() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMethod = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:504:2: (iv_ruleMethod= ruleMethod EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:505:2: iv_ruleMethod= ruleMethod EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMethodRule()); 
            }
            pushFollow(FOLLOW_ruleMethod_in_entryRuleMethod1031);
            iv_ruleMethod=ruleMethod();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMethod; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMethod1041); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMethod"


    // $ANTLR start "ruleMethod"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:512:1: ruleMethod returns [EObject current=null] : ( ( (lv_returntype_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_body_8_0= ruleMethodBody ) ) otherlv_9= '}' ) ;
    public final EObject ruleMethod() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_returntype_0_0 = null;

        EObject lv_params_3_0 = null;

        EObject lv_params_5_0 = null;

        EObject lv_body_8_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:515:28: ( ( ( (lv_returntype_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_body_8_0= ruleMethodBody ) ) otherlv_9= '}' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:516:1: ( ( (lv_returntype_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_body_8_0= ruleMethodBody ) ) otherlv_9= '}' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:516:1: ( ( (lv_returntype_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_body_8_0= ruleMethodBody ) ) otherlv_9= '}' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:516:2: ( (lv_returntype_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_body_8_0= ruleMethodBody ) ) otherlv_9= '}'
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:516:2: ( (lv_returntype_0_0= ruleType ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:517:1: (lv_returntype_0_0= ruleType )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:517:1: (lv_returntype_0_0= ruleType )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:518:3: lv_returntype_0_0= ruleType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMethodAccess().getReturntypeTypeParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleType_in_ruleMethod1087);
            lv_returntype_0_0=ruleType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getMethodRule());
              	        }
                     		set(
                     			current, 
                     			"returntype",
                      		lv_returntype_0_0, 
                      		"Type");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:534:2: ( (lv_name_1_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:535:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:535:1: (lv_name_1_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:536:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMethod1104); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getMethodAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getMethodRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleMethod1121); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getMethodAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:556:1: ( ( (lv_params_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )* )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID||(LA9_0>=11 && LA9_0<=13)) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:556:2: ( (lv_params_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )*
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:556:2: ( (lv_params_3_0= ruleParameter ) )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:557:1: (lv_params_3_0= ruleParameter )
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:557:1: (lv_params_3_0= ruleParameter )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:558:3: lv_params_3_0= ruleParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleParameter_in_ruleMethod1143);
                    lv_params_3_0=ruleParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMethodRule());
                      	        }
                             		add(
                             			current, 
                             			"params",
                              		lv_params_3_0, 
                              		"Parameter");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:574:2: (otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) ) )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==20) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:574:4: otherlv_4= ',' ( (lv_params_5_0= ruleParameter ) )
                    	    {
                    	    otherlv_4=(Token)match(input,20,FOLLOW_20_in_ruleMethod1156); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getMethodAccess().getCommaKeyword_3_1_0());
                    	          
                    	    }
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:578:1: ( (lv_params_5_0= ruleParameter ) )
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:579:1: (lv_params_5_0= ruleParameter )
                    	    {
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:579:1: (lv_params_5_0= ruleParameter )
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:580:3: lv_params_5_0= ruleParameter
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getMethodAccess().getParamsParameterParserRuleCall_3_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleParameter_in_ruleMethod1177);
                    	    lv_params_5_0=ruleParameter();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getMethodRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"params",
                    	              		lv_params_5_0, 
                    	              		"Parameter");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,21,FOLLOW_21_in_ruleMethod1193); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getMethodAccess().getRightParenthesisKeyword_4());
                  
            }
            otherlv_7=(Token)match(input,16,FOLLOW_16_in_ruleMethod1205); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getMethodAccess().getLeftCurlyBracketKeyword_5());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:604:1: ( (lv_body_8_0= ruleMethodBody ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:605:1: (lv_body_8_0= ruleMethodBody )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:605:1: (lv_body_8_0= ruleMethodBody )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:606:3: lv_body_8_0= ruleMethodBody
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMethodAccess().getBodyMethodBodyParserRuleCall_6_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleMethodBody_in_ruleMethod1226);
            lv_body_8_0=ruleMethodBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getMethodRule());
              	        }
                     		set(
                     			current, 
                     			"body",
                      		lv_body_8_0, 
                      		"MethodBody");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_9=(Token)match(input,17,FOLLOW_17_in_ruleMethod1238); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_9, grammarAccess.getMethodAccess().getRightCurlyBracketKeyword_7());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMethod"


    // $ANTLR start "entryRuleMethodBody"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:634:1: entryRuleMethodBody returns [EObject current=null] : iv_ruleMethodBody= ruleMethodBody EOF ;
    public final EObject entryRuleMethodBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMethodBody = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:635:2: (iv_ruleMethodBody= ruleMethodBody EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:636:2: iv_ruleMethodBody= ruleMethodBody EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMethodBodyRule()); 
            }
            pushFollow(FOLLOW_ruleMethodBody_in_entryRuleMethodBody1274);
            iv_ruleMethodBody=ruleMethodBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMethodBody; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMethodBody1284); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMethodBody"


    // $ANTLR start "ruleMethodBody"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:643:1: ruleMethodBody returns [EObject current=null] : (otherlv_0= 'return' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ';' ) ;
    public final EObject ruleMethodBody() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_expression_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:646:28: ( (otherlv_0= 'return' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ';' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:647:1: (otherlv_0= 'return' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ';' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:647:1: (otherlv_0= 'return' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ';' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:647:3: otherlv_0= 'return' ( (lv_expression_1_0= ruleExpression ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleMethodBody1321); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getMethodBodyAccess().getReturnKeyword_0());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:651:1: ( (lv_expression_1_0= ruleExpression ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:652:1: (lv_expression_1_0= ruleExpression )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:652:1: (lv_expression_1_0= ruleExpression )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:653:3: lv_expression_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMethodBodyAccess().getExpressionExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleMethodBody1342);
            lv_expression_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getMethodBodyRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_1_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleMethodBody1354); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getMethodBodyAccess().getSemicolonKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMethodBody"


    // $ANTLR start "entryRuleExpression"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:681:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:682:2: (iv_ruleExpression= ruleExpression EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:683:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression1390);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression1400); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:690:1: ruleExpression returns [EObject current=null] : (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( (lv_message_3_0= ruleMessage ) ) )* ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_TerminalExpression_0 = null;

        EObject lv_message_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:693:28: ( (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( (lv_message_3_0= ruleMessage ) ) )* ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:694:1: (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( (lv_message_3_0= ruleMessage ) ) )* )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:694:1: (this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( (lv_message_3_0= ruleMessage ) ) )* )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:695:2: this_TerminalExpression_0= ruleTerminalExpression ( () otherlv_2= '.' ( (lv_message_3_0= ruleMessage ) ) )*
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getTerminalExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleTerminalExpression_in_ruleExpression1450);
            this_TerminalExpression_0=ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_TerminalExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:706:1: ( () otherlv_2= '.' ( (lv_message_3_0= ruleMessage ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==23) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:706:2: () otherlv_2= '.' ( (lv_message_3_0= ruleMessage ) )
            	    {
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:706:2: ()
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:707:2: 
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	  /* */ 
            	      	
            	    }
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndSet(
            	                  grammarAccess.getExpressionAccess().getSelectionReceiverAction_1_0(),
            	                  current);
            	          
            	    }

            	    }

            	    otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleExpression1474); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getExpressionAccess().getFullStopKeyword_1_1());
            	          
            	    }
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:719:1: ( (lv_message_3_0= ruleMessage ) )
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:720:1: (lv_message_3_0= ruleMessage )
            	    {
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:720:1: (lv_message_3_0= ruleMessage )
            	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:721:3: lv_message_3_0= ruleMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getExpressionAccess().getMessageMessageParserRuleCall_1_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleMessage_in_ruleExpression1495);
            	    lv_message_3_0=ruleMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getExpressionRule());
            	      	        }
            	             		set(
            	             			current, 
            	             			"message",
            	              		lv_message_3_0, 
            	              		"Message");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleMessage"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:745:1: entryRuleMessage returns [EObject current=null] : iv_ruleMessage= ruleMessage EOF ;
    public final EObject entryRuleMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessage = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:746:2: (iv_ruleMessage= ruleMessage EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:747:2: iv_ruleMessage= ruleMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMessageRule()); 
            }
            pushFollow(FOLLOW_ruleMessage_in_entryRuleMessage1533);
            iv_ruleMessage=ruleMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMessage; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessage1543); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:754:1: ruleMessage returns [EObject current=null] : (this_MethodCall_0= ruleMethodCall | this_FieldSelection_1= ruleFieldSelection ) ;
    public final EObject ruleMessage() throws RecognitionException {
        EObject current = null;

        EObject this_MethodCall_0 = null;

        EObject this_FieldSelection_1 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:757:28: ( (this_MethodCall_0= ruleMethodCall | this_FieldSelection_1= ruleFieldSelection ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:758:1: (this_MethodCall_0= ruleMethodCall | this_FieldSelection_1= ruleFieldSelection )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:758:1: (this_MethodCall_0= ruleMethodCall | this_FieldSelection_1= ruleFieldSelection )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_ID) ) {
                int LA11_1 = input.LA(2);

                if ( (LA11_1==EOF||LA11_1==18||(LA11_1>=20 && LA11_1<=21)||LA11_1==23) ) {
                    alt11=2;
                }
                else if ( (LA11_1==19) ) {
                    alt11=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:759:2: this_MethodCall_0= ruleMethodCall
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getMessageAccess().getMethodCallParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleMethodCall_in_ruleMessage1593);
                    this_MethodCall_0=ruleMethodCall();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_MethodCall_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:772:2: this_FieldSelection_1= ruleFieldSelection
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getMessageAccess().getFieldSelectionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleFieldSelection_in_ruleMessage1623);
                    this_FieldSelection_1=ruleFieldSelection();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FieldSelection_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleMethodCall"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:791:1: entryRuleMethodCall returns [EObject current=null] : iv_ruleMethodCall= ruleMethodCall EOF ;
    public final EObject entryRuleMethodCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMethodCall = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:792:2: (iv_ruleMethodCall= ruleMethodCall EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:793:2: iv_ruleMethodCall= ruleMethodCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMethodCallRule()); 
            }
            pushFollow(FOLLOW_ruleMethodCall_in_entryRuleMethodCall1658);
            iv_ruleMethodCall=ruleMethodCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMethodCall; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMethodCall1668); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMethodCall"


    // $ANTLR start "ruleMethodCall"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:800:1: ruleMethodCall returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_args_2_0= ruleArgument ) ) (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )* )? otherlv_5= ')' ) ;
    public final EObject ruleMethodCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_args_2_0 = null;

        EObject lv_args_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:803:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_args_2_0= ruleArgument ) ) (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )* )? otherlv_5= ')' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:804:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_args_2_0= ruleArgument ) ) (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )* )? otherlv_5= ')' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:804:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_args_2_0= ruleArgument ) ) (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )* )? otherlv_5= ')' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:804:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_args_2_0= ruleArgument ) ) (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )* )? otherlv_5= ')'
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:804:2: ( (otherlv_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:805:1: (otherlv_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:805:1: (otherlv_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:806:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getMethodCallRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMethodCall1717); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getMethodCallAccess().getNameMethodCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,19,FOLLOW_19_in_ruleMethodCall1729); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getMethodCallAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:824:1: ( ( (lv_args_2_0= ruleArgument ) ) (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )* )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( ((LA13_0>=RULE_ID && LA13_0<=RULE_INT)||LA13_0==19||(LA13_0>=24 && LA13_0<=27)) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:824:2: ( (lv_args_2_0= ruleArgument ) ) (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )*
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:824:2: ( (lv_args_2_0= ruleArgument ) )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:825:1: (lv_args_2_0= ruleArgument )
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:825:1: (lv_args_2_0= ruleArgument )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:826:3: lv_args_2_0= ruleArgument
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleArgument_in_ruleMethodCall1751);
                    lv_args_2_0=ruleArgument();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMethodCallRule());
                      	        }
                             		add(
                             			current, 
                             			"args",
                              		lv_args_2_0, 
                              		"Argument");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:842:2: (otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==20) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:842:4: otherlv_3= ',' ( (lv_args_4_0= ruleArgument ) )
                    	    {
                    	    otherlv_3=(Token)match(input,20,FOLLOW_20_in_ruleMethodCall1764); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_3, grammarAccess.getMethodCallAccess().getCommaKeyword_2_1_0());
                    	          
                    	    }
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:846:1: ( (lv_args_4_0= ruleArgument ) )
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:847:1: (lv_args_4_0= ruleArgument )
                    	    {
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:847:1: (lv_args_4_0= ruleArgument )
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:848:3: lv_args_4_0= ruleArgument
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getMethodCallAccess().getArgsArgumentParserRuleCall_2_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleArgument_in_ruleMethodCall1785);
                    	    lv_args_4_0=ruleArgument();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getMethodCallRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"args",
                    	              		lv_args_4_0, 
                    	              		"Argument");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,21,FOLLOW_21_in_ruleMethodCall1801); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getMethodCallAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMethodCall"


    // $ANTLR start "entryRuleFieldSelection"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:876:1: entryRuleFieldSelection returns [EObject current=null] : iv_ruleFieldSelection= ruleFieldSelection EOF ;
    public final EObject entryRuleFieldSelection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFieldSelection = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:877:2: (iv_ruleFieldSelection= ruleFieldSelection EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:878:2: iv_ruleFieldSelection= ruleFieldSelection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFieldSelectionRule()); 
            }
            pushFollow(FOLLOW_ruleFieldSelection_in_entryRuleFieldSelection1837);
            iv_ruleFieldSelection=ruleFieldSelection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFieldSelection; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFieldSelection1847); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFieldSelection"


    // $ANTLR start "ruleFieldSelection"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:885:1: ruleFieldSelection returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleFieldSelection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:888:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:889:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:889:1: ( (otherlv_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:890:1: (otherlv_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:890:1: (otherlv_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:891:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFieldSelectionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFieldSelection1895); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getFieldSelectionAccess().getNameFieldCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFieldSelection"


    // $ANTLR start "entryRuleTerminalExpression"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:913:1: entryRuleTerminalExpression returns [EObject current=null] : iv_ruleTerminalExpression= ruleTerminalExpression EOF ;
    public final EObject entryRuleTerminalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerminalExpression = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:914:2: (iv_ruleTerminalExpression= ruleTerminalExpression EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:915:2: iv_ruleTerminalExpression= ruleTerminalExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTerminalExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleTerminalExpression_in_entryRuleTerminalExpression1930);
            iv_ruleTerminalExpression=ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTerminalExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTerminalExpression1940); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerminalExpression"


    // $ANTLR start "ruleTerminalExpression"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:922:1: ruleTerminalExpression returns [EObject current=null] : (this_This_0= ruleThis | this_Variable_1= ruleVariable | this_New_2= ruleNew | this_Cast_3= ruleCast | this_Constant_4= ruleConstant | this_Paren_5= ruleParen ) ;
    public final EObject ruleTerminalExpression() throws RecognitionException {
        EObject current = null;

        EObject this_This_0 = null;

        EObject this_Variable_1 = null;

        EObject this_New_2 = null;

        EObject this_Cast_3 = null;

        EObject this_Constant_4 = null;

        EObject this_Paren_5 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:925:28: ( (this_This_0= ruleThis | this_Variable_1= ruleVariable | this_New_2= ruleNew | this_Cast_3= ruleCast | this_Constant_4= ruleConstant | this_Paren_5= ruleParen ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:926:1: (this_This_0= ruleThis | this_Variable_1= ruleVariable | this_New_2= ruleNew | this_Cast_3= ruleCast | this_Constant_4= ruleConstant | this_Paren_5= ruleParen )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:926:1: (this_This_0= ruleThis | this_Variable_1= ruleVariable | this_New_2= ruleNew | this_Cast_3= ruleCast | this_Constant_4= ruleConstant | this_Paren_5= ruleParen )
            int alt14=6;
            alt14 = dfa14.predict(input);
            switch (alt14) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:927:2: this_This_0= ruleThis
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTerminalExpressionAccess().getThisParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleThis_in_ruleTerminalExpression1990);
                    this_This_0=ruleThis();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_This_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:940:2: this_Variable_1= ruleVariable
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTerminalExpressionAccess().getVariableParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleVariable_in_ruleTerminalExpression2020);
                    this_Variable_1=ruleVariable();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Variable_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:953:2: this_New_2= ruleNew
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTerminalExpressionAccess().getNewParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNew_in_ruleTerminalExpression2050);
                    this_New_2=ruleNew();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_New_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:966:2: this_Cast_3= ruleCast
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTerminalExpressionAccess().getCastParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleCast_in_ruleTerminalExpression2080);
                    this_Cast_3=ruleCast();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Cast_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:979:2: this_Constant_4= ruleConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTerminalExpressionAccess().getConstantParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleConstant_in_ruleTerminalExpression2110);
                    this_Constant_4=ruleConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Constant_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:992:2: this_Paren_5= ruleParen
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTerminalExpressionAccess().getParenParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleParen_in_ruleTerminalExpression2140);
                    this_Paren_5=ruleParen();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Paren_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerminalExpression"


    // $ANTLR start "entryRuleThis"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1011:1: entryRuleThis returns [EObject current=null] : iv_ruleThis= ruleThis EOF ;
    public final EObject entryRuleThis() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleThis = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1012:2: (iv_ruleThis= ruleThis EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1013:2: iv_ruleThis= ruleThis EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getThisRule()); 
            }
            pushFollow(FOLLOW_ruleThis_in_entryRuleThis2175);
            iv_ruleThis=ruleThis();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleThis; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleThis2185); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleThis"


    // $ANTLR start "ruleThis"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1020:1: ruleThis returns [EObject current=null] : ( (lv_variable_0_0= 'this' ) ) ;
    public final EObject ruleThis() throws RecognitionException {
        EObject current = null;

        Token lv_variable_0_0=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1023:28: ( ( (lv_variable_0_0= 'this' ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1024:1: ( (lv_variable_0_0= 'this' ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1024:1: ( (lv_variable_0_0= 'this' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1025:1: (lv_variable_0_0= 'this' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1025:1: (lv_variable_0_0= 'this' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1026:3: lv_variable_0_0= 'this'
            {
            lv_variable_0_0=(Token)match(input,24,FOLLOW_24_in_ruleThis2227); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_variable_0_0, grammarAccess.getThisAccess().getVariableThisKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getThisRule());
              	        }
                     		setWithLastConsumed(current, "variable", lv_variable_0_0, "this");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleThis"


    // $ANTLR start "entryRuleVariable"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1047:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1048:2: (iv_ruleVariable= ruleVariable EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1049:2: iv_ruleVariable= ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_ruleVariable_in_entryRuleVariable2275);
            iv_ruleVariable=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariable; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariable2285); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1056:1: ruleVariable returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1059:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1060:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1060:1: ( (otherlv_0= RULE_ID ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1061:1: (otherlv_0= RULE_ID )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1061:1: (otherlv_0= RULE_ID )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1062:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVariable2333); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableAccess().getParamrefParameterCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleNew"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1084:1: entryRuleNew returns [EObject current=null] : iv_ruleNew= ruleNew EOF ;
    public final EObject entryRuleNew() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNew = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1085:2: (iv_ruleNew= ruleNew EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1086:2: iv_ruleNew= ruleNew EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNewRule()); 
            }
            pushFollow(FOLLOW_ruleNew_in_entryRuleNew2368);
            iv_ruleNew=ruleNew();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNew; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNew2378); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNew"


    // $ANTLR start "ruleNew"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1093:1: ruleNew returns [EObject current=null] : (otherlv_0= 'new' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleArgument ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )* )? otherlv_6= ')' ) ;
    public final EObject ruleNew() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_type_1_0 = null;

        EObject lv_args_3_0 = null;

        EObject lv_args_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1096:28: ( (otherlv_0= 'new' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleArgument ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )* )? otherlv_6= ')' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1097:1: (otherlv_0= 'new' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleArgument ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )* )? otherlv_6= ')' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1097:1: (otherlv_0= 'new' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleArgument ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )* )? otherlv_6= ')' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1097:3: otherlv_0= 'new' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= '(' ( ( (lv_args_3_0= ruleArgument ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )* )? otherlv_6= ')'
            {
            otherlv_0=(Token)match(input,25,FOLLOW_25_in_ruleNew2415); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNewAccess().getNewKeyword_0());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1101:1: ( (lv_type_1_0= ruleClassType ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1102:1: (lv_type_1_0= ruleClassType )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1102:1: (lv_type_1_0= ruleClassType )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1103:3: lv_type_1_0= ruleClassType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNewAccess().getTypeClassTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleClassType_in_ruleNew2436);
            lv_type_1_0=ruleClassType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNewRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_1_0, 
                      		"ClassType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleNew2448); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getNewAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1123:1: ( ( (lv_args_3_0= ruleArgument ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )* )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=RULE_ID && LA16_0<=RULE_INT)||LA16_0==19||(LA16_0>=24 && LA16_0<=27)) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1123:2: ( (lv_args_3_0= ruleArgument ) ) (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )*
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1123:2: ( (lv_args_3_0= ruleArgument ) )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1124:1: (lv_args_3_0= ruleArgument )
                    {
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1124:1: (lv_args_3_0= ruleArgument )
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1125:3: lv_args_3_0= ruleArgument
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleArgument_in_ruleNew2470);
                    lv_args_3_0=ruleArgument();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNewRule());
                      	        }
                             		add(
                             			current, 
                             			"args",
                              		lv_args_3_0, 
                              		"Argument");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1141:2: (otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==20) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1141:4: otherlv_4= ',' ( (lv_args_5_0= ruleArgument ) )
                    	    {
                    	    otherlv_4=(Token)match(input,20,FOLLOW_20_in_ruleNew2483); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getNewAccess().getCommaKeyword_3_1_0());
                    	          
                    	    }
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1145:1: ( (lv_args_5_0= ruleArgument ) )
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1146:1: (lv_args_5_0= ruleArgument )
                    	    {
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1146:1: (lv_args_5_0= ruleArgument )
                    	    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1147:3: lv_args_5_0= ruleArgument
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getNewAccess().getArgsArgumentParserRuleCall_3_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleArgument_in_ruleNew2504);
                    	    lv_args_5_0=ruleArgument();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getNewRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"args",
                    	              		lv_args_5_0, 
                    	              		"Argument");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,21,FOLLOW_21_in_ruleNew2520); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getNewAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNew"


    // $ANTLR start "entryRuleCast"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1175:1: entryRuleCast returns [EObject current=null] : iv_ruleCast= ruleCast EOF ;
    public final EObject entryRuleCast() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCast = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1176:2: (iv_ruleCast= ruleCast EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1177:2: iv_ruleCast= ruleCast EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCastRule()); 
            }
            pushFollow(FOLLOW_ruleCast_in_entryRuleCast2556);
            iv_ruleCast=ruleCast();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCast; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCast2566); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCast"


    // $ANTLR start "ruleCast"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1184:1: ruleCast returns [EObject current=null] : (otherlv_0= '(' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= ')' ( (lv_object_3_0= ruleTerminalExpression ) ) ) ;
    public final EObject ruleCast() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_type_1_0 = null;

        EObject lv_object_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1187:28: ( (otherlv_0= '(' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= ')' ( (lv_object_3_0= ruleTerminalExpression ) ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1188:1: (otherlv_0= '(' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= ')' ( (lv_object_3_0= ruleTerminalExpression ) ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1188:1: (otherlv_0= '(' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= ')' ( (lv_object_3_0= ruleTerminalExpression ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1188:3: otherlv_0= '(' ( (lv_type_1_0= ruleClassType ) ) otherlv_2= ')' ( (lv_object_3_0= ruleTerminalExpression ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleCast2603); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCastAccess().getLeftParenthesisKeyword_0());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1192:1: ( (lv_type_1_0= ruleClassType ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1193:1: (lv_type_1_0= ruleClassType )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1193:1: (lv_type_1_0= ruleClassType )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1194:3: lv_type_1_0= ruleClassType
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCastAccess().getTypeClassTypeParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleClassType_in_ruleCast2624);
            lv_type_1_0=ruleClassType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCastRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_1_0, 
                      		"ClassType");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleCast2636); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getCastAccess().getRightParenthesisKeyword_2());
                  
            }
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1214:1: ( (lv_object_3_0= ruleTerminalExpression ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1215:1: (lv_object_3_0= ruleTerminalExpression )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1215:1: (lv_object_3_0= ruleTerminalExpression )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1216:3: lv_object_3_0= ruleTerminalExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCastAccess().getObjectTerminalExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleTerminalExpression_in_ruleCast2657);
            lv_object_3_0=ruleTerminalExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCastRule());
              	        }
                     		set(
                     			current, 
                     			"object",
                      		lv_object_3_0, 
                      		"TerminalExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCast"


    // $ANTLR start "entryRuleParen"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1240:1: entryRuleParen returns [EObject current=null] : iv_ruleParen= ruleParen EOF ;
    public final EObject entryRuleParen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParen = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1241:2: (iv_ruleParen= ruleParen EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1242:2: iv_ruleParen= ruleParen EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParenRule()); 
            }
            pushFollow(FOLLOW_ruleParen_in_entryRuleParen2693);
            iv_ruleParen=ruleParen();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParen; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleParen2703); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParen"


    // $ANTLR start "ruleParen"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1249:1: ruleParen returns [EObject current=null] : (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' ) ;
    public final EObject ruleParen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject this_Expression_1 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1252:28: ( (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1253:1: (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1253:1: (otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')' )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1253:3: otherlv_0= '(' this_Expression_1= ruleExpression otherlv_2= ')'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleParen2740); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getParenAccess().getLeftParenthesisKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getParenAccess().getExpressionParserRuleCall_1()); 
                  
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleParen2765);
            this_Expression_1=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_Expression_1; 
                      afterParserOrEnumRuleCall();
                  
            }
            otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleParen2776); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getParenAccess().getRightParenthesisKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParen"


    // $ANTLR start "entryRuleConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1281:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1282:2: (iv_ruleConstant= ruleConstant EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1283:2: iv_ruleConstant= ruleConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstantRule()); 
            }
            pushFollow(FOLLOW_ruleConstant_in_entryRuleConstant2812);
            iv_ruleConstant=ruleConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstant2822); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1290:1: ruleConstant returns [EObject current=null] : (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_StringConstant_2= ruleStringConstant ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        EObject this_IntConstant_0 = null;

        EObject this_BoolConstant_1 = null;

        EObject this_StringConstant_2 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1293:28: ( (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_StringConstant_2= ruleStringConstant ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1294:1: (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_StringConstant_2= ruleStringConstant )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1294:1: (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_StringConstant_2= ruleStringConstant )
            int alt17=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt17=1;
                }
                break;
            case 26:
            case 27:
                {
                alt17=2;
                }
                break;
            case RULE_STRING:
                {
                alt17=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1295:2: this_IntConstant_0= ruleIntConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConstantAccess().getIntConstantParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntConstant_in_ruleConstant2872);
                    this_IntConstant_0=ruleIntConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntConstant_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1308:2: this_BoolConstant_1= ruleBoolConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConstantAccess().getBoolConstantParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBoolConstant_in_ruleConstant2902);
                    this_BoolConstant_1=ruleBoolConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BoolConstant_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1321:2: this_StringConstant_2= ruleStringConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConstantAccess().getStringConstantParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStringConstant_in_ruleConstant2932);
                    this_StringConstant_2=ruleStringConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringConstant_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleStringConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1340:1: entryRuleStringConstant returns [EObject current=null] : iv_ruleStringConstant= ruleStringConstant EOF ;
    public final EObject entryRuleStringConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringConstant = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1341:2: (iv_ruleStringConstant= ruleStringConstant EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1342:2: iv_ruleStringConstant= ruleStringConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringConstantRule()); 
            }
            pushFollow(FOLLOW_ruleStringConstant_in_entryRuleStringConstant2967);
            iv_ruleStringConstant=ruleStringConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStringConstant2977); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringConstant"


    // $ANTLR start "ruleStringConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1349:1: ruleStringConstant returns [EObject current=null] : ( (lv_constant_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringConstant() throws RecognitionException {
        EObject current = null;

        Token lv_constant_0_0=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1352:28: ( ( (lv_constant_0_0= RULE_STRING ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1353:1: ( (lv_constant_0_0= RULE_STRING ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1353:1: ( (lv_constant_0_0= RULE_STRING ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1354:1: (lv_constant_0_0= RULE_STRING )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1354:1: (lv_constant_0_0= RULE_STRING )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1355:3: lv_constant_0_0= RULE_STRING
            {
            lv_constant_0_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleStringConstant3018); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_constant_0_0, grammarAccess.getStringConstantAccess().getConstantSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringConstantRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"constant",
                      		lv_constant_0_0, 
                      		"STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringConstant"


    // $ANTLR start "entryRuleIntConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1379:1: entryRuleIntConstant returns [EObject current=null] : iv_ruleIntConstant= ruleIntConstant EOF ;
    public final EObject entryRuleIntConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntConstant = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1380:2: (iv_ruleIntConstant= ruleIntConstant EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1381:2: iv_ruleIntConstant= ruleIntConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntConstantRule()); 
            }
            pushFollow(FOLLOW_ruleIntConstant_in_entryRuleIntConstant3058);
            iv_ruleIntConstant=ruleIntConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntConstant3068); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntConstant"


    // $ANTLR start "ruleIntConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1388:1: ruleIntConstant returns [EObject current=null] : ( (lv_constant_0_0= RULE_INT ) ) ;
    public final EObject ruleIntConstant() throws RecognitionException {
        EObject current = null;

        Token lv_constant_0_0=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1391:28: ( ( (lv_constant_0_0= RULE_INT ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1392:1: ( (lv_constant_0_0= RULE_INT ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1392:1: ( (lv_constant_0_0= RULE_INT ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1393:1: (lv_constant_0_0= RULE_INT )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1393:1: (lv_constant_0_0= RULE_INT )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1394:3: lv_constant_0_0= RULE_INT
            {
            lv_constant_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntConstant3109); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_constant_0_0, grammarAccess.getIntConstantAccess().getConstantINTTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntConstantRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"constant",
                      		lv_constant_0_0, 
                      		"INT");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntConstant"


    // $ANTLR start "entryRuleBoolConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1418:1: entryRuleBoolConstant returns [EObject current=null] : iv_ruleBoolConstant= ruleBoolConstant EOF ;
    public final EObject entryRuleBoolConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolConstant = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1419:2: (iv_ruleBoolConstant= ruleBoolConstant EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1420:2: iv_ruleBoolConstant= ruleBoolConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBoolConstantRule()); 
            }
            pushFollow(FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant3149);
            iv_ruleBoolConstant=ruleBoolConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBoolConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBoolConstant3159); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolConstant"


    // $ANTLR start "ruleBoolConstant"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1427:1: ruleBoolConstant returns [EObject current=null] : ( ( (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' ) ) ) ;
    public final EObject ruleBoolConstant() throws RecognitionException {
        EObject current = null;

        Token lv_constant_0_1=null;
        Token lv_constant_0_2=null;

         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1430:28: ( ( ( (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' ) ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1431:1: ( ( (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' ) ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1431:1: ( ( (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' ) ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1432:1: ( (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' ) )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1432:1: ( (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' ) )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1433:1: (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' )
            {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1433:1: (lv_constant_0_1= 'true' | lv_constant_0_2= 'false' )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==26) ) {
                alt18=1;
            }
            else if ( (LA18_0==27) ) {
                alt18=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1434:3: lv_constant_0_1= 'true'
                    {
                    lv_constant_0_1=(Token)match(input,26,FOLLOW_26_in_ruleBoolConstant3203); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_constant_0_1, grammarAccess.getBoolConstantAccess().getConstantTrueKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getBoolConstantRule());
                      	        }
                             		setWithLastConsumed(current, "constant", lv_constant_0_1, null);
                      	    
                    }

                    }
                    break;
                case 2 :
                    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1446:8: lv_constant_0_2= 'false'
                    {
                    lv_constant_0_2=(Token)match(input,27,FOLLOW_27_in_ruleBoolConstant3232); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_constant_0_2, grammarAccess.getBoolConstantAccess().getConstantFalseKeyword_0_1());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getBoolConstantRule());
                      	        }
                             		setWithLastConsumed(current, "constant", lv_constant_0_2, null);
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolConstant"


    // $ANTLR start "entryRuleArgument"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1469:1: entryRuleArgument returns [EObject current=null] : iv_ruleArgument= ruleArgument EOF ;
    public final EObject entryRuleArgument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArgument = null;


        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1470:2: (iv_ruleArgument= ruleArgument EOF )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1471:2: iv_ruleArgument= ruleArgument EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArgumentRule()); 
            }
            pushFollow(FOLLOW_ruleArgument_in_entryRuleArgument3283);
            iv_ruleArgument=ruleArgument();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArgument; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleArgument3293); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArgument"


    // $ANTLR start "ruleArgument"
    // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1478:1: ruleArgument returns [EObject current=null] : this_Expression_0= ruleExpression ;
    public final EObject ruleArgument() throws RecognitionException {
        EObject current = null;

        EObject this_Expression_0 = null;


         enterRule(); 
            
        try {
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1481:28: (this_Expression_0= ruleExpression )
            // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:1483:2: this_Expression_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getArgumentAccess().getExpressionParserRuleCall()); 
                  
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleArgument3342);
            this_Expression_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_Expression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArgument"

    // $ANTLR start synpred18_InternalFJ
    public final void synpred18_InternalFJ_fragment() throws RecognitionException {   
        EObject this_Cast_3 = null;


        // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:966:2: (this_Cast_3= ruleCast )
        // ../org.eclipse.xtext.example.fj/src-gen/org/eclipse/xtext/example/parser/antlr/internal/InternalFJ.g:966:2: this_Cast_3= ruleCast
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }
        pushFollow(FOLLOW_ruleCast_in_synpred18_InternalFJ2080);
        this_Cast_3=ruleCast();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred18_InternalFJ

    // Delegated rules

    public final boolean synpred18_InternalFJ() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred18_InternalFJ_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA14_eotS =
        "\13\uffff";
    static final String DFA14_eofS =
        "\13\uffff";
    static final String DFA14_minS =
        "\1\4\3\uffff\1\0\6\uffff";
    static final String DFA14_maxS =
        "\1\33\3\uffff\1\0\6\uffff";
    static final String DFA14_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\uffff\1\5\3\uffff\1\4\1\6";
    static final String DFA14_specialS =
        "\4\uffff\1\0\6\uffff}>";
    static final String[] DFA14_transitionS = {
            "\1\2\2\5\14\uffff\1\4\4\uffff\1\1\1\3\2\5",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "926:1: (this_This_0= ruleThis | this_Variable_1= ruleVariable | this_New_2= ruleNew | this_Cast_3= ruleCast | this_Constant_4= ruleConstant | this_Paren_5= ruleParen )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_4 = input.LA(1);

                         
                        int index14_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred18_InternalFJ()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index14_4);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleProgram_in_entryRuleProgram81 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProgram91 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_ruleProgram137 = new BitSet(new long[]{0x000000000F084072L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleProgram159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType196 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType206 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicType_in_ruleType256 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClassType_in_ruleType286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicType_in_entryRuleBasicType321 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBasicType331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleBasicType375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleBasicType404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_ruleBasicType433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClassType_in_entryRuleClassType484 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClassType494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleClassType542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClass_in_entryRuleClass579 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClass589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleClass626 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleClass643 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_15_in_ruleClass661 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleClass685 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleClass699 = new BitSet(new long[]{0x0000000000023810L});
    public static final BitSet FOLLOW_ruleField_in_ruleClass720 = new BitSet(new long[]{0x0000000000023810L});
    public static final BitSet FOLLOW_ruleMethod_in_ruleClass742 = new BitSet(new long[]{0x0000000000023810L});
    public static final BitSet FOLLOW_17_in_ruleClass755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleField_in_entryRuleField791 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleField801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_ruleField847 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleField864 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleField881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParameter_in_entryRuleParameter917 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParameter927 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_ruleParameter973 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleParameter990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethod_in_entryRuleMethod1031 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMethod1041 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_ruleMethod1087 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMethod1104 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleMethod1121 = new BitSet(new long[]{0x0000000000203810L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleMethod1143 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_20_in_ruleMethod1156 = new BitSet(new long[]{0x0000000000003810L});
    public static final BitSet FOLLOW_ruleParameter_in_ruleMethod1177 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_21_in_ruleMethod1193 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleMethod1205 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_ruleMethodBody_in_ruleMethod1226 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleMethod1238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethodBody_in_entryRuleMethodBody1274 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMethodBody1284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleMethodBody1321 = new BitSet(new long[]{0x000000000F080070L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleMethodBody1342 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleMethodBody1354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression1390 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression1400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerminalExpression_in_ruleExpression1450 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_23_in_ruleExpression1474 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleMessage_in_ruleExpression1495 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage1533 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessage1543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethodCall_in_ruleMessage1593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFieldSelection_in_ruleMessage1623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMethodCall_in_entryRuleMethodCall1658 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMethodCall1668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMethodCall1717 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleMethodCall1729 = new BitSet(new long[]{0x000000000F280070L});
    public static final BitSet FOLLOW_ruleArgument_in_ruleMethodCall1751 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_20_in_ruleMethodCall1764 = new BitSet(new long[]{0x000000000F080070L});
    public static final BitSet FOLLOW_ruleArgument_in_ruleMethodCall1785 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_21_in_ruleMethodCall1801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFieldSelection_in_entryRuleFieldSelection1837 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFieldSelection1847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFieldSelection1895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerminalExpression_in_entryRuleTerminalExpression1930 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTerminalExpression1940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleThis_in_ruleTerminalExpression1990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleTerminalExpression2020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNew_in_ruleTerminalExpression2050 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCast_in_ruleTerminalExpression2080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleTerminalExpression2110 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParen_in_ruleTerminalExpression2140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleThis_in_entryRuleThis2175 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleThis2185 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleThis2227 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_entryRuleVariable2275 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariable2285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVariable2333 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNew_in_entryRuleNew2368 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNew2378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleNew2415 = new BitSet(new long[]{0x0000000000003810L});
    public static final BitSet FOLLOW_ruleClassType_in_ruleNew2436 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleNew2448 = new BitSet(new long[]{0x000000000F280070L});
    public static final BitSet FOLLOW_ruleArgument_in_ruleNew2470 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_20_in_ruleNew2483 = new BitSet(new long[]{0x000000000F080070L});
    public static final BitSet FOLLOW_ruleArgument_in_ruleNew2504 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_21_in_ruleNew2520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCast_in_entryRuleCast2556 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCast2566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleCast2603 = new BitSet(new long[]{0x0000000000003810L});
    public static final BitSet FOLLOW_ruleClassType_in_ruleCast2624 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleCast2636 = new BitSet(new long[]{0x000000000F080070L});
    public static final BitSet FOLLOW_ruleTerminalExpression_in_ruleCast2657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleParen_in_entryRuleParen2693 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleParen2703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleParen2740 = new BitSet(new long[]{0x000000000F080070L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleParen2765 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleParen2776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant2812 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstant2822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntConstant_in_ruleConstant2872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolConstant_in_ruleConstant2902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringConstant_in_ruleConstant2932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStringConstant_in_entryRuleStringConstant2967 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStringConstant2977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleStringConstant3018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntConstant_in_entryRuleIntConstant3058 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntConstant3068 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntConstant3109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant3149 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBoolConstant3159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleBoolConstant3203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleBoolConstant3232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArgument_in_entryRuleArgument3283 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArgument3293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleArgument3342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCast_in_synpred18_InternalFJ2080 = new BitSet(new long[]{0x0000000000000002L});

}