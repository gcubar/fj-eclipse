
package org.eclipse.xtext.example;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class FJStandaloneSetup extends FJStandaloneSetupGenerated{

	public static void doSetup() {
		new FJStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

