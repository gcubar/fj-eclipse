/**
 * 
 */
package org.eclipse.xtext.example.linker;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.Program;
import org.eclipse.xtext.example.lookup.AuxiliaryFunctions;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.linking.lazy.LazyLinkingResource;

/**
 * Customized resource which automatically adds in the resource set
 * the resource with the implicit class Object
 * 
 * @author bettini
 * 
 */
public class FJLinkingResource extends LazyLinkingResource {
	/**
	 * The implicit object element
	 */
	private Class Object = null;

	AuxiliaryFunctions auxiliaryFunctions = new AuxiliaryFunctions();
	
	/**
	 * The uri of the implicit resource containing the implicit object 
	 */
	public static final URI implicitObjectUri = URI.createURI("http:///Object.fj");

	@Override
	protected void doLinking() {
		ensureObjectIsPresent();
		super.doLinking();
	}

	/**
	 * Ensures that in the resource set there is the implicit Object resource
	 */
	private void ensureObjectIsPresent() {
		if (Object != null) {
			return;
		}
		
		// retrieve the implicit Object resource
		ResourceSet resourceSet = getResourceSet();
		Resource res = resourceSet.getResource(implicitObjectUri, true);
		if (res != null) {
			// store the implicit object locally for later use
			setObjectClass(((Program) res.getContents().get(0)).getClasses().get(0));
			System.out.println("Using implicit resource Object class");
			return;
		}
	}
	
	/**
	 * Utility method returning the implicit Object in the resource
	 * set of the specified element
	 * 
	 * @param eObject
	 * @return
	 */
	public static Class getObjectClass(EObject eObject) {
		return ((FJLinkingResource)eObject.eResource()).getObjectClass();
	}

	/**
	 * @return the implicit Object (creates it on the first invocation)
	 */
	public Class getObjectClass() {
		if (Object == null) {
			Object = FjFactory.eINSTANCE.createClass();
			Object.setName("Object");
		}

		return Object;
	}
	
	public void setObjectClass(Class objectClass) {
		Object = objectClass;
	}

	@Override
	public void doSave(OutputStream outputStream, Map<?, ?> options)
			throws IOException {
		// don't save the implicit resource
		if (getURI().equals(implicitObjectUri))
			return;
		
		super.doSave(outputStream, options);
	}
}
