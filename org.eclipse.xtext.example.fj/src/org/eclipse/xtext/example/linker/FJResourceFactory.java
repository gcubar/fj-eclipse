/**
 * 
 */
package org.eclipse.xtext.example.linker;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.FjFactory;
import org.eclipse.xtext.example.fj.Program;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Customized ResourceFactory which can return a resource with the implicit
 * Object.
 * 
 * @author bettini
 * 
 */
public class FJResourceFactory extends XtextResourceFactory {

	@Inject
	public FJResourceFactory(Provider<XtextResource> resourceProvider) {
		super(resourceProvider);
	}

	/**
	 * If asked for the implicit Object URI, it synthesizes such a resource
	 * 
	 * @see
	 * org.eclipse.xtext.resource.XtextResourceFactory#createResource(org.eclipse
	 * .emf.common.util.URI)
	 */
	@Override
	public Resource createResource(URI uri) {
		if (uri.equals(FJLinkingResource.implicitObjectUri)) {
			FJLinkingResource resource = new FJLinkingResource();
			resource.getContents().add(createObjectClassProgram());
			resource.setURI(uri);
			return resource;
		}

		return super.createResource(uri);
	}

	/**
	 * Creates a program consisting of the single implicit class Object
	 * @return program consisting of the single implicit class Object
	 */
	Program createObjectClassProgram() {
		Program program = FjFactory.eINSTANCE.createProgram();
		Class Object = FjFactory.eINSTANCE.createClass();
		Object.setName("Object");
		program.getClasses().add(Object);

		return program;
	}
}
