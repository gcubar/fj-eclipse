/**
 * 
 */
package org.eclipse.xtext.example.typing;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Type;
import org.eclipse.xtext.example.lookup.AuxiliaryFunctions;
import org.eclipse.xtext.example.util.TypeUtils;

/**
 * Subtyping rules.
 * 
 * @author bettini
 * 
 */
public class FJSubtyping {
	AuxiliaryFunctions auxiliaryFunctions = new AuxiliaryFunctions();

	public boolean isSubtype(Type t1, Type t2) {
		Class c1 = TypeUtils.getClassref(t1);
		Class c2 = TypeUtils.getClassref(t2);

		if (c1 != null && c2 != null) {
			return isSubtype(c1, c2);
		} else if (c1 == null && c2 == null) {
			// basic types
			return TypeUtils.getBasicTypeString(t1).equals(
					TypeUtils.getBasicTypeString(t2));
		}

		// all other cases are false
		return false;
	}

	public boolean isSubtype(Class c1, Class c2) {
		return (c1 != null && c2 != null)
				&& (c1.getName().equals(c2.getName()) || auxiliaryFunctions
						.getSuperclasses(c1).contains(c2));
	}
}
