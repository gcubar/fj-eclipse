/**
 * 
 */
package org.eclipse.xtext.example.util;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.FjFactory;

/**
 * Simple utility methods for creating classes with a name
 * and possible superclass (for testing purposes)
 * 
 * @author bettini
 * 
 */
public class ClassFactory {
	public static Class createClass(String name, Class superClass) {
		Class c = FjFactory.eINSTANCE.createClass();
		c.setName(name);
		c.setExtends(superClass);
		return c;
	}
	
	public static Class createClass(String name) {
		return createClass(name, null);
	}
}
