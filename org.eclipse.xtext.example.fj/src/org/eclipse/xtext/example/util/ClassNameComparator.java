/**
 * 
 */
package org.eclipse.xtext.example.util;

import java.util.Comparator;

import org.eclipse.xtext.example.fj.Class;

/**
 * Compares two classes using their names
 * 
 * @author bettini
 *
 */
public class ClassNameComparator implements Comparator<org.eclipse.xtext.example.fj.Class> {

	@Override
	public int compare(Class arg0, Class arg1) {
		return arg0.getName().compareTo(arg1.getName());
	}
	
}
