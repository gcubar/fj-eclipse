/**
 * 
 */
package org.eclipse.xtext.example.util;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Expression;
import org.eclipse.xtext.example.fj.Method;
import org.eclipse.xtext.example.fj.MethodBody;
import org.eclipse.xtext.example.fj.util.FjSwitch;

/**
 * Searches for the class containing an Expression
 * 
 * @author bettini
 * 
 */
public class ContainingClassFinder extends FjSwitch<Class> {

	@Override
	public Class caseExpression(Expression object) {
		return doSwitch(object.eContainer());
	}

	@Override
	public Class caseMethod(Method object) {
		return doSwitch(object.eContainer());
	}

	@Override
	public Class caseMethodBody(MethodBody object) {
		return doSwitch(object.eContainer());
	}

	@Override
	public Class defaultCase(EObject object) {
		if (object.eContainer() != null)
			return doSwitch(object.eContainer());

		return super.defaultCase(object);
	}

	@Override
	public Class caseClass(Class object) {
		return object;
	}

	public Class getContainingClass(EObject eObject) {
		return doSwitch(eObject);
	}
}
