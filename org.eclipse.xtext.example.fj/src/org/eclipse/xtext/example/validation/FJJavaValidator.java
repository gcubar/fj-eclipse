package org.eclipse.xtext.example.validation;

import org.eclipse.xtext.example.fj.Cast;
import org.eclipse.xtext.example.fj.Class;
import org.eclipse.xtext.example.fj.Field;
import org.eclipse.xtext.example.fj.FjPackage;
import org.eclipse.xtext.example.fj.Method;
import org.eclipse.xtext.example.fj.New;
import org.eclipse.xtext.example.fj.Selection;
import org.eclipse.xtext.example.typing.FJTypeChecker;

import org.eclipse.xtext.validation.Check;

public class FJJavaValidator extends AbstractFJJavaValidator {
	FJTypeChecker typeChecker = new FJTypeChecker();

	public static final String INVALID_CLASS_NAME = "org.eclipse.xtext.fj.InvalidTypeName";
	@Check
	public void checkClassNameStartsWithCapital(Class type) {
		if (!Character.isUpperCase(type.getName().charAt(0))) {
			warning("Class name should start with a capital",
					FjPackage.Literals.CLASS__NAME,
					INVALID_CLASS_NAME,
					type.getName());
		}
	}

	/**
	 * Checks that there are no cycles in the hierarchy. In the editor the error
	 * will show up only in cases 'class A extends A', while for recursive cases
	 * the linker itself will report errors since it cannot locate a class
	 * uniquely.
	 * 
	 * @param type
	 */
	@Check
	public void checkAcyclicHierarchy(Class type) {
		String errors = typeChecker.typeCheck(type);

		if (errors.length() > 0) {
			error("Cycle in the class hierarchy", FjPackage.Literals.CLASS__NAME);
			return;
		}
	}

	public static final String DUPLICATE_CLASS_NAMES = "org.eclipse.xtext.fj.DuplicateClassNames";
	/**
	 * Checks that there are no duplicate classes (classes with the same name)
	 * in a program uniquely.
	 * 
	 * @param type
	 */
	@Check
	public void checkNoDuplicateClasses(Class type) {
		String errors = typeChecker.typeCheck(type.eContainer());

		if (errors.length() > 0
				&& errors.contains("class " + type.getName() + " is already")) {
			error(errors, FjPackage.Literals.CLASS__NAME, DUPLICATE_CLASS_NAMES, type.getName());
			return;
		}
	}

	/**
	 * Checks that there are no duplicate fields in a class hierarchy
	 * 
	 * @param type
	 */
	@Check
	public void checkField(Field field) {
		String errors = typeChecker.typeCheck(field);

		if (errors.length() > 0) {
			error(errors, FjPackage.Literals.TYPED_ELEMENT__NAME);
			return;
		}
	}

	/**
	 * Checks the body of the method (if the body is consistent with its
	 * signature)
	 * 
	 * @param method
	 */
	@Check
	public void checkMethodOk(Method method) {
		String errors = typeChecker.typeCheck(method);

		if (errors != null && errors.length() > 0) {
			error(errors, FjPackage.Literals.METHOD__BODY);
			return;
		}
	}

	/**
	 * Checks whether a method selection is correct
	 * 
	 * @param method
	 */
	@Check
	public void checkMethodSelection(Selection selection) {
		String errors = typeChecker.typeCheck(selection);

		if (errors != null && errors.length() > 0) {
			error(errors, FjPackage.Literals.METHOD_CALL__ARGS);
			return;
		}
	}

	/**
	 * Checks that a new expression has the correct arguments (one for each of
	 * the fields in the hierarchy)
	 * 
	 * @param n
	 */
	@Check
	public void checkNew(New n) {
		String errors = typeChecker.typeCheck(n);

		if (errors != null && errors.length() > 0) {
			error(errors, FjPackage.Literals.NEW__ARGS);
			return;
		}
	}

	/**
	 * Checks that type cast and the object to cast have no unrelated types.
	 * 
	 * @param n
	 */
	@Check
	public void checkCast(Cast cast) {
		String errors = typeChecker.typeCheck(cast);

		if (errors != null && errors.length() > 0) {
			error(errors, FjPackage.Literals.CAST__TYPE);
			return;
		}
	}

	/*
	 * It's useless since now this is a keyword
	 * 
	 * @Check public void checkFieldNameDifferentFromThis(Field field) { if
	 * (field.getName().equals("this")) {
	 * error("Field name must be different from 'this'", FJPackage.FIELD__NAME);
	 * } }
	 */

}
